Welcome to the Web User Interface for CLIF
==========================================

![paclif_100](gitlab-pages/images/paclif_48.png)  
This projects aims at providing a web interface to [CLIF load testing framework](https://clif.ow2.io/), in order to bring CLIF test plans edition and execution to your web browser.

Disclaimer
----------
CLIF web UI is fresh work in progress. As of today, it has been tested only on Linux environment. However, being based on Java 8, it should be easy to adapt it to other environments.

Download or build
-----------------
You may download a [ready-to-use CLIF web UI application](https://clif.ow2.io/clif-webui/download) archive built by Gitlab-CI.
Or you may build it yourself. After cloning this repository, execute the following commands:

    $ git submodule init
    $ git submodule update
    $ mvn install
    $ cd distro
    $ mvn install

*Note: make sure you use the right versions of Maven (3.6+) and Java (8).*

You will find the CLIF web UI binary distribution .war in the ``distro/target`` directory or in directory ``.m2/`` in your personal folder.

Unzip the binary distribution zip of CLIF web UI to some directory.

Running the CLIF web UI
-----------------------
You need a ProActive CLIF runtime (aka ProActive CLIF server) installed before running the CLIF web application.
Get a [ProActive CLIF server distribution](https://clif.ow2.io/clif-proactive/download/) and unzip it wherever you want.
Set the PATH environment variable to that:
- it includes the ``bin`` directory of the unzipped CLIF runtime,
- and the default Java runtime is version 8.

Then, run ``clifwebui`` command located in the distribution's bin directory.
    
You'll see some execution traces, which shall end with this line on successful start-up completion:

    ...
    INFOS: Starting ProtocolHandler ["http-nio-8090"]    

All CLIF files handled and edited by clif-webui users are stored in the ``CLIFspace`` directory created in current folder from where clif-webui command has been launched. This default directory may be changed by setting property ``clif.workspace`` to the alternative target workspace.

CLIF users who already have a workspace resulting from previous use of the Eclipse-based GUI (aka "console"), be it the default ``CLIFspace`` directory or a custom workspace directory, it may be straightforwardly reused as a clif-webui workspace. Suffice to run clifwebui with their home directory as current directory, or to set Java system property ``clif.workspace`` accordingly through the JAVA_OPTS environment variable.

    $ JAVA_OPTS="-Dclif.workspace=/my/custom/CLIFworkspace" clifwebui

Spring profiles can be chosen to run CLIF web UI. If no profile is chosen, you have to indicate the registry port.
For example, profile "dev" uses port 1234 with Proactive "pnp" protocol:

    $ JAVA_OPTS="-Dspring.profiles.active=dev" clifwebui

It is possible to specify a custom profile with the following command

	$ JAVA_OPTS="--spring.config.location=file:///my/custom/profile" clifwebui
	

Connecting to the CLIF web UI
-----------------------------
Open your favorite web browser and open URL http://localhost:8090/clifwebui. Security and user management is disabled by default.
You can create, import and edit CLIF test plans and scenarios in their native formats (respectively properties files and XML files). For convenience, you may also edit them in a YAML-based "equivalent".
Then, you can deploy and run CLIF tests plans.

More documentation and features to come...

Optional: install Elasticsearch and Kibana
------------------------------------------
CLIF enables for pushing tests measurements to an Elastic Search instance.
If you wish to use this feature, you need an instance of 7.6.2 version of Elasticsearch (and possibly Kibana). You may [install ElasticSearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html) or follow this tutorial to quickly install Elasticsearch on docker for Linux.
To obtain ElasticSearch 7.6.2 for Docker, write the following command:

	$ sudo docker pull docker.elastic.co/elasticsearch/elasticsearch:7.6.2

To launch it on the port 9200, write 

	$ sudo docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.6.2

You may [install Kibana 7.6.2](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html).
Kibana listens by default to [port 5601](http://localhost:5601)
