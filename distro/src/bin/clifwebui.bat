@echo off
set "__webuidir=%~dp0.."
set "basepath=%path%"
set "path=%path%;%__webuidir%\bin"
java -jar %java_opts% -Djava.security.policy="%__webuidir%\etc\java.policy" %__webuidir%\lib\clif-webui-bin.war
set __webuidir=
set "path=%basepath%"