#!/bin/sh

TITLE="CLIF web UI fresh distributions from OW2 Gitlab CI"

# header
printf "<!DOCTYPE html>\n"
printf "<html>\n\t<head><title>%s</title></head>\n" "$TITLE"

# body and title
printf "\t<body>\n"
printf "\t\t<h1>%s</h1>\n" "$TITLE"

# list of distributions
printf "\t\t<ul>\n"
printf "\t\t\t<li><a href=\"%s/-/jobs/artifacts/master/download?job=build\"</a>zip archive of latest CLIF web UI binary distribution</li>\n" "$CI_PROJECT_URL"
printf "\t\t</ul>\n"

# footer
printf "\t<hr>\n"
printf "\t<table border=\"0\">\n"
printf "\t\t<tr valign=\"center\"><td><a href=\"%s\"><img src=\"../images/paclif_48.png\"></a></td>\n" 
printf "\t\t<td><pre>"
printf "build date: %s\n" "`date`"
printf "commit  id: %s" "$CI_COMMIT_SHA"
printf "</pre></td></tr>\n"
printf "\t</table>\n"

# body end
printf "\t</body>\n"
printf "</html>\n"
