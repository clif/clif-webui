/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.jsonhandler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;

/**
 * Test class for JsonHandler plug-in.
 * @author Bruno Dillenseger
 */
public abstract class ParseTest
{
	/**
	 * Runs the tests: loads a JSON file, perform some JsonPath searches and check result set handling
	 * @param args no argument
	 */
	public static void main(String[] args)
	throws IOException
	{
		// create a specimen object and clones it to a session object
		SessionObject specimen = new SessionObject(null);
		SessionObject so = (SessionObject)specimen.createNewSessionObject();

		// set JSON content from a sample JSON file
		Map<String,String> params = new HashMap<String,String>();
		String jsonContent = loadAsString(new FileReader("src/org/ow2/isac/plugin/jsonhandler/ParseTest.json"));
		params.put(SessionObject.CONTROL_SETJSONCONTENT_JSONCONTENT, jsonContent);
		so.doControl(SessionObject.CONTROL_SETJSONCONTENT, params);

		// parse JSON content applying a sample JSON Path, and checks the value of the unique match
		params.clear();
		params.put(SessionObject.CONTROL_PARSE_JSONPATH, "$.store.book[1].title");
		params.put(SessionObject.CONTROL_PARSE_RESULTSET, "title");
		so.doControl(SessionObject.CONTROL_PARSE, params);
		assert so.doGet("title").equals("Sword of Honour") : "Bad resolution of Json Path $.store.book[1].title";

		// parse JSON content applying a sample JSON Path, and checks the value of the unique match of type float
		params.clear();
		params.put(SessionObject.CONTROL_PARSE_JSONPATH, "$.store.book[1].price");
		params.put(SessionObject.CONTROL_PARSE_RESULTSET, "price");
		so.doControl(SessionObject.CONTROL_PARSE, params);
		assert so.doGet("price").equals("12.99") : "Bad resolution of Json Path $.store.book[1].price";

		// parse JSON content applying a sample JSON Path giving no match
		params.clear();
		params.put(SessionObject.CONTROL_PARSE_JSONPATH, "$..book[?(@.author == \"CLIF\")].title");
		params.put(SessionObject.CONTROL_PARSE_RESULTSET, "title");
		so.doControl(SessionObject.CONTROL_PARSE, params);
		try
		{
			so.doGet("title");
			assert false : "Previous match result should not be defined anymore";
		}
		catch (IsacRuntimeException ex)
		{
			// good (expected)
			params.clear();
			params.put(SessionObject.TEST_ISDEFINED_RESULTSET, "title");
			assert
				so.doTest(SessionObject.TEST_ISNOTDEFINED, params)
				: "Unmatchable expression should not define a result set variable";
		}

		// parse JSON content applying a sample JSON Path giving several matches
		params.clear();
		params.put(SessionObject.CONTROL_PARSE_JSONPATH, "$.store.book[*].author");
		params.put(SessionObject.CONTROL_PARSE_RESULTSET, "authors");
		so.doControl(SessionObject.CONTROL_PARSE, params);

		// iterate on printing all matches' values
		params.clear();
		params.put(SessionObject.TEST_ISDEFINED_RESULTSET, "authors");
		Map<String,String> nextParams = new HashMap<String,String>();
		nextParams.put(SessionObject.CONTROL_NEXTRESULT_RESULTSET, "authors");
		int remainingResults = 4;
		while (so.doTest(SessionObject.TEST_ISDEFINED, params))
		{
			System.out.println(so.doGet("#authors") + ". " + so.doGet("authors"));
			assert
				so.doGet("#authors").equals(String.valueOf(remainingResults--))
				: "incorrect number of results for 'authors' result set.";
			so.doControl(SessionObject.CONTROL_NEXTRESULT, nextParams);
		}

		// check that no more value is returned
		try
		{
			so.doControl(SessionObject.CONTROL_NEXTRESULT, nextParams);
			assert false : "Control nextResult should raise an exception once all results have been read";
		}
		catch (IsacRuntimeException ex)
		{
			// good (expected)
		}

		// check that the result set is not defined anymore
		assert
			so.doTest(SessionObject.TEST_ISNOTDEFINED, params)
			: "Result set 'authors' should not be defined once all results have been read";
		try
		{
			so.doGet("authors");
			assert false : "Variable 'results' sould not be available anymore";
		}
		catch (IsacRuntimeException ex)
		{
			// good (expected)
		}

		// clear all variables and check the first result set is not defined anymore 
		params.clear();
		so.doControl(SessionObject.CONTROL_CLEARALL, params);
		try
		{
			so.doGet("title");
			assert false : "Variable 'title' sould not be available anymore";
		}
		catch (IsacRuntimeException ex)
		{
			// good (expected)
		}
	}


	// loads a file content into a String

	static String loadAsString(Reader source)
	throws IOException
	{
		BufferedReader br = new BufferedReader(source);
		StringBuilder fullContent = new StringBuilder();
		String line = br.readLine();
		while (line != null)
		{
			fullContent.append(line).append('\n');
			line = br.readLine();
		}
		br.close();
		return fullContent.toString();
	}
}
