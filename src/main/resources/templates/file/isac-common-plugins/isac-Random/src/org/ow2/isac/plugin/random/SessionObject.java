/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.random;

import java.util.Map;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.TimerAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.util.Random;

/**
 * This plug-in provides samples and timers with random durations.
 * 
 * Its sample actions do not do anything, but take a certain time as explained below.
 * The following distribution laws are provided:
 * <ul>
 * <li>Dirac distribution, centered on the parameter <code>"value"</code>, which turns to be a constant;
 * <li>uniform distribution, on the interval delimited by the parameters <code>"min"</code> and <code>"max"</code>;
 * <li>Poisson distribution, with the parameter given by the parameter <code>"parameter"</code>
 * setting both the duration mean and deviation.
 * <li>Gaussian distribution, with minimum duration given by parameter <code>min</code>,
 * maximum duration given by parameter <code>max</code>,
 * duration mean given by parameter <code>mean</code>, and
 * duration deviation given by parameter <code>deviation</code>
 * <li>negative exponential distribution,
 * with minimum duration given by parameter <code>min</code>
 * and duration mean given by parameter <code>mean</code>
 * </ul> 
 * 
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class SessionObject extends Random implements
	SessionObjectAction,
	SampleAction,
	TimerAction,
	ControlAction,
	DataProvider
{
	static final int CONTROL_SETDIRAC = 0;
	static final String CONTROL_SETDIRAC_VALUE = "value";
	static final int CONTROL_SETUNIFORM = 1;
	static final String CONTROL_SETUNIFORM_MAX = "max";
	static final String CONTROL_SETUNIFORM_MIN = "min";
	static final int CONTROL_SETPOISSON = 2;
	static final String CONTROL_SETPOISSON_PARAMETER = "parameter";
	static final String CONTROL_SETPOISSON_UNIT = "unit";
	static final int CONTROL_SETGAUSSIAN = 3;
	static final String CONTROL_SETGAUSSIAN_DEVIATION = "deviation";
	static final String CONTROL_SETGAUSSIAN_MEAN = "mean";
	static final String CONTROL_SETGAUSSIAN_MAX = "max";
	static final String CONTROL_SETGAUSSIAN_MIN = "min";
	static final int CONTROL_SETNEGATIVEEXPO = 4;
	static final String CONTROL_SETNEGATIVEEXPO_MEAN = "mean";
	static final String CONTROL_SETNEGATIVEEXPO_MIN = "min";
	static final int TIMER_SLEEP = 0;
	static final int SAMPLE_ACTION = 0;
	static final String SAMPLE_ACTION_TYPE = "type";
	static final String SAMPLE_ACTION_COMMENT = "comment";
	static final String SAMPLE_ACTION_SUCCESSFUL = "successful";

	static final String GET_STRING = "string";
	static final String GET_INT = "int";

	// parameters names
	public static final String ENABLE_CBX = "enable";
	/**
	 * 
	 */
	private static final long serialVersionUID = -592953548394386633L;

	private static class UnknownIdentifierError extends Error {
		private static final long serialVersionUID	= -859973658854852340L;
		private final int number;
		private String action;

		private UnknownIdentifierError(int number) {
			this.number = number;
		}

		private void setAction(String action) {
			this.action = action;
		}

		public String getMessage() {
			return "Fatal error in plug-in " + SessionObject.class.getName()
					+ ": unknown " + (action != null ? action + " " : "")
					+ "identifier: " + number;
		}
	}


	private int min=0, max=0, unit=1;
	private double mean=0, deviation=0;
	private int mode = CONTROL_SETDIRAC;

	private long random()
	{
		switch (mode)
		{
		case CONTROL_SETDIRAC:
			return min;
		case CONTROL_SETUNIFORM:
			return nextUniform(min, max);
		case CONTROL_SETPOISSON:
			return nextPoisson(mean, unit);
		case CONTROL_SETGAUSSIAN:
			return nextGaussian(min, max, (int)mean, (int)deviation);
		case CONTROL_SETNEGATIVEEXPO:
			return nextNegativeExponential(min, (int)mean);
		default:
			throw new Error("unknown random mode " + mode);
		}
	}

	//////////////////
	// constructors //
	//////////////////

	public SessionObject()
	{
	}

	public SessionObject(Map<String,String> params)
	{
		this();
	}

	///////////////////////////////////
	// SessionObjectAction interface //
	///////////////////////////////////

	public Object createNewSessionObject()
	{
		return new SessionObject();
	}

	public void reset() {}

	public void close() {}

	////////////////////////////
	// SampleAction interface //
	////////////////////////////

	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report)
	{
		report.setDate(System.currentTimeMillis());
		long value = random();
		try
		{
			Thread.sleep(value);
		}
		catch (UnknownIdentifierError e)
		{
			e.setAction("sample");
			throw e;
		}
		catch (InterruptedException e)
		{
			Thread.currentThread().interrupt();
		}
		report.duration = (int)(System.currentTimeMillis() - report.getDate());
		report.successful = ParameterParser.getCheckBox(params.get(SAMPLE_ACTION_SUCCESSFUL)).contains(ENABLE_CBX);
		report.result = value;
		report.type = params.get(SAMPLE_ACTION_TYPE);
		if (report.type == null || report.type.trim().isEmpty())
		{
			report.type = "dummy action";
		}
		report.comment = params.get(SAMPLE_ACTION_COMMENT);
		if (report.comment == null)
		{
			report.comment = "";
		}
		return report;
	}

	///////////////////////////
	// TimerAction interface //
	///////////////////////////

	public long doTimer(int number, Map<String,String> params)
	{
		try
		{
			return random();
		}
		catch (UnknownIdentifierError e) {
			e.setAction("timer");
			throw e;
		}
	}

	////////////////////////////
	// DataProvider interface //
	////////////////////////////

	public String doGet(String arg)
	{
		long value = random();
		if (arg.equals(GET_INT))
		{
			return String.valueOf(value);
		}
		else if (arg.equals(GET_STRING))
		{
			return nextStringBuilder((int)value).toString();
		}
		else
		{
			throw new IsacRuntimeException("Unknown variable in ISAC plug-in Random: " + arg);
		}
	}

	/////////////////////////////
	// ControlAction interface //
	/////////////////////////////

	public void doControl(int id, Map<String,String> params)
	{
		switch(id)
		{
		case CONTROL_SETDIRAC:
			min = Integer.parseInt(params.get(CONTROL_SETDIRAC_VALUE));
			mode = CONTROL_SETDIRAC;
			break;
		case CONTROL_SETUNIFORM:
			min = Integer.parseInt(params.get(CONTROL_SETUNIFORM_MIN));
			max = Integer.parseInt(params.get(CONTROL_SETUNIFORM_MAX));
			mode = CONTROL_SETUNIFORM;
			break;
		case CONTROL_SETPOISSON:
			mean = Double.parseDouble(params.get(CONTROL_SETPOISSON_PARAMETER));
			unit = (params.containsKey(CONTROL_SETPOISSON_UNIT)
				? Integer.parseInt(params.get(CONTROL_SETPOISSON_UNIT))
				: 1);
			mode = CONTROL_SETPOISSON;
			break;
		case CONTROL_SETGAUSSIAN:
			min = Integer.parseInt(params.get(CONTROL_SETGAUSSIAN_MIN));
			max = Integer.parseInt(params.get(CONTROL_SETGAUSSIAN_MAX));
			mean = Integer.parseInt(params.get(CONTROL_SETGAUSSIAN_MEAN));
			deviation = Integer.parseInt(params.get(CONTROL_SETGAUSSIAN_DEVIATION));
			mode = CONTROL_SETGAUSSIAN;
			break;
		case CONTROL_SETNEGATIVEEXPO:
			min = Integer.parseInt(params.get(CONTROL_SETNEGATIVEEXPO_MIN));
			mean = Integer.parseInt(params.get(CONTROL_SETNEGATIVEEXPO_MEAN));
			mode = CONTROL_SETNEGATIVEEXPO;
			break;
		default:
			throw new UnknownIdentifierError(id);
		}
	}
}
