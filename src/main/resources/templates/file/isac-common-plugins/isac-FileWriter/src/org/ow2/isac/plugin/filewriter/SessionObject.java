/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2013 Orange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.filewriter;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.TestAction;

/**
 * Implementation of a session object for plugin ~FileWriter~
 */
public class SessionObject implements SessionObjectAction, ControlAction, DataProvider, TestAction
{
	static final int TEST_ISOPEN = 7;
	static final int TEST_ISNOTOPEN = 8;
	static final int TEST_EXISTS = 10;
	static final String TEST_EXISTS_PATH = "path";
	static final int TEST_EXISTSNOT = 11;
	static final String TEST_EXISTSNOT_PATH = "path";
	static final int CONTROL_WRITE = 2;
	static final String CONTROL_WRITE_STRING = "string";
	static final int CONTROL_WRITELN = 3;
	static final String CONTROL_WRITELN_STRING = "string";
	static final int CONTROL_NEWLINE = 4;
	static final int CONTROL_FLUSH = 5;
	static final int CONTROL_CLOSE = 6;
	static final int CONTROL_OPEN = 9;
	static final String CONTROL_OPEN_PATH = "path";
	static final String CONTROL_OPEN_APPEND = "append";
	static final String PLUGIN_CHARSET = "charset";

	private BufferedWriter writer = null;
	private String charset = null;
	private File current = null;

	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		charset = ParameterParser.getCombo(params.get(PLUGIN_CHARSET));
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
		this.charset = so.charset;
	}


	private void writeFile(String content, boolean newline)
	{
		if (writer != null)
		{
			try
			{
				if (content != null)
				{
					writer.write(content);
				}
				if (newline)
				{
					writer.newLine();
				}
				if (!newline && content == null)
				{
					writer.flush();
				}
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("~FileWriter~ plug-in can't write to file " + current, ex);
			}
		}
		else
		{
			throw new IsacRuntimeException("~FileWriter~ plug-in can't write: no open file");
		}
	}


	private void openFile(String path, boolean append)
	{
		close();
		try
		{
			current = new File(path);
			writer = new BufferedWriter(
				new OutputStreamWriter(
					new FileOutputStream(current, append),
					charset));
		}
		catch (Exception ex)
		{
			current = null;
			writer = null;
			throw new IsacRuntimeException("~FileWriter~ plug-in could not open file " + path, ex);
		}
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		if (writer != null)
		{
			try
			{
				writer.close();
			}
			catch (IOException ex)
			{
				throw new IsacRuntimeException("~FileWriter~ plug-in could not close file " + current, ex);
			}
			finally
			{
				writer = null;
				current = null;
			}
		}
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		close();
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params)
	{
		switch (number)
		{
		case CONTROL_OPEN:
			String option = ParameterParser.getRadioGroup(params.get(CONTROL_OPEN_APPEND));
			openFile(params.get(CONTROL_OPEN_PATH), !option.equalsIgnoreCase("overwrite"));
			break;
		case CONTROL_CLOSE:
			close();
			break;
		case CONTROL_FLUSH:
			writeFile(null, false);
			break;
		case CONTROL_NEWLINE:
			writeFile(null, true);
			break;
		case CONTROL_WRITELN:
			writeFile(params.get(CONTROL_WRITELN_STRING), true);
			break;
		case CONTROL_WRITE:
			writeFile(params.get(CONTROL_WRITE_STRING), false);
			break;
		default:
			throw new Error(
				"Unable to find this control in ~FileWriter~ ISAC plugin: "
					+ number);
		}
	}

	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var)
	{
		if (var.equals("pathname"))
		{
			return current.toString();
		}
		else if (var.equals("dirname"))
		{
			return current.getParent();
		}
		else if (var.equals("basename"))
		{
			return current.getName();
		}
		else if (var.equals("length"))
		{
			return String.valueOf(current.length());
		}
		else
		{
			throw new IsacRuntimeException("Unknown variable in ~FileWriter~ plug-in: " + var);
		}
	}

	
	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String, String> params)
	{
		switch (number)
		{
		case TEST_EXISTSNOT:
			return ! new File(params.get(TEST_EXISTSNOT_PATH)).exists();
		case TEST_EXISTS:
			return new File(params.get(TEST_EXISTS_PATH)).exists();
		case TEST_ISNOTOPEN:
			return current == null;
		case TEST_ISOPEN:
			return current != null;
		default:
			throw new Error(
				"Unable to find this test #" + number + " in ~FileWriter~ ISAC plugin");
		}
	}
}
