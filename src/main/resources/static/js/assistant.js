/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/*------------------------------------------------------------------	
------------------------------Plugin--------------------------------
-------------------------------------------------------------------*/

/**
 * This function retrieve the params tag and its content for a given plugin.
 * It also scans ace editor content to select an available id.
 * Then, it calls insertPlugin to insert the plugin contents on Ace editor
 *@param {String} pluginName - the plugin Name
 *@param {String} pluginId - the id chose by the user
 */
function addPluginEditor(pluginName, pluginId) {
	localCache.get("activeView") == "xml" ? addPluginEditorXML(pluginName, pluginId) : addPluginEditorYAML(pluginName, pluginId) ;
}

/**
 * This function retrieve the params tag and its content for a given plugin.
 * It also scans ace editor content to select an available id.
 * Then, it calls insertPlugin to insert the plugin contents on Ace editor
 *@param {String} pluginName - the plugin Name
 *@param {String} pluginId - the id chose by the user
 */
function addPluginEditorXML(pluginName, pluginId) {
	var cache = localCache.get("pluginInfo");
	//var pluginId = choosePluginId(pluginName);
	var arrayPluginId = scanPluginId(pluginName);
	if (arrayPluginId.includes(pluginId)) {
		showPopup(getLocalizedText("error.plugin.id2").replace("%1", pluginName).replace("%2", pluginId));
	}
	else {
		var xmlBegin = "<use id=\"" + pluginId + "\" name=\"" + pluginName + "\">";
		var xmlEnd = "</use>";

		for (var i = 0; i < cache["plugins"].length; i++) {
			if (cache["plugins"][i]["name"] == pluginName) {
				var retrievedJson = cache["plugins"][i]["params"];
			}
		}
		var xmlConverted = "";
		if (retrievedJson != undefined && retrievedJson != "") {
			for (var i = 0; i < retrievedJson.length; i++) {
				var temp = "<param name=\"" + retrievedJson[i] + "\" value=\"\"></param>" + "\n";
				//xmlConverted += JSONtoXML(temp);
				xmlConverted += temp;
			}
			xmlBegin += "<params>";
			xmlEnd = "</params>" + xmlEnd;
		}
		var xml = xmlBegin + xmlConverted + xmlEnd;
		xml = formatXml(xml);
		//modify type="String" to value =""
		xml = xml.replaceAll("type=\"String\"", "value=\"\"");
		insertPluginXML(xml);
	}
}

/**
* This function inserts automatically the retrieved plugin text at a valid position.
* It will be placed before the tag </plugins>
* @param {String} pluginContent : the text that will be inserted at the position defined by this function
*/
function insertPluginXML(pluginContent) {
	var editor = ace.edit(getActiveEditor());
	var cursorPosition = editor.getCursorPosition();
	//remove the last \n
	xmlToInsert = pluginContent.substring(0, pluginContent.length - 1);
	var cursorPositionToInsert = [cursorPosition["row"], cursorPosition["column"]]
	xmlToInsert = formatInsertion(xmlToInsert, cursorPositionToInsert, "plugins");
	//insert the formatted xml
	var Range = ace.require('ace/range').Range;
	editor.session.replace(new Range(cursorPosition["row"], cursorPosition["column"], cursorPosition["row"], cursorPosition["column"]), xmlToInsert);
}


/**
 * This function retrieve the params tag and its content for a given plugin.
 * It also scans ace editor content to select an available id.
 * Then, it calls insertPlugin to insert the plugin contents on Ace editor
 *@param {String} pluginName - the plugin Name
 *@param {String} pluginId - the id chose by the user
 */
function addPluginEditorYAML(pluginName, pluginId) {
	var cache = localCache.get("pluginInfo");
	// to do: determine the number of spaces
	for (var i = 0; i < cache["plugins"].length; i++) {
		if (cache["plugins"][i]["name"] == pluginName) {
			var retrievedJson = cache["plugins"][i]["params"];
		}
	}
	var textParams = "";
	if (retrievedJson != undefined && retrievedJson != "") {
		for (var i = 0; i < retrievedJson.length; i++) {
			textParams += "\n            - name: " + retrievedJson[i] + "\n              value: ";
		}
	}
	
	insertPluginYAML("        - id: " + pluginId + "\n          name: " + pluginName + "\n          params: " + textParams);
}

/**
* This function inserts automatically the retrieved plugin text at a valid position.
* It will be placed before the tag </plugins>
* @see scenarioMenuHandlerYAML It restrict the insert location
* @param {String} content : the text that will be inserted at the position defined by this function
*/
function insertPluginYAML(content) {
	var e = ace.edit(getActiveEditor());// editor
	var eT = e.getValue();
	var eTA = eT.split("\n");
	
	var currentRow = e.getCursorPosition().row;
	var pasteRange = new ace.Range(currentRow, Number.MAX_VALUE, currentRow, Number.MAX_VALUE);
	
	var behaviorsIndex = -1;
	var pluginIndex = -1;
	var useIndex = -1;
	
	// check missing directives : 
	for(var i=0 ; i < eTA.length ; i++){
		if(eTA[i].startsWith(ws(2)+"behaviors")){
			behaviorsIndex = i;
		} else if(eTA[i].startsWith(ws(4)+"plugins")){
			pluginIndex = i;
		} else if(eTA[i].startsWith(ws(6)+"use")){
			useIndex = i;
			break;
		} else if(eTA[i].includes("id:") || eTA[i].includes("behavior:")){ // only for minimizing calls
			break;
		}
		
	}
	
	// handle missing directives: 
	var preContent = "";
	if(behaviorsIndex > -1){
		if(pluginIndex > -1){
			if(useIndex > -1){
				//pasteRange = new ace.Range(useIndex, Number.MAX_VALUE, useIndex, Number.MAX_VALUE); // add systematically after "use:"
			} else {
				pasteRange = new ace.Range(pluginIndex, Number.MAX_VALUE, pluginIndex, Number.MAX_VALUE);
				preContent += ws(6)+"use: \n"; 
			}
		} else {
			pasteRange = new ace.Range(behaviorsIndex, Number.MAX_VALUE, behaviorsIndex, Number.MAX_VALUE);
			preContent += ws(4)+"plugins: \n"+ws(6)+"use: \n"; 
		}
	} else {
		preContent += ws(2)+"behaviors: \n"+ws(4)+"plugins: \n"+ws(6)+"use: \n"; 
	}
	currentRow = pasteRange.start.row;
	content = preContent + content;
	
	// \n determination:
	var result = generalInsertionHandler(pasteRange, eTA, content, currentRow, "id:");
	content = result.content;
	pasteRange = result.range
	
	dashesHandler(e, eTA, ws(4)+"plugins");
	e.session.replace(pasteRange, content);
}

/*------------------------------------------------------------------	
-----------------------------Primitive------------------------------
-------------------------------------------------------------------*/
/**
 *This function handles the primitive insertion. It retrieves the text to insert and determine where it will be inserted.
 *Then it formats it and inserts it in the Ace editor
 */
function insertPrimitive(primitiveType, pluginName, primitiveName, idPlugin) {
	localCache.get("activeView") == "xml" ? insertPrimitiveXML(primitiveType, pluginName, primitiveName, idPlugin) : insertPrimitiveYAML(primitiveType, pluginName, primitiveName, idPlugin) ;
}

function insertPrimitiveXML(primitiveType, pluginName, primitiveName, idPlugin) {
	//retrive the text to insert
	var lowerCasePrimitiveType = primitiveType.toLowerCase();
	var editor = ace.edit(getActiveEditor());
	var cache = localCache.get("pluginInfo");
	var retrievedJson = retrievePrimitive(lowerCasePrimitiveType, pluginName, primitiveName, cache);
	var cursorPosition = editor.getCursorPosition();

	//transform the json object to a string
	var xmlConverted = "";
	for (var i = 0; i < retrievedJson.length; i++) {
		var temp = "<param name=\"" + retrievedJson[i] + "\" value=\"\"></param>" + "\n";
		//xmlConverted += JSONtoXML(temp);
		xmlConverted += temp;
	}
	//format the retrieved text
	var beginXml = "";
	var primitiveTypeSingular = lowerCasePrimitiveType.substring(0, lowerCasePrimitiveType.length - 1);
	beginXml += "<" + primitiveTypeSingular + " use=\"" + idPlugin + "\" name=\"" + primitiveName + "\">";
	var endXml = "</" + primitiveTypeSingular + ">";
	if (retrievedJson.length > 0) {
		beginXml += "<params>";
		endXml = "</params>" + endXml;
	}
	var xmlToInsert = formatXml(beginXml + xmlConverted + endXml);

	//delete the "\n added by format xml
	xmlToInsert = xmlToInsert.substring(0, xmlToInsert.length - 1);
	var cursorPositionToInsert = [cursorPosition["row"], cursorPosition["column"]]
	xmlToInsert = formatInsertion(xmlToInsert, cursorPositionToInsert, "behavior");
	var Range = ace.require('ace/range').Range;
	editor.session.replace(new Range(cursorPosition["row"], cursorPosition["column"], cursorPosition["row"], cursorPosition["column"]), xmlToInsert);
}

function insertPrimitiveYAML(primitiveType, pluginName, primitiveName, pluginId) {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var currentRow = editor.getCursorPosition().row;
	var pasteRange = new ace.Range(currentRow, Number.MAX_VALUE, currentRow, Number.MAX_VALUE);
	var eTA = editorText.split("\n");
	var editorTextWithoutSpaces = editorText.replace(/[^\S\n]/g,'');
	var eTA_spaceless = editorTextWithoutSpaces.split("\n");
	
	var indent = primstate_findIndent(eTA, currentRow);
	
	var lowerCasePrimitiveType = primitiveType.toLowerCase();
	var content = "";
	content += indent + "- " + lowerCasePrimitiveType.substring(0, lowerCasePrimitiveType.length - 1) + ": \n";
	content += indent + "    use: " + pluginId + "\n";
	content += indent + "    name: " + primitiveName + "\n";
	content += indent + "    params: ";
	
	var cache = localCache.get("pluginInfo");
	var retrievedJson = retrievePrimitive(lowerCasePrimitiveType, pluginName, primitiveName, cache);
	
	for (var i = 0; i < retrievedJson.length; i++) {
		content += "\n" + indent + "      - name: " + retrievedJson[i] + "\n";
		content += indent + "        value: ";
	}
	//content = content.substring(0, content.length-3);
			
	content += "\n" + indent;
	//dashHandlerPrim(editor, eTA, pasteRange);
	// \n determination:
	var gih = generalInsertionHandler(pasteRange, eTA_spaceless, content, currentRow,new RegExp(' *\\b(?:sample|timer|control|nchoice|preemptive|while|if)\\b:', 'g'));
	content = gih.content.replace(/\n$/, "");
	pasteRange = gih.range
	
	editor.session.replace(pasteRange, content);
	
}

/**
* This function retrieves a given primitive. It reads the cache, retrieves the wanted JSON object and returns it.
* @param {String} primitiveType : the primitive type (control, sample, timer)
* @param {String} pluginName : the plugin name
* @param {String} primitiveName : the primitive name
* @param {Object} cache : the cache content
* @returns {String} the retrieved content of the primitive with an xml format
*/
retrievePrimitive = function retrievePrimitive(primitiveType, pluginName, primitiveName, cache) {
	var endFor = false;
	var retrievedJson = "";
	var i = 0;
	var j = 0;
	for (i; i < cache["plugins"].length && endFor == false; i++) {
		if (cache["plugins"][i]["name"] == pluginName) {
			for (j; j < cache["plugins"][i][primitiveType].length && endFor == false; j++) {
				if (cache["plugins"][i][primitiveType][j]["name"] == primitiveName) {
					if (cache["plugins"][i][primitiveType][j].hasOwnProperty("params")) {
						retrievedJson = cache["plugins"][i][primitiveType][j]["params"];
					}
					endFor = true;
				}
			}
		}
	}
console.log(retrievedJson);
	return retrievedJson;

}

/*------------------------------------------------------------------	
-----------------------------Statement------------------------------
-------------------------------------------------------------------*/


function insertStatement(statementType, pluginName, testName, pluginId) {
	localCache.get("activeView") == "xml" ? insertStatementXML(statementType, pluginName, testName, pluginId) : insertStatementYAML(statementType, pluginName, testName, pluginId);
}

function insertStatementXML(statementType, pluginName, testName, pluginId) {
	var editor = ace.edit(getActiveEditor());
	var cursorPosition = editor.getCursorPosition();
	var cache = localCache.get("pluginInfo");
	var xmlConverted = "";
	var retrievedJson = retrieveTest(pluginName, testName, cache);

	for (var i = 0; i < retrievedJson.length; i++) {
		var temp = "<param name=\"" + retrievedJson[i] + "\" value=\"\"></param>" + "\n";
		xmlConverted += temp;
	}

	//build the xml String to insert
	var beginXml = "<" + statementType + ">";
	var endXml = "</" + statementType + ">"
	var beginCondition = "<condition use=\"" + pluginId + "\" name=\"" + testName + "\">";
	var endCondition = "</condition>"
	var additionalTag = "<then>" + "\n" + "</then>" + "\n" + "<else>" + "\n" + "</else>";
	var beginParamsTag = "<params>";
	var endParamsTag = "</params>";

	var xmlToInsert = beginXml + beginCondition;
	if (xmlConverted.length > 1) {
		xmlToInsert += beginParamsTag + xmlConverted + endParamsTag;
	}
	xmlToInsert += endCondition;
	if (statementType == "if") {
		xmlToInsert += additionalTag;
	}
	xmlToInsert += endXml;
	xmlToInsert = formatXml(xmlToInsert);

	//delete the \n added by format xml
	xmlToInsert = xmlToInsert.substring(0, xmlToInsert.length - 1);

	//format the insertion
	var cursorPositionToInsert = [cursorPosition["row"], cursorPosition["column"]]
	xmlToInsert = formatInsertion(xmlToInsert, cursorPositionToInsert, "behavior");
	//insert the formatted xml
	var Range = ace.require('ace/range').Range;
	editor.session.replace(new Range(cursorPosition["row"], cursorPosition["column"], cursorPosition["row"], cursorPosition["column"]), xmlToInsert);
}


function insertStatementYAML(statementType, pluginName, testName, pluginId) {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var currentRow = editor.getCursorPosition().row;
	var pasteRange = new ace.Range(currentRow, Number.MAX_VALUE, currentRow, Number.MAX_VALUE);
	var eTA = editorText.split("\n");
	var editorTextWithoutSpaces = editorText.replace(/[^\S\n]/g,'');
	var eTA_spaceless = editorTextWithoutSpaces.split("\n");
	
	var indent = primstate_findIndent(eTA, currentRow);
	
	var content = "";
	content += indent + "- " + statementType + ": \n";
	content += indent + "    condition: \n";
	content += indent + "      use: " + pluginId + "\n";
	content += indent + "      name: " + testName + "\n";
	content += indent + "      params: ";
	
	var cache = localCache.get("pluginInfo");
	var retrievedJson = retrieveTest(pluginName, testName, cache);

	console.log(retrievedJson);
	for (var i = 0; i < retrievedJson.length; i++) {
		content += "\n" + indent + "      - name: " + retrievedJson[i] + "\n";
		content += indent + "        value: ";
	}
	
	if(statementType == "if"){
		content += "\n" + indent + "    then: \n";
		content += indent + "    else: ";
	}else{// while & preemptive
		content += "\n" + indent + "    do: ";
	}
		
	content += "\n" + indent;
	//dashHandlerPrim(editor, eTA, pasteRange);
	// \n determination:
	var gih = generalInsertionHandler(pasteRange, eTA_spaceless, content, currentRow, new RegExp(' *\\b(?:sample|timer|control|nchoice|preemptive|while|if)\\b:', 'g'));
	content = gih.content;//.replace(/\n$/, "");
	pasteRange = gih.range
	
	editor.session.replace(pasteRange, content);
	
}

/**
* This function retrieves a given test. It reads the cache, retrieves the wanted JSON object and returns it.
* @param {String} pluginName : the plugin name
* @param {String} testName : the test name
* @param {Object} cache : the cache content
* @returns {String} the retrieved content of the primitive with an object format
*/
retrieveTest = function retrieveTest(pluginName, testName, cache) {
	var endFor = false;
	var retrievedJson = "";
	var i = 0;
	var j = 0;
	for (i; i < cache["plugins"].length && endFor == false; i++) {
		if (cache["plugins"][i]["name"] == pluginName) {
			for (j; j < cache["plugins"][i]["tests"].length && endFor == false; j++) {
				if (cache["plugins"][i]["tests"][j]["name"].toString() == testName) {
					retrievedJson = cache["plugins"][i]["tests"][j]["params"];
					endFor = true;
				}
			}
		}
	}
	
	return retrievedJson;
}

/*------------------------------------------------------------------	
-----------------------------Choice-------------------------------
-------------------------------------------------------------------*/

function insertChoice() {
	localCache.get("activeView") == "xml" ? insertChoiceXML() : insertChoiceYAML();
}

function insertChoiceXML() {
	var editor = ace.edit(getActiveEditor());
	var cursorPosition = editor.getCursorPosition();
	var xmlToInsert = "<nchoice>\n\t<choice proba=\"1\">\n\t</choice><choice proba=\"1\">\n\t</choice>\n</nchoice>";
	xmlToInsert = formatXml(xmlToInsert);
	//delete the \n added by format xml
	xmlToInsert = xmlToInsert.substring(0, xmlToInsert.length - 1);
	//format the insertion
	var cursorPositionToInsert = [cursorPosition["row"], cursorPosition["column"]]
	xmlToInsert = formatInsertion(xmlToInsert, cursorPositionToInsert, "behavior");
	//insert the formatted xml
	var Range = ace.require('ace/range').Range;
	editor.session.replace(new Range(cursorPosition["row"], cursorPosition["column"], cursorPosition["row"], cursorPosition["column"]), xmlToInsert);
}

function insertChoiceYAML() {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var currentRow = editor.getCursorPosition().row;
	var pasteRange = new ace.Range(currentRow, Number.MAX_VALUE, currentRow, Number.MAX_VALUE);
	var eTA = editorText.split("\n");
	var editorTextWithoutSpaces = editorText.replace(/[^\S\n]/g,'');
	var eTA_spaceless = editorTextWithoutSpaces.split("\n");
	
	var indent = primstate_findIndent(eTA, currentRow);
	
	var content = "";
	content += indent + "- nchoice: \n";
	content += indent + "  - proba: 1\n";
	content += indent + "  - proba: 1";
	
		
	content += "\n" + indent;
	//dashHandlerPrim(editor, eTA, pasteRange);
	// \n determination:
	var gih = generalInsertionHandler(pasteRange, eTA_spaceless, content, currentRow, new RegExp(' *\\b(?:sample|timer|control|nchoice|preemptive|while|if)\\b:', 'g'));
	content = gih.content;
	pasteRange = gih.range
	
	editor.session.replace(pasteRange, content);
}

/*------------------------------------------------------------------	
-----------------------------Behavior-------------------------------
-------------------------------------------------------------------*/

function insertBehavior() {
	localCache.get("activeView") == "xml" ? insertBehaviorXML() : insertBehaviorYAML();
}

function insertBehaviorXML() {
	var editor = ace.edit(getActiveEditor());
	var cursorPosition = editor.getCursorPosition();
	//	var completeText = edit.getValue();
	var behaviorId = chooseBehaviorId();
	//create the text that will be inserted
	var xmlBegin = "<behavior id=\"" + behaviorId + "\">";
	var xmlEnd = "</behavior>";
	var xmlToInsert = xmlBegin + xmlEnd;
	xmlToInsert = formatXml(xmlToInsert);
	//delete the \n added by format xml
	xmlToInsert = xmlToInsert.substring(0, xmlToInsert.length - 1);
	//format the insertion
	var cursorPositionToInsert = [cursorPosition["row"], cursorPosition["column"]]
	xmlToInsert = formatInsertion(xmlToInsert, cursorPositionToInsert, "behaviors");

	var Range = ace.require('ace/range').Range;
	editor.session.replace(new Range(cursorPosition["row"], cursorPosition["column"], cursorPosition["row"], cursorPosition["column"]), xmlToInsert);
}

function insertBehaviorYAML() {
	var editor = ace.edit(getActiveEditor());
	var eTA = editor.getValue().split("\n");
	var eT = editor.getValue();
	var eTA_spaceless = eT.replace(/[^\S\n]/g,'').split("\n");
	
	var currentRow = editor.getCursorPosition().row;
	var pasteRange = new ace.Range(currentRow, Number.MAX_VALUE, currentRow, Number.MAX_VALUE);
	
	var behaviorId = checkId(editor.getValue(), "B");
	var content = "      - id: " + behaviorId + "\n        do:";
	
	// check if currentRow is at scenario: directive (specific case): 
	for(var i = currentRow ; i < eTA.length ; i--){
		if(eTA_spaceless[i] == "behavior:"){
			break;
		}else 
		
		if(eTA_spaceless[i] == "scenario:"){
			// find behavior: flag
			for(var j = currentRow ; j<eTA.length ; j++){
				if(eTA_spaceless[j] == "behavior:"){
					find = true;
					currentRow = j;
					pasteRange = new ace.Range(currentRow, Number.MAX_VALUE, currentRow, Number.MAX_VALUE);
					break;
				}else
				// case when directive behavior missing:
				if(eTA_spaceless[j] == "loadprofile:"){
					content = ws(4) + "behavior: \n" + content + "\n";
					pasteRange = new ace.Range(j, 0, j, 0);
					break;
				}
			}
			
			break;
		}else if(eTA[i].trimStart() != ""){
			break;
		}
	}
	
	// \n determination:	
	var gih = generalInsertionHandler(pasteRange, eTA_spaceless, content, currentRow, "id:");
	content = gih.content;
	pasteRange = gih.range;
	
	dashesHandler(editor, eTA,  ws(4) + "behavior")
	editor.session.replace(pasteRange, content);
}


/*------------------------------------------------------------------	
-----------------------------Loadprofile-------------------------------
-------------------------------------------------------------------*/

function insertLoadprofile(behaviorName) {
	localCache.get("activeView") == "xml" ? insertLoadprofileXML(behaviorName) : insertLoadprofileYAML(behaviorName) ;
}

function insertLoadprofileXML(behaviorName) {
	var editor = ace.edit(getActiveEditor());
	var cursorPosition = editor.getCursorPosition();

	//build the text to insert
	var groupBegin = "<group behavior=\"" + behaviorName + "\" forceStop=\"true\">" + "\n";
	var rampBegin = "\t" + "<ramp style=\"line\">" + "\n";
	var points1 = "\t\t" + "<points>" + "\n" + " \t\t\t<point x=\"0\" y=\"1\"></point>" + "\n";
	var points2 = "\t\t\t<point x=\"1\" y=\"1\"></point>" + "\n" + "\t\t</points>" + "\n";
	var rampEnd = "\t</ramp>\n";
	var groupEnd = "</group>";
	var xmlToInsert = groupBegin + rampBegin + points1 + points2 + rampEnd + groupEnd;
	xmlToInsert = formatXml(xmlToInsert);
	//delete the \n added by format xml
	xmlToInsert = xmlToInsert.substring(0, xmlToInsert.length - 1);
	//format the insertion
	var cursorPositionToInsert = [cursorPosition["row"], cursorPosition["column"]]
	xmlToInsert = formatInsertion(xmlToInsert, cursorPositionToInsert, "loadprofile");
	//insert the formatted xml
	var Range = ace.require('ace/range').Range;
	editor.session.replace(new Range(cursorPosition["row"], cursorPosition["column"], cursorPosition["row"], cursorPosition["column"]), xmlToInsert);

}

function insertLoadprofileYAML(behaviorName) {
	var content = "";
	content += "      - behavior: " + behaviorName + "\n";
	content += "        forceStop: true\n";
	content += "        ramp: \n";
	content += "          style: line\n";
	content += "          points: \n";
	content += "            point: \n";
	content += "              - x: 0\n";
	content += "                y: 1\n";
	content += "              - x: 1\n";
	content += "                y: 1";

	var editor = ace.edit(getActiveEditor());
	var eTA = editor.getValue().split("\n");
	var eT = editor.getValue();
	var eTA_spaceless = eT.replace(/[^\S\n]/g,'').split("\n");
	
	var currentRow = editor.getCursorPosition().row;
	var pasteRange = new ace.Range(currentRow, Number.MAX_VALUE, currentRow, Number.MAX_VALUE);
	
	
	
	var loadprofileIndex = -1;;
	var groupIndex = -1;
	
	// determine potential missing declaration : 
	for(var i=0 ; i<eTA_spaceless.length ; i++){
		if(eTA_spaceless[i].includes("loadprofile:")){
			loadprofileIndex = i;
		} else if(eTA_spaceless[i].includes("group:")){
			groupIndex = i;
		}
		
	}
	
	if(loadprofileIndex > -1){
		if(groupIndex > -1){
			if(groupIndex >= currentRow){
				pasteRange = new ace.Range(groupIndex, Number.MAX_VALUE, groupIndex, Number.MAX_VALUE);
			}
			
		}else{
			pasteRange = new ace.Range(loadprofileIndex, Number.MAX_VALUE, loadprofileIndex, Number.MAX_VALUE);
			content = "    group: \n" + content; 
			
		}
		
	}else{ 
		content = "\n  loadprofile: \n    group: \n" + content; 
		pasteRange = new ace.Range(eTA_spaceless.length-1, Number.MAX_VALUE, eTA_spaceless.length-1, Number.MAX_VALUE);
		
	}
	
	
	// \n determination:
	var gih = generalInsertionHandler(pasteRange, eTA_spaceless, content, currentRow, "behavior:");
	content = gih.content;
	pasteRange = gih.range;
	
	// need check group:
	dashesHandler(editor, eTA, ws(2) + "loadprofile")
	editor.session.replace(pasteRange, content);
	
}

/*------------------------------------------------------------------	
------------------------------Shaping-------------------------------
-------------------------------------------------------------------*/

/** This function removes the Ace context menu
 */
function removeAceContextMenu() {
	contextMenuAceEditor.opacity = "0";
	contextMenuAceEditor.visibility = "hidden";
}

/**
 *This function adds tabs to a formated xml String by checking the previous xml tags
 *@param {String} xml - The formatted xml String
 *@param {Array} cursorPositionInsert - The row and column where the xml must be inserted
 *@param {String} - the type of xml text that will be inserted
 *@returns {String} formatedXml - The xml with the inserted tabulation 
 */
function formatInsertion(xml, cursorPositionToInsert, tagType) {
	var formattedXml = "";
	var cursorRow = cursorPositionToInsert[0];
	var cursorColumn = cursorPositionToInsert[1];
	//split the xml by its line break
	var editor = ace.edit(getActiveEditor());
	var completeText = editor.getValue();

	var arrayXml = xml.split("\n");
	for (var i = 0; i < arrayXml.length - 1; i++) {
		arrayXml[i] += "\n";
	}

	//split the array according to "\n"
	var arrayCompleteText = completeText.split("\n");
	for (var i = 0; i < arrayCompleteText.length; i++) {
		arrayCompleteText[i] += "\n";
	}

	var tabCount = countTabulation(arrayCompleteText, cursorRow, tagType);

	//add tabs to each line
	for (var i = 0; i < arrayXml.length; i++) {
		for (var j = 0; j < tabCount; j++) {
			arrayXml[i] = "\t" + arrayXml[i];
		}
		formattedXml += arrayXml[i];
	}
	formattedXml = formatAdjacentLines(arrayCompleteText, cursorRow, cursorColumn, formattedXml, tabCount);
	return formattedXml;
}
/** This function checks the the number of tabs on the current or previous line
 *	It also checks the previous tag line to determine if an extra tab is required
 * @param arrayCompleteText {Array} - the text contained in the editor divided by line breaks
 * @param cursorRow {number} - the cursor row number
**/
function countTabulation(arrayCompleteText, cursorRow, tagType) {
	var validRow = false;
	var count = cursorRow;
	var tabCount = 0;

	//check if the current row is empty. Else, we use the previous row to check if we must add a tab
	var row = arrayCompleteText[count];
	for (count; count >= 0 && validRow == false; count--) {
		row = arrayCompleteText[count];
		if (row.includes("<") && row.includes(">")) {
			validRow = true;
		}
	}
	//count the number of tabs on the chosen row
	var stopTabCount = false;
	var charIndex = 0;
	var spaceCount = 0;
	while (stopTabCount != true && charIndex < row.indexOf("<")) {
		if (row[charIndex] == "\t") {
			tabCount++;
			spaceCount = 0;
		}
		else if (row[charIndex] == ' ') {
			spaceCount++;
		}
		else {
			stopTabCount = true;
		}
		if (spaceCount == 4) {
			spaceCount = 0;
			tabCount++;
		}
		charIndex++;
	}
	if (tagType != "behaviors") {
		if (row.includes("<scenario>") || row.includes("<behaviors>") || row.includes("<plugins>") ||
			row.includes("loadprofile") || row.includes("<behavior") || row.includes("then") ||
			row.includes("else") || row.includes("choice")) {
			tabCount++;
		}
	}

	return tabCount;
}

/**
 * According to the content on the line where the user's cursor is, this function will insert lines before or after
 * the inserted text and add the required tabulations
 * @param arrayCompleteText {Array} - the text contained in the editor divided by line breaks
 * @param cursorRow {number} - the cursor row number
 * @param cursorColumn {number} - the cursor column number
 * @param formattedXml {String} - the text to insert
 * @param tabCount {number} - the number of tabs to insert
 */
function formatAdjacentLines(arrayCompleteText, cursorRow, cursorColumn, formattedXml, tabCount) {
	//check if we need to insert \n before or after the inserted text
	var row = arrayCompleteText[cursorRow];
	var editor = ace.edit(getActiveEditor());
	if ((row.includes("<") || row.includes(">")) && cursorColumn <= row.indexOf("<")) { //right click before <
		formattedXml = formattedXml + "\n";
		var tabAfterCursor = 0;
		//remove tabs before what is to be inserted
		for (var i = 0; i < row.indexOf("<") - cursorColumn; i++) {
			tabAfterCursor++;
		}
		//add tabs after
		for (var i = 0; i < row.indexOf("<") - tabAfterCursor; i++) {
			formattedXml = formattedXml + "\t";
			formattedXml = formattedXml.substring(1);
		}
	}
	else if ((row.includes("<") || row.includes(">")) && cursorColumn <= row.lastIndexOf("<")) {
		formattedXml = formattedXml + "\n";
		for (var i = 0; i < tabCount - 1; i++) {
			formattedXml += "\t";
		}
	}

	if ((row.includes("<") || row.includes(">")) && cursorColumn >= row.indexOf(">")) { //right click after >
		formattedXml = "\n" + formattedXml;
	}

	if (row.indexOf("<") == -1 && row.indexOf(">") == -1) {
		//delete the empty line
		var Range = ace.require('ace/range').Range;
		editor.session.replace(new Range(cursorRow, 0, cursorRow, Number.MAX_VALUE), "");;
	}
	return formattedXml;
}

/*------------------------------------------------------------------	
------------------------------File Name-----------------------------
-------------------------------------------------------------------*/

/**
*Check if the file's name contains .extension or not
*If the file doesn't contains the extension at its end, it is added
*@param {string} extension - The extension of the file
*@param {string} filePromptName - The file's name
*@returns {string} The new file's name
 */
checkFilePromptName = function checkFilePromptName(extension, filePromptName) {
	var extensionIndex = filePromptName.lastIndexOf("." + extension);
	if (extensionIndex == -1) {
		filePromptName = filePromptName + "." + extension;
	}
	else if ((filePromptName.length - extensionIndex) != 4) {
		filePromptName = filePromptName + "." + extension;
	}
	return filePromptName;
}

/**
 *This function is called when a ctp file is created from an xis file. It adds the xis file name as a blade argument
 */
function addFileNameTestPlan() {
	var editor = ace.edit(getActiveEditor());
	var completeText = editor.getValue();
	var xisFileName = localCache.get("clickedXisFile");
	var arrayXml = completeText.split("\n");
	for (var i = 0; i < arrayXml.length; i++) {
		if (arrayXml[i].includes("blade.1.argument=")) {
			arrayXml[i] += xisFileName;
		}
	}
	completeText = "";
	for (var i = 0; i < arrayXml.length - 1; i++) {
		completeText += arrayXml[i] + "\n";
	}
	editor.setValue(completeText);
}


/**
 * Add dash and spaces when the directive have more than one component implemented
 * @param e editor
 * @param eTA editor text array
 * @param directive the directive declaration
 */
function dashesHandler(e, eTA, directive){
	var start = -1;
	// nb of spaces for directive declaration + 2 for children: 
	indent = directive.substring(0, directive.length - directive.trimStart().length) + ws(2);
	
	if(directive == ws(2)+"loadprofile" || directive == ws(4)+"plugins" ){
		indent += ws(2);
	}
	
	var isChild = false;
	var isList = true;
	var isId = false;
	
	for(var i=0 ; i < eTA.length ; i++){
		if(start > 0){
			isChild = eTA[i].startsWith(indent) || eTA[i].trimLeft() == "";
			isId = eTA[i].startsWith(indent + "id") || eTA[i].startsWith(indent + "- id") ||eTA[i].startsWith(indent + "behavior") || eTA[i].startsWith(indent + "- behavior");
			if (isId){
				isList = eTA[i].startsWith(indent + "-");
			}
			
			if(!isChild && !eTA[i].startsWith(ws(4)+"group") && !eTA[i].startsWith(ws(6)+"use")){
				break;
			}else if(!isList){
				if(isId){
					e.session.insert({row:i, column:indent.length}, "- ");
				}else{
					e.session.insert({row:i, column:indent.length}, "  ");
				}
			}
			
		} else if(eTA[i].startsWith(directive)){
			start = i;
			
		}

	}
	
}

function dashHandlerPrim(e, eTA, pasteRange){
	var tmpContent;
	var directiveIndex = -1;
	var directiveIndent = 0;
	var regex = new RegExp(ws(6) + ' *\\b(?:do|then|else)\\b:', 'g');
	for(var i = pasteRange.start.row ; i>0 ; i--){ 
		tmpContent = eTA[i];
		if(tmpContent.match(regex)){
			directiveIndex = i;
			directiveIndent = primstate_findIndent(eTA, directiveIndex);
			break;
		}
	}
	
	for(var i = directiveIndex+1 ; i<eTA.length ; i++){
		tmpContent = eTA[i];
		
		if(!tmpContent.startsWith(directiveIndent + "- ")){
			addDashHandlerPrim(e, eTA, directiveIndex, directiveIndent);
			break;
		}else if(tmpContent.trimLeft() != ""){
			break;
		}
	}
	
}

function addDashHandlerPrim(e, eTA, directiveIndex, directiveIndent){
	var regex = new RegExp(directiveIndent + '\\b(?:sample|timer|control|nchoice|preemptive|while|if)\\b:', 'g');
	var stop = false;
	for(var i = directiveIndex ; i < eTA.length || !stop ; i++){
		if(eTA[i].startsWith(directiveIndent)){
			if(eTA[i].match(regex)){
				e.session.insert({row:i, column:directiveIndent.length}, "- ");
			}else{
				e.session.insert({row:i, column:0}, "  ");
			}
			// add indent
		}else if (eTA[i].trimLeft() != ""){
			stop = true;
		}
	}
}

function primstate_findIndent(eTA, currentRow){
	var childIndent = "";
	
	var content = eTA[currentRow];
	
	if(content.trimLeft() == ""){
		// look dictionary vs collection to know the minimum indentation: 
		var find = false;
		var indent = 0;
		for(var i = 0; i < eTA.length ; i++){
			
			if(find){
				console.log(eTA[i]);
				if(eTA[i].trimLeft() != ""){
					if(eTA[i].includes("-")){
						indent = 10;
					}else{
						indent = 8;
					}
					break;
				}
			}
			
			if(eTA[i].includes(ws(4)+"behavior")){
				find = true;
			}
		}
		if(content.length <indent){
			return ws(indent);
		}
		return content;
	}
	
	if(content.match(new RegExp(' *-? *\\b(?:sample|timer|control|nchoice|preemptive|while|if)\\b:', 'g'))){
		return ws(content.search(/\S/));
	}
	
	if(content.includes("do:") || content.includes("then:") || content.includes("else:")){
		return ws(content.search(/\S/)+2);
	}
	
	return ws(content.search(/\S/));
	/*
	var testNextChild = null;
	var ws = ' ';
	for(var i=0 ; i < eTA.length ; i++){
		if(i <= currentRow){
			if(eTA[i].includes(ws(6) + "do:") || eTA[i].includes(ws(10) + "then:") || eTA[i].includes(ws(10) + "else:")){
				childIndent = eTA[i].substring(0, eTA[i].length - eTA[i].trimStart().length) + "  ";
				if(eTA[i].match('-')){
					childIndent += "  ";
				}
			}
		}else{
			if(testNextChild == null){
				if(eTA[i].trimLeft != ""){
					if(eTA[i].match(new RegExp(ws(8) + ' *-? *\\b(?:sample|timer|control|nchoice|preemptive|while|if)\\b:', 'g'))){
						testNextChild = eTA[i].substring(0, eTA[i].length - eTA[i].trimStart().length);
						break;
					}else{
						break;
					}
				}
				
			}
		}
	}
	
	if(testNextChild != null){
		// have to choose the statement concerned by the adding (the do/then/else nested or at parents declaration)
		
		// we'll suppose that if it is ONE empty line after the nested, we will add for it
		if(eTA[currentRow-1].trimLeft() == ""){
			return testNextChild;
		} else{
			return childIndent;
		}
	}*/
	
	return childIndent;
}

function generalInsertionHandler(pasteRange, textArray, content, currentRow, identifier){
	if(textArray[currentRow].match(identifier)){
		pasteRange = new ace.Range(pasteRange.start.row,0,pasteRange.start.row,0);
		content += "\n";
	}else if(textArray[pasteRange.start.row].trimLeft() != ""){
		content = "\n" + content;
	}else {
		pasteRange = new ace.Range(pasteRange.start.row,0,pasteRange.start.row,Number.MAX_VALUE);
	}
	
	return {
		content : content, 
		range : pasteRange
	}

}

/**
 * {Integer} nb number of spaces wished
 * @returns String containing nb withespaces
 */
function ws(nb){
	return ' '.repeat(nb);
}

//module.exports = { retrievePrimitive, checkFilePromptName };

