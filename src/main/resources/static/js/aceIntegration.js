/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
var ace;

var theme = "monokai";

/*------------------------------------------------------------------	
-------------------------Editor choice------------------------------
-------------------------------------------------------------------*/


function selectScenarioXML() {
	if (checkAceError())
		if (yamlToXml()) {
			document.getElementById("scenarioYAML").setAttribute("hidden", true);
			document.getElementById("scenarioXML").removeAttribute("hidden");
			localCache.set("activeView", "xml");
			var button;
			button = document.getElementById("selectScenarioXML");
			button.disabled = true;
			button = document.getElementById("selectScenarioYAML");
			button.disabled = false;
			
			
			var editor = ace.edit(getActiveEditor());
			editor.session.setOptions({ tabSize: 4, useSoftTabs: false });
		}

}

function selectScenarioYAML() {
	if (checkAceError())
		if (xmlToYaml()) {
			document.getElementById("scenarioXML").setAttribute("hidden", true);
			document.getElementById("scenarioYAML").removeAttribute("hidden");
			localCache.set("activeView", "yaml");
			var button;
			button = document.getElementById("selectScenarioYAML");
			button.disabled = true;
			button = document.getElementById("selectScenarioXML");
			button.disabled = false;
			
			var editor = ace.edit(getActiveEditor());
			editor.session.setOptions({ tabSize: 2, useSoftTabs: true });
			
			ace.edit(editor).getSession().on('change', function(e) {
				var editor = ace.edit(getActiveEditor());
				var map = new Map();
				var eT = editor.getValue();
				eT = eT.replace(/ /g, "");
				var eTA = eT.split("\n");
				var tmpContent, tmpId, tmpSet;
				
				//var markers = editor.session.$frontMarkers;
				//console.log(markers);
				
				for(var i = 0 ; i < eTA.length ; i++){
					tmpContent = eTA[i];
					tmpId = tmpContent;
					if(tmpContent.includes("id:")){
						if(!map.get(tmpId)){
							map.set(tmpId, new Set());
						}
						map.get(tmpId).add(i);
					}
				}
				
				map.forEach((value, key) => {
					if(value.size > 1){
						for(let row of value){
							//editor.session.addGutterDecoration(row, "duplicatedId");
							//editor.session.$frontMarkers[markers.length] =
							editor.session.addMarker(new ace.Range(row, 0, row, Number.MAX_VALUE), "duplicatedId", "text", true); 
						}
					}
				})
			})
		}
}

function selectTestPlanProperties() {
	if (checkAceError())
		if (yamlToProperties()) {
			document.getElementById("testPlanYAML").setAttribute("hidden", true);
			document.getElementById("testPlanProperties").removeAttribute("hidden");
			localCache.set("activeView", "properties");
			var button;
			button = document.getElementById("selectTestPlanProperties");
			button.disabled = true;
			button = document.getElementById("selectTestPlanYAML");
			button.disabled = false;
			
		}
}

function selectTestPlanYAML() {
	if (checkAceError())
		if (propertiesToYaml()) {
			document.getElementById("testPlanProperties").setAttribute("hidden", true);
			document.getElementById("testPlanYAML").removeAttribute("hidden");
			localCache.set("activeView", "yaml");
			var button;
			button = document.getElementById("selectTestPlanYAML");
			button.disabled = true;
			button = document.getElementById("selectTestPlanProperties");
			button.disabled = false;
			
			
			var editor = ace.edit(getActiveEditor());
			editor.session.setOptions({ tabSize: 2, useSoftTabs: true });
		}
}


/*------------------------------------------------------------------	
-------------------------Editor opener------------------------------
-------------------------------------------------------------------*/

var editor = new Object();

function openISACEditors() {
	localCache.set("activeView", "xml");
	
	var containers = document.getElementById("editorContent");
	var editorChoice = document.getElementById("switchEditorBar");

	if (document.getElementById("executeTest")) {
		containers.removeChild(document.getElementById("executeTest"));
	}

	//Creating editor
	var scenarioXML = document.createElement("div");
	createEditor(containers, scenarioXML, "scenarioXML", "ace/mode/xml");
	var scenarioYAML = document.createElement("div");
	createEditor(containers, scenarioYAML, "scenarioYAML", "ace/mode/yaml");
	scenarioYAML.setAttribute("hidden", true);

	//Creating buttons
	var chooseXML = document.createElement("button");
	createButton(editorChoice, chooseXML, "XML", "selectScenarioXML", function onclick() { selectScenarioXML() });
	var chooseYAML = document.createElement("button");
	createButton(editorChoice, chooseYAML, "YAML", "selectScenarioYAML", function onclick() { selectScenarioYAML() });
	
	chooseXML.disabled = true;
	
	if (deployed) {
		var executeTest = document.createElement("button");
		createButton(editorChoice, executeTest, getLocalizedText("test.access"), "deployTestButton", function onclick() { showModalTestDeployed() }, "btn-success");
	}
	setTimeout(function() { ace.edit("scenarioXML").getSession().foldToLevel(2); }, 100);

}

function openTextEditors() {
	var containers = document.getElementById("editorContent");
	var editorChoice = document.getElementById("switchEditorBar");
	var textContainer = document.createElement("div");

	if (document.getElementById("executeTest")) {
		containers.removeChild(document.getElementById("executeTest"));
	}
	if (deployed) {
		var executeTest = document.createElement("button");
		createButton(editorChoice, executeTest, getLocalizedText("test.access"), "deployTestButton", function onclick() { showModalTestDeployed() }, "btn-success");
	}
	createEditor(containers, textContainer, "TextEditor", "ace/mode/text");

}

function openPropertiesEditors() {
	localCache.set("activeView", "properties");

	var containers = document.getElementById("editorContent");
	var editorChoice = document.getElementById("switchEditorBar");

	//Creating editor
	var testPlanProperties = document.createElement("div");
	createEditor(containers, testPlanProperties, "testPlanProperties", "ace/mode/properties");
	var testPlanYAML = document.createElement("div");
	createEditor(containers, testPlanYAML, "testPlanYAML", "ace/mode/yaml");
	testPlanYAML.setAttribute("hidden", true);

	//Creating buttons
	var chooseProperties = document.createElement("button");
	createButton(editorChoice, chooseProperties, "Properties", "selectTestPlanProperties", function onclick() { selectTestPlanProperties() });
	var chooseYAML = document.createElement("button");
	createButton(editorChoice, chooseYAML, "YAML", "selectTestPlanYAML", function onclick() { selectTestPlanYAML() });
	
	chooseProperties.disabled = true;
	
	if (deployed) {
		var executeTest = document.createElement("button");
		createButton(editorChoice, executeTest, getLocalizedText("test.access"), "deployTestButton", function onclick() { showModalTestDeployed() }, "btn-success");
	} else {
		var executeTest = document.createElement("button");
		createButton(editorChoice, executeTest, getLocalizedText("test.deploy"), "deployTestButton", function onclick() { checkRegistryServers() }, "btn-success");
	}
}

function createEditor(container, editor, id, mode) {

	editor.setAttribute("id", id);
	editor.setAttribute("class", "specificEditor");
	container.append(editor);
	var editorAce = ace.edit(id);
	editorAce.getSession().setMode(mode);
	editorAce.session.setOptions({ tabSize: 4, useSoftTabs: false });
	reloadEditor(editorAce);
}

function createButton(container, button, name, id, func, buttonType) {
	button.classList.add("btn");
	button.classList.add("btn-primary");
	button.classList.add("btn-sm");
	if (buttonType)
		button.classList.add(buttonType);

	button.innerHTML = name;
	button.setAttribute("id", id);
	button.onclick = func;
	button.style.margin = "5px";
	container.append(button);
}

function reloadEditor(editorLocal) {
	editorLocal.setTheme("ace/theme/" + theme);
	ace.require("ace/ext/language_tools");
	editorLocal.setOptions({
		fontSize: "1.1em",
	});
	editor.editorLocal = editorLocal.getSession().getValue();
	if (getPermissionProject().includes("admin") || getPermissionProject().includes("editor") || !secutriyEnabled) {
		editorLocal.setReadOnly(false);
	} else {
		editorLocal.setReadOnly(true);
	}
	editorLocal.commands.addCommand({
		name: 'save',
		bindKey: { win: "Ctrl-S", mac: "Cmd-S" },
		exec: function(editorLocal) {
			saveAceText();
		}
	});
	editorLocal.selection.clearSelection();
	/*editorLocal.getSession().on("changeFold", function(data,action){
		console.log(data);
		console.log(action);
	});*/


}

function resetEditors() {
	var containers = document.getElementById("editorContent");
	while (containers.lastChild) {
		containers.removeChild(containers.lastChild);
	}
	var editorChoice = document.getElementById("switchEditorBar");
	while (editorChoice.lastChild.id != "saveFileButton" && editorChoice.lastChild.id != "downloadFileButton") {
		editorChoice.removeChild(editorChoice.lastChild);
	}
}

function checkAceError() {
	var activeEditor = ace.edit(getActiveEditor());
	var listAnnotations = activeEditor.getSession().getAnnotations();
	var textAnnotation = "";
	listAnnotations.forEach(annotation => {
		if (annotation.type == "error") {
			textAnnotation += "Error at line : " + annotation.row + ": " + annotation.text + "\n";
		}
	});
	if (textAnnotation) {
		alert(textAnnotation);
		return false;
	} else {
		return true;
	}
}

function createFileDisplay(file) {
	var displayFileName = document.createElement("div");
	displayFileName.setAttribute("id", "displayFileName");
	displayFileName.classList.add("btn");
	//displayFileName.classList.add("btn-outline-info");
	displayFileName.innerHTML = file;
	displayFileName.classList.add("displayFileNameButton")
	return displayFileName;
}

function LoadFileAce(text, path) {
	resetEditors();
	document.getElementById("switchEditorBar").append(createFileDisplay(path.substring(path.lastIndexOf("/") + 1)));
	if (path.length - 4 > 0 && path.substring(path.length - 4).includes(".xis")) {
		openISACEditors();
		var editorXml = ace.edit("scenarioXML");
		editorXml.getSession().setValue(text);
		editorXml.getSession().foldToLevel(2);
	}
	else if (path.length - 4 > 0 && path.substring(path.length - 4).includes(".ctp")) {
		openPropertiesEditors();
		var editorProperties = ace.edit("testPlanProperties");
		editorProperties.getSession().setValue(text);
	}
	else if (path.length - 4 > 0 && path.substring(path.length - 4).includes(".csv")) {
		openTextEditors()
		var editorText = ace.edit("TextEditor");
		//we add the button that display the csv window
		addCsvButtonAndSubmenu();
		editorText.getSession().setValue(text);
	}
	else if (filesList.includes(path + ".classname")) {
		openTextEditors()
		var editorText = ace.edit("TextEditor");
		//we add the button that display the csv window
		addCsvButtonAndSubmenu();
		editorText.getSession().setValue(text);
	}
	else {
		openTextEditors()
		var editorText = ace.edit("TextEditor");
		editorText.getSession().setValue(text);
	}
	if (document.getElementById("saveFileButton").getAttribute("hidden")) {
		document.getElementById("saveFileButton").removeAttribute("hidden");
	}

	if (document.getElementById("downloadFileButton").getAttribute("hidden")) {
		document.getElementById("downloadFileButton").removeAttribute("hidden");
	}
}


function addCsvButtonAndSubmenu() {

	editorChoice = document.getElementById("switchEditorBar");

	var parentDiv = document.createElement("div");
	parentDiv.id = "parentCsvDiv";
	parentDiv.classList.add("dropdown");

	//button parameters
	var displayCsv = document.createElement("button");
	displayCsv.classList.add("contextualMenu");
	var editorChoice = document.getElementById("switchEditorBar");

	displayCsv.classList.add("btn");
	displayCsv.classList.add("btn-primary");
	displayCsv.classList.add("btn-sm");

	displayCsv.innerHTML = "CSV";
	displayCsv.setAttribute("id", "displayCsv");
	displayCsv.addEventListener('click', function() { displayTableCsvButton(); });
	displayCsv.style.margin = "5px";

	//submenu creation 
	var childDiv = document.createElement("div");
	childDiv.id = "dropdownCsv";
	childDiv.classList.add("dropdown-contentCsv");
	childDiv.style.position = "fixed";

	//checkbox
	createCheckbox("checkboxLabels", getLocalizedText("csv.label"), true, childDiv);

	//radiobutton
	var h5 = document.createElement("h5");
	h5.classList.add("contextualMenu");
	h5.style.fontSize = "13px";
	h5.style.fontFamily = "sans-serif";
	h5.innerHTML = getLocalizedText("csv.separator");
	childDiv.append(h5);
	createRadio("radioTab", getLocalizedText("csv.tab"), "csvDisplay", "\t", true, childDiv);
	createRadio("radioComma", getLocalizedText("csv.comma"), "csvDisplay", ",", false, childDiv);
	createRadio("radioSemicolon", getLocalizedText("csv.semicolon"), "csvDisplay", ";", false, childDiv);
	createRadio("radioSpace", getLocalizedText("csv.space"), "csvDisplay", " ", false, childDiv);
	createInputText("radioCustom", getLocalizedText("csv.customChoice"), "csvDisplay", "csvInput", childDiv);

	//add elements to their parents
	parentDiv.append(displayCsv);
	parentDiv.append(childDiv);
	editorChoice.append(parentDiv);
}
/**
 * This function adds a radiobutton to the dropdown element
 */
function createRadio(radiobuttonId, radiobuttonInnerHtml, radioName, value, isChecked, parentElement) {
	var div = document.createElement("div");
	div.classList.add("contextualMenu");
	var radiobutton = document.createElement("input");
	radiobutton.type = "radio";
	radiobutton.name = radioName;
	radiobutton.value = value;
	if (isChecked == true) {
		radiobutton.checked = true;
	}
	radiobutton.id = radiobuttonId;
	var labelRadiobutton = document.createElement("label");
	labelRadiobutton.htmlFor = radiobuttonId;
	labelRadiobutton.innerHTML = radiobuttonInnerHtml;

	div.append(radiobutton);
	div.append(labelRadiobutton);
	parentElement.append(div);

}
/**
 * This function adds a radiobutton and an input text to the dropdown element
 */
function createInputText(radiobuttonId, radiobuttonInnerHtml, radioName, inputId, parentElement) {
	var div = document.createElement("div");
	div.classList.add("contextualMenu");
	var radiobutton = document.createElement("input");
	radiobutton.type = "radio";
	radiobutton.name = radioName;
	radiobutton.id = radiobuttonId;

	var labelRadiobutton = document.createElement("label");
	labelRadiobutton.htmlFor = radiobuttonId;
	labelRadiobutton.innerHTML = radiobuttonInnerHtml;

	var inputText = document.createElement("input");
	inputText.type = "text";
	inputText.id = inputId;
	inputText.addEventListener('click', function() { document.getElementById(radiobuttonId).checked = true; });

	div.append(radiobutton);
	div.append(labelRadiobutton);
	div.append(inputText);
	parentElement.append(div);
}

/**
 * This function adds checkboxs to the dropdown element
 */
function createCheckbox(checkboxId, checkboxInnerHtml, isChecked, parentElement) {
	var div = document.createElement("div");
	div.classList.add("contextualMenu");
	var checkBox = document.createElement("input");
	checkBox.type = "checkbox";
	checkBox.id = checkboxId;
	if (isChecked == true) {
		checkBox.checked = true;
	}
	var labelCheckbox = document.createElement("label");
	labelCheckbox.htmlFor = checkboxId;
	labelCheckbox.innerHTML = checkboxInnerHtml;

	div.append(checkBox);
	div.append(labelCheckbox);
	parentElement.append(div);
}

/**
 * 
 */

function getActiveEditor() {
	var containerChilds = document.getElementById("editorContent").childNodes;
	for (e in containerChilds) {
		if (containerChilds[e].attributes) {
			if (!containerChilds[e].hasAttribute("hidden")) {
				return containerChilds[e].id;
			}
		}
	}
}

function saveAceText() {
	if (document.getElementById("editorContent").getAttribute("file") &&
		document.getElementById("editorContent").getAttribute("file") != "") {
		var editorToSave = "";
		if (getActiveEditor() == "scenarioYAML") {
			yamlToXml();
			editorToSave = ace.edit("scenarioXML");
		} else if (getActiveEditor() == "scenarioXML") {
			editorToSave = ace.edit("scenarioXML");
		} else if (getActiveEditor() == "testPlanYAML") {
			yamlToProperties();
			editorToSave = ace.edit("testPlanProperties");
		} else if (getActiveEditor() == "testPlanProperties") {
			editorToSave = ace.edit("testPlanProperties");
		} else {
			editorToSave = ace.edit("TextEditor");
		}
		var content = editorToSave.getSession().getValue();
		var file = document.getElementById("editorContent").getAttribute("file");
		$.ajax({
			url: "/saveFile",
			type: 'POST',
			dataType: "text",
			data: file + '\n' + content.trim(),
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(response) {
				showMessage(response);
			},
		});
	} else {

		var file = document.getElementById("editorContent").getAttribute("file");
		showMessage("Error : no file open");
	}

}


/*------------------------------------------------------------------	
---------------------Ace and website Theme--------------------------
--------------------------------------------------------------------*/

var currentTheme = getCookie("theme");
if (currentTheme == "dark") {
	swapStyleSheet("css/bootstrap-dark.min.css")
	currentTheme = "dark";
	theme = "monokai";
//	var filetree = document.getElementById("ulFileTreeContainer");
//	filetree.style.color = "white";
} else if (currentTheme == "light") {
	swapStyleSheet("css/bootstrap-light.min.css");
	currentTheme = "light";
	theme = "chrome";
//	var filetree = document.getElementById("ulFileTreeContainer");
//	filetree.style.color = "black";
} else {
	document.cookie = "theme=dark; expires=Thu, 01 Jan 2038 00:00:00 UTC; path=/;";
	currentTheme = "dark";
	theme = "monokai";
//	var filetree = document.getElementById("ulFileTreeContainer");
//	filetree.style.color = "white";
}

function swapTheme(element) {
	if (currentTheme == "light") {
		swapStyleSheet("css/bootstrap-dark.min.css")
		currentTheme = "dark";
		element.src = "/images/SelectLightTheme.png";
		document.cookie = "theme=dark; expires=Thu, 01 Jan 2038 00:00:00 UTC; path=/;";
		theme = "monokai";
		var editor = ace.edit(getActiveEditor());
		editor.setTheme("ace/theme/" + theme);
		//change filetree color 
		var filetree = document.getElementById("ulFileTreeContainer");
		filetree.style.color = "white";

	} else if (currentTheme == "dark") {
		swapStyleSheet("css/bootstrap-light.min.css");
		currentTheme = "light";
		element.src = "/images/SelectDarkTheme.png";
		document.cookie = "theme=light; expires=Thu, 01 Jan 2038 00:00:00 UTC; path=/;";
		theme = "chrome";
		var editor = ace.edit(getActiveEditor());
		editor.setTheme("ace/theme/" + theme);
		//change filetree color 
		var filetree = document.getElementById("ulFileTreeContainer");
		filetree.style.color = "black";

	}
}

function fileTreeColor(){
	var filetree = document.getElementById("ulFileTreeContainer");
	if (currentTheme == "dark") {
		filetree.style.color = "white";
	}
	else if (currentTheme == "white"){
		fileTree.color = "black";
	}
}

function getTheme() {
	return currentTheme;
}

function swapStyleSheet(sheet) {
	document.getElementById("pagestyle").setAttribute("href", sheet);
}


function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

