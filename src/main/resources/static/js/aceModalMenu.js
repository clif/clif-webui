/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */


/*------------------------------------------------------------------	
------------------------Modal Menu Statement------------------------
-------------------------------------------------------------------*/

/**
 * This function is called when the user clicks on the item if, while or preemptive on the context menu.
 * It prepares the modal window about statements that will be displayed on the screen and binds the insertion of content
 * to the validate button.
 * @param {String} statementType - if, while or preemptive
 */
function initModalAddTest(statementType) {
	clearModalSelect(["statementTestSelect", "statementPluginIdSelect", "statementPluginNameSelect"])
	var pluginArray = scanPlugins("tests");
	pluginArray.sort();
	var uniqueActivePlugin = pluginArray.filter(getUniqueVal);
	addModalSelectOptionInit(uniqueActivePlugin, "statementPluginNameSelect");
	document.getElementById("modalFormChooseStatementTest").removeAttribute("hidden");
	var pluginName = document.getElementById("statementPluginNameSelect");
	var testName = document.getElementById("statementTestSelect");
	var idPlugin = document.getElementById("statementPluginIdSelect");
	document.getElementById("statementValidate").onclick = function() { 
			try{
				insertStatement(statementType, pluginName.options[pluginName.selectedIndex].text, testName.options[testName.selectedIndex].text, idPlugin.options[idPlugin.selectedIndex].text)
				$("#modalFormChooseStatementTest").modal('hide');
			}catch(e){
				alert(e);
			}
		};
}

/**
 *This function is triggered when the user changes its selection inside the plugin name list in the modalFormChooseStatementTest menu.
 *It changes the displayed tests and plugin ids according to match the plugin name
 */
function modalChangeIdTest() {
	clearModalSelect(["statementTestSelect", "statementPluginIdSelect"])
	var pluginName = document.getElementById("statementPluginNameSelect");
	var pluginNameText = pluginName.options[pluginName.selectedIndex].text;
	var arrayPluginId = scanPluginId(pluginNameText);
	var arrayTest = getTest(pluginNameText);
	arrayTest.sort();
	arrayPluginId.sort();
	//check if an id is duplicated
	var setPluginId = new Set(arrayPluginId);
	if (arrayPluginId.length !== setPluginId.size) {
		var duplicates = "";
		const tempArray = [...arrayPluginId].sort();

		for (let i = 0; i < tempArray.length; i++) {
			if (tempArray[i + 1] === tempArray[i]) {
				duplicates += tempArray[i] + ", ";
			}
		}
		$('#modalFormChooseStatementTest').modal('hide');
		showPopup(getLocalizedText("error.plugin.id").replace("%1", pluginNameText).replace("%2", duplicates));
	}
	else {
		addModalSelectOption("statementPluginIdSelect", arrayPluginId);
		addModalSelectOption("statementTestSelect", arrayTest);
	}
}

/**
 *This function is used to retrieve the test associated to a plugin.
 *It is used to actualise the list inside the modal window modalFormChooseStatementTest
 *@param {String} pluginName - The selected plugin name
 *@returns {Array} arrayTest - A list of the different tests
 */
function getTest(pluginName) {
	var cache = localCache.get("pluginInfo");
	var pluginNumber = cache["plugins"].length;
	var arrayTest = [];

	for (var i = 0; i < pluginNumber; i++) {
		if (pluginName == cache["plugins"][i]["name"]) {
			if (cache["plugins"][i].hasOwnProperty("tests")) {
				for (var j = 0; j < cache["plugins"][i]["tests"].length; j++) {
					arrayTest.push(cache["plugins"][i]["tests"][j]["name"]);
				}
			}
		}
	}
	return arrayTest;
}

/*------------------------------------------------------------------	
-------------------------Modal add Plugin--------------------------
-------------------------------------------------------------------*/
/**
 * This function is called when the user clicks on the item plugin on the context menu.
 * It prepares the modal window about plugins that will be displayed on the screen and binds the insertion of content
 * to the validate button.
 * @param {String} statementType - if, while or preemptive
 */
function initModalAddPlugin() {
	//clear fields
	clearModalSelect(["pluginNameSelect"]);
	document.getElementById("pluginInputId").value = "";

	var arrayAvailablePlugin = getAvailablePlugin();
	addModalSelectOptionInit(arrayAvailablePlugin, "pluginNameSelect")
	document.getElementById("modalChoosePlugin").removeAttribute("hidden");
	var pluginName = document.getElementById("pluginNameSelect");
	document.getElementById("pluginValidate").onclick = function() { 
		try{
			addPluginEditor(pluginName.options[pluginName.selectedIndex].text, document.getElementById("pluginInputId").value);
			$("#modalChoosePlugin").modal('hide');
		}catch(e){
			alert(e);
		}
	};
}

function modalPluginChangeInputId() {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	
	var inputId = document.getElementById("pluginInputId");
	var pluginName = document.getElementById("pluginNameSelect");
	var stringPluginName = pluginName.options[pluginName.selectedIndex].text;
	var pluginId = localCache.get("activeView")  == "xml" ? choosePluginId(stringPluginName) : checkId(editorText, stringPluginName + "_");
	inputId.value = pluginId;
}

/**
 *This function returns an array that contains the plugins names contained inside the cache
 *@returns {Array} - The plugins contained in the cache
 */
function getAvailablePlugin() {
	var arrayAvailablePlugin = [];
	var cache = localCache.get("pluginInfo");
	var pluginNumber = cache["plugins"].length;
	for (var i = 0; i < pluginNumber; i++) {
		arrayAvailablePlugin.push(cache["plugins"][i]["name"]);
	}
	return arrayAvailablePlugin;
}

/*------------------------------------------------------------------	
-------------------------Modal add options--------------------------
-------------------------------------------------------------------*/

/**
 *This function adds options to a select tag
 *@param {String} elementId - the id of the select tag
 *@param {Array} optionArray - the names of the different options
 */
function addModalSelectOption(elementId, optionArray) {
	var parent = document.getElementById(elementId);
	optionArray.sort();
	var firstItem = true;
	for (var i = 0; i < optionArray.length; i++) {
		var option = document.createElement("option");
		option.setAttribute('value', i);
		if (firstItem == true) {
			option.setAttribute('selected', '');
			firstItem = false;
		}
		var pluginId = document.createTextNode(optionArray[i]);
		option.appendChild(pluginId);
		parent.appendChild(option);
	}
}

/**
 *This function adds options to a select tag, but without selectionning automatically an option
 *@param {Array} optionArray - the names of the different options
 *@param {String} elementId - the id of the select tag
 */
function addModalSelectOptionInit(optionArray, elementId) {
	var parent = document.getElementById(elementId);
	for (let i = 0; i < optionArray.length; i++) {
		var option = document.createElement("option");
		option.setAttribute('value', i);
		var pluginId = document.createTextNode(optionArray[i]);
		option.appendChild(pluginId);
		parent.appendChild(option);
	}
}

/*------------------------------------------------------------------	
------------------------Modal add Primitive-------------------------
-------------------------------------------------------------------*/
/**
 *This function is called when the user right click on the context menu, then clicks on control, timer or sample.
 *
 *@param {String} primitiveType - the primitive type, control, sample or timer
 */
function initModalAddPrimitive(primitiveType) {
	//display the menu
	clearModalSelect(["primitivePluginNameSelect", "primitivePrimitiveNameSelect", "primitivePluginIdSelect"]);
	var pluginArray = scanPlugins(primitiveType);
	pluginArray.sort();
	var uniqueActivePlugin = pluginArray.filter(getUniqueVal);
	addModalSelectOptionInit(uniqueActivePlugin, "primitivePluginNameSelect");
	document.getElementById("modalFormChoosePrimitive").removeAttribute("hidden");

	//change the menu title
	var primitiveTitle = document.getElementById("modalPrimitiveTitle");
	if (primitiveType == "controls") {
		primitiveTitle.innerHTML = getLocalizedText("submenu.control");
	}
	else if (primitiveType == "timers") {
		primitiveTitle.innerHTML = getLocalizedText("submenu.timer");
	}
	else if (primitiveType == "samples") {
		primitiveTitle.innerHTML = getLocalizedText("submenu.sample");
	}

	//add the primitive type to the local cache
	localCache.set("modalPrimitiveType", primitiveType);

	//retrieve the name of the chosen option
	var pluginName = document.getElementById("primitivePluginNameSelect");
	var primitiveName = document.getElementById("primitivePrimitiveNameSelect");
	var idPlugin = document.getElementById("primitivePluginIdSelect");

	document.getElementById("primitiveValidate").onclick = function() { 
		try{
			insertPrimitive(primitiveType, pluginName.options[pluginName.selectedIndex].text, primitiveName.options[primitiveName.selectedIndex].text, idPlugin.options[idPlugin.selectedIndex].text);
			$("#modalFormChoosePrimitive").modal('hide');
		}catch(e){
			alert(e);
		}
		 
		};
}

/**
 *This function is called whenever the user changes the plugin selected in the modal primitive menu.
 *It changes the primitive and plugin id lists to match the selected plugin

 */
function modalPrimitiveChangeOption() {
	clearModalSelect(["primitivePrimitiveNameSelect", "primitivePluginIdSelect"])
	var pluginName = document.getElementById("primitivePluginNameSelect");
	var pluginNameText = pluginName.options[pluginName.selectedIndex].text;
	var arrayPluginId = scanPluginId(pluginNameText);
	var primitiveType = localCache.get("modalPrimitiveType");
	var arrayPrimitive = getPrimitive(pluginNameText, primitiveType);
	arrayPrimitive.sort();
	arrayPluginId.sort();
	//check if an id is duplicated
	var setPluginId = new Set(arrayPluginId);
	if (arrayPluginId.length !== setPluginId.size) {
		var duplicates = "";
		const tempArray = [...arrayPluginId].sort();

		for (let i = 0; i < tempArray.length; i++) {
			if (tempArray[i + 1] === tempArray[i]) {
				duplicates += tempArray[i] + ", ";
			}
		}
		$('#modalFormChooseStatementTest').modal('hide');
		showPopup(getLocalizedText("error.plugin.id").replace("%1", pluginNameText).replace("%2", duplicates));
	}
	else {
		addModalSelectOption("primitivePrimitiveNameSelect", arrayPrimitive);
		addModalSelectOption("primitivePluginIdSelect", arrayPluginId);
	}
}

/**
 *This function is used to retrieve the primitives associated to a plugin and a primitive type.
 *It is used to actualise the list inside the modal window modalFormChoosePrimitive
 *@param {String} pluginName - The selected plugin name
 *@param {String} primitiveType - The primitive type (control, sample, timer)
 *@returns {Array} arrayTest - A list of the different tests
 */
function getPrimitive(pluginName, primitiveType) {
	var cache = localCache.get("pluginInfo");
	var arrayPrimitive = [];
	var endFor = false;

	for (var i = 0; i < cache["plugins"].length && endFor == false; i++) {
		if (cache["plugins"][i]["name"] == pluginName) {
			if(cache["plugins"][i][primitiveType] != undefined){
				for (var j = 0; j < cache["plugins"][i][primitiveType].length; j++) {
						arrayPrimitive.push(cache["plugins"][i][primitiveType][j]["name"]);
					
				}
			}
			endFor = true;
		}
	}
	return arrayPrimitive;
}

/*------------------------------------------------------------------	
----------------------Modal choose behavior id----------------------
-------------------------------------------------------------------*/

/**
 *This function is called when the user right click to display the context menu, then clicks on new loadprofile.
 *It displays a modal window and retrieves a behavior id
 *One group is associated to a unique behavior Id
 */
function modalLoadprofileId() {
	var isXML = localCache.get("activeView") == "xml";
	
	clearModalSelect(["modalFormChooseBehaviorIdSelect"]);
	var behaviorArray = isXML ? scanBehaviorId() : scanBehaviorIdYAML();
	var loadprofileIdArray = isXML ? scanLoadprofileIdXML() : scanLoadprofileIdYAML();
	behaviorArray.sort();
	var uniqueActivePlugin = behaviorArray.filter(getUniqueVal);

	//obtain an array that contains behavior ids that are not associated to a group
	var unusedBehaviorId = [];
	for (var i = 0; i < uniqueActivePlugin.length; i++) {
		if (loadprofileIdArray.indexOf(uniqueActivePlugin[i]) == -1) {
			unusedBehaviorId.push(uniqueActivePlugin[i]);
		}
	}
	if (unusedBehaviorId.length > 0) {
		addModalSelectOptionInit(unusedBehaviorId, "modalFormChooseBehaviorIdSelect");
		document.getElementById("modalFormChooseBehaviorId").removeAttribute("hidden");
		var behaviorId = document.getElementById("modalFormChooseBehaviorIdSelect");
		document.getElementById("modalFormChooseBehaviorIdValidate").onclick = function() { insertLoadprofile(behaviorId.options[behaviorId.selectedIndex].text) };
		$('#modalFormChooseBehaviorId').modal('show');
		$('#modalFormChooseBehaviorId').removeAttr('hidden');
	}
	else {
		showPopup(getLocalizedText("error.loadprofile.notAvailable"));
	}

}

function scanLoadprofileIdXML() {
	var arrayLoadprofileId = [];
	var textAsObject = stringToObj();
	if (textAsObject["loadprofile"].hasOwnProperty("group")) {
		var useCount = textAsObject["loadprofile"]["group"].length;
		if (useCount != undefined) {
			for (var i = 0; i < useCount; i++) {
				arrayLoadprofileId.push(textAsObject["loadprofile"]["group"][i]["behavior"]);
			}
		}
		else {
			arrayLoadprofileId.push(textAsObject["loadprofile"]["group"]["behavior"])
		}
	}
	return arrayLoadprofileId;
}

function scanLoadprofileIdYAML() {
	var profiles = [];
	
	var e = ace.edit(getActiveEditor());
	var eT = e.getValue();
	var eTA = eT.split("\n");
	//var eTA_spaceless = eT.replace(/[^\S\n]/g,'').split("\n");
	
	var flag = false;
	for(var i=0 ; i<eTA.length ; i++){
		if(flag){
			if(eTA[i].startsWith("      - behavior")){
				profiles.push(eTA[i].substring(16,Number.MAX_VALUE).replace(/[ :]/g,''));
			} else if(eTA[i].startsWith("      behavior")){
				profiles.push(eTA[i].substring(14, Number.MAX_VALUE).replace(/[ :]/g,''));
			}
		}else {
			flag = eTA[i].startsWith("  loadprofile");
		}
	}

	return profiles;
}

function scanBehaviorIdYAML(){
	var behaviors = [];
	
	var e = ace.edit(getActiveEditor());
	var eT = e.getValue();
	var eTA = eT.split("\n");
	//var eTA_spaceless = eT.replace(/[^\S\n]/g,'').split("\n");
	
	var flag = false;
	for(var i=0 ; i<eTA.length ; i++){
		if(flag){
			if(eTA[i].startsWith("      - id")){
				behaviors.push(eTA[i].substring(10,Number.MAX_VALUE).replace(/[ :]/g,''));
			} else if(eTA[i].startsWith("      id")){
				behaviors.push(eTA[i].substring(8, Number.MAX_VALUE).replace(/[ :]/g,''));
			}
		}else {
			flag = eTA[i].startsWith("    behavior");
		}
	}
	
	return behaviors;
	
}

/*------------------------------------------------------------------	
-----------------------------Clear Menu-----------------------------
-------------------------------------------------------------------*/

/**
 *This function clears a select tag of a modal menu
 *@param {String} arrayId - the id of the select tag that we want to clear
 */
function clearModalSelect(arrayId) {
	for (var i = 0; i < arrayId.length; i++) {
		var select = document.getElementById(arrayId[i]);
		while (select.firstChild) {
			select.removeChild(select.firstChild);
		}
	}
}
