/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */


/** 
 * This function removes the properties menu
 */
function removeTestPlanContextMenu() {
	contextMenuPropertiesEditor.opacity = "0";
	contextMenuPropertiesEditor.visibility = "hidden";
}

/**
	This function handles the creation of the contextual menu for the ace editor.
	It adapts its position so that the menu doesn't appear outside the screen.
 */
function createTestPlanMenu(x, y) {
	contextMenuPropertiesEditor.visibility = "visible";
	contextMenuPropertiesEditor.opacity = "1";
	resetTestPlanMenu();
	//get window size
	var editorWidth = $('#editorContent').width();
	var handlerWidth = $('#handler').width();
	var leftPanelWidth = $('#leftPanel').width();

	var childHeight = $('#probePropertiesChild').height();
	var editorHeight = $('#editorContent').height();
	var switchEditorHeight = $("#switchEditorBar").height();
	//menu size
	var menuWidth = $('#contextMenuPropertiesEditor').width();
	var menuHeight = $('#contextMenuPropertiesEditor').height();

	//context menu 
	if (x > editorWidth + handlerWidth + leftPanelWidth - menuWidth) {
		contextMenuPropertiesEditor.left = (editorWidth + handlerWidth + leftPanelWidth - menuWidth) + "px";
		var allChild = document.getElementsByClassName('child');
		for (var i = 0; i < allChild.length; i++) {
			allChild[i].style.left = '-105%';
		}
	}
	else {
		contextMenuPropertiesEditor.left = x + "px";
	}

	if (y + menuHeight > editorHeight + switchEditorHeight + 55) {
		contextMenuPropertiesEditor.top = (editorHeight + switchEditorHeight + 55 - menuHeight - 20) + "px";
		var probeChild = document.getElementById("probePropertiesChild");
		probeChild.style.top = '-300%';
	}
	else {
		if (y + childHeight > editorHeight + switchEditorHeight + 55) {
			var probeChild = document.getElementById("probePropertiesChild");
			probeChild.style.top = '-300%';
		}
		contextMenuPropertiesEditor.top = y + "px";
	}
}

/**
 * This function is called each time the properties context menu is displayed
 * It resets the position of the properties context menu
 */
function resetTestPlanMenu() {
	document.getElementById("probePropertiesChild").innerHTML = "";		
	completePropertiesContextMenu(localCache.get("probeInfo"));
	var allChild = document.getElementsByClassName('child');
	for (var i = 0; i < allChild.length; i++) {
		allChild[i].style.left = '100%';
	}
	var statementChild = document.getElementById("probePropertiesChild");
	statementChild.style.top = '0%';
}

/**
 * This function adds the probes to the properties context menu.
 * It is executed when the window is refreshed
 * @param {JSON} probeInfoCache - a JSON String that contains the data about the probes
 */
function completePropertiesContextMenu(probeInfoCache) {
	var ul = document.getElementById("probePropertiesChild");
	for (let i = 0; i < probeInfoCache["probes"].length; i++) {
		let probeName = probeInfoCache["probes"][i]["name"];
		let probePath = probeInfoCache["probes"][i]["path"];
		let probeHelp = probeInfoCache["probes"][i]["help"];
		var li = document.createElement('li');
		li.classList.add("parent");

		var aTag = document.createElement("a");
		aTag.classList.add("contextualMenu");
		aTag.onclick = function() { handleInsertProbe(probeName, probePath, probeHelp) };

		var div = document.createElement("div");
		div.innerHTML = probeName;

		aTag.appendChild(div);
		li.appendChild(aTag);
		ul.appendChild(li);
	}
}

function handleInsertServer() {
	localCache.get("activeView") == "properties" ? handleInsertServerPROPERTIES() : handleInsertServerYAML();
}

function handleInsertServerPROPERTIES(probeName, probePath, probeHelp) {
	findPositionAndInsertPROPERTIES(editor, editorText, "") // none available
}

function handleInsertServerYAML() {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	
	var id = checkId(editorText, "clif_");
	
	//create the text to insert
	var firstLine = "    - name: " + id;
	var secondLine = "      injectors:";
	var thirdLine = "      probes:";
	var textToInsert = firstLine + "\n" + secondLine + "\n" + thirdLine;
	
	findPositionAndInsertYAML(editor, editorText, textToInsert, "server");
}

function handleInsertProbe(probeName, probePath, probeHelp) {
	localCache.get("activeView") == "properties"  ? handleInsertProbePROPERTIES(probeName, probePath, probeHelp) : handleInsertProbeYAML(probeName, probePath, probeHelp);
}

/**
 * This function prepares the probes-related text that will be inserted in the properties editor
 * @param {String} probeName - the name of the probe
 * @param {String} probePath - the class path of the probe
 * @param {String} probeHelp - indications about the probe arguments
 */
function handleInsertProbePROPERTIES(probeName, probePath, probeHelp) {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var bladeNumber = findBladeNumber(editorText);
	var firstLine = "blade." + bladeNumber + ".id=" + probeName + bladeNumber;
	var secondLine = "blade." + bladeNumber + ".probe=" + probePath;
	var thirdLine = "blade." + bladeNumber + ".server=local host";
	var fourthLine = "blade." + bladeNumber + ".argument=" + probeHelp;
	var fifthLine = "blade." + bladeNumber + ".comment=";
	var textToInsert = "\n" + firstLine + "\n" + secondLine + "\n" + thirdLine + "\n" + fourthLine + "\n" + fifthLine + "\n";
	findPositionAndInsertPROPERTIES(editor, editorText, textToInsert)
}

/**
 * This function prepares the probes-related text that will be inserted in the properties editor
 * @param {String} probeName - the name of the probe
 * @param {String} probePath - the class path of the probe
 * @param {String} probeHelp - indications about the probe arguments
 */
function handleInsertProbeYAML(probeName, probePath, probeHelp) {
	
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	
	var nameId = checkId(editorText, probeName + "_"); // generate unique identifier
	
	var firstLine = "        - id: " + nameId;
	var secondLine = "          class: " + probePath;
	var thirdLine = "          argument: " + probeHelp;
	var fourthLine = "          comment: ";
	var textToInsert = firstLine + "\n" + secondLine + "\n" + thirdLine + "\n" + fourthLine;
	
	findPositionAndInsertYAML(editor, editorText, textToInsert, "probes");
}

function handleInsertInjector() {
	localCache.get("activeView") == "properties"  ? handleInsertInjectorPROPERTIES() : handleInsertInjectorYAML();
}

/**
 * This function prepares the inector-related text that will be inserted in the properties editor
 */
function handleInsertInjectorPROPERTIES() {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var bladeNumber = findBladeNumber(editorText);
	
	//retrieve the filename and change ctp to xis
	var fileName = document.getElementById("displayFileName").innerHTML;	
	fileName = fileName.substring(0, fileName.lastIndexOf(".ctp")) + ".xis";
	
	//create the text to insert
	var firstLine = "blade." + bladeNumber + ".id=injector" + bladeNumber;
	var secondLine = "blade." + bladeNumber + ".injector=IsacRunner";
	var thirdLine = "blade." + bladeNumber + ".server=local host";
	var fourthLine = "blade." + bladeNumber + ".argument=" + fileName;
	var fifthLine = "blade." + bladeNumber + ".comment=";
	var textToInsert = "\n" + firstLine + "\n" + secondLine + "\n" + thirdLine + "\n" + fourthLine + "\n" + fifthLine + "\n";
	findPositionAndInsertPROPERTIES(editor, editorText, textToInsert)
}


/**
 * This function prepares the inector-related text that will be inserted in the properties editor
 */
function handleInsertInjectorYAML() {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	
	//retrieve the filename and change ctp to xis
	var fileName = document.getElementById("displayFileName").innerHTML;	
	fileName = fileName.substring(0, fileName.lastIndexOf(".ctp"));
	var nameId = checkId(editorText, "injector_"); // generate unique identifier
	fileName += ".xis";
	
	//create the text to insert
	var firstLine = "        - id: " + nameId;
	var secondLine = "          class: IsacRunner";
	var thirdLine = "          argument: " + fileName;
	var fourthLine = "          comment: ";
	var textToInsert = firstLine + "\n" + secondLine + "\n" + thirdLine + "\n" + fourthLine;
	
	findPositionAndInsertYAML(editor, editorText, textToInsert, "injectors");
}

/**
 * This function retrieves the cursor position and determines where the text will be inserted
 * @param {Object} editor - The ace editor object
 * @param {String} editorText - The text contained in the ace editor
 * @param {String} textToInsert - The text that will be inserted
 */
function findPositionAndInsertPROPERTIES(editor, editorText, textToInsert) {
	var cursorPosition = editor.getCursorPosition();
	var Range = ace.require('ace/range').Range;
	var cursorLine = cursorPosition["row"];
	var textArray = editorText.split("\n");
	var lineToInsert = 0;

	var stopSearching = false;
	while (stopSearching == false) {
		if (cursorLine >= textArray.length) {
			stopSearching = true;
			textToInsert = "\n" + textToInsert;
			lineToInsert = cursorLine;
			editor.session.replace(new Range(lineToInsert, Number.MAX_VALUE, lineToInsert, Number.MAX_VALUE), textToInsert);

		}
		else if (textArray[cursorLine] == "") {
			stopSearching = true;
			lineToInsert = cursorLine;
			editor.session.replace(new Range(lineToInsert, 0, lineToInsert, 0), textToInsert);
		}
		// We'll suppose there's no empty line between 2 properties of the same blade
		// and that there will be one empty line between 2 different blades
		else if (!textArray[cursorLine].includes("blade.")) { 
			stopSearching = true;
			lineToInsert = cursorLine;
			editor.session.replace(new Range(lineToInsert, Number.MAX_VALUE, lineToInsert, Number.MAX_VALUE), textToInsert);
		}
		else {
			cursorLine++;
		}
	}
}

/**
 * This function browses a property file and find the first blade number that is not already used
 * @param {String} editorText : the text contained in the ace editor
 * @return {number} The first unused blade number
 */
function findBladeNumber(editorText) {
	var i = 0;
	var bladeNumberFound = false;
	var bladeNumber = "";
	while (bladeNumberFound == false) {
		if (editorText.includes("blade." + i + ".")) {
			i++;

		}
		else {
			bladeNumber = i;
			bladeNumberFound = true;
		}
	}
	return bladeNumber;
}

/**
 * This function retrieves the cursor position and determines where the text will be inserted
 * @see testPlanMenuHandlerYAML which initially delimits the cases of insertions
 * @param {Object} editor - The ace editor object
 * @param {String} editorText - The text contained in the ace editor
 * @param {String} textToInsert - The text that will be inserted
 * @param {String} componentType - Specifies if it's a probe, injector or server
 */
function findPositionAndInsertYAML(editor, editorText, textToInsert, componentType) {
	var cursorRow = editor.getCursorPosition().row;
	//default value is at the end of the line/next row where the cursor is: 
	var pasteRange = new ace.Range(cursorRow, Number.MAX_VALUE, cursorRow, Number.MAX_VALUE);
	var find = false;
	
	var editorTextWithoutSpaces = editorText.replace(/[^\S\n]/g,'');
	var textArray = editorTextWithoutSpaces.split("\n");
	
	// when it's a huge selection, we replace the text selected: 
	if(editor.getSelectionRange().start.row != editor.getSelectionRange().end.row){ 
		pasteRange = new ace.Range(editor.getSelectionRange().start.row, editor.getSelectionRange().start.column, editor.getSelectionRange().end.row, editor.getSelectionRange().end.column);
		// when the first raw is not entirely selected, we add line break:  
		if(editor.getSelectionRange().start.column != 0){ textToInsert = "\n" + textToInsert; }
		find = true;
		
	} else if(componentType == "server"){
		// if the cursor line is empty, we can add the server here, otherwise it will insert at the next line because it means we are at the " server: " line
		if(textArray[cursorRow] == ""){
			pasteRange = new ace.Range(editor.getSelectionRange().start.row, 0, editor.getSelectionRange().end.row, Number.MAX_VALUE);
		}else{ textToInsert = "\n" + textToInsert; }
		find = true;
	}
	
	// if the paste range isn't find at the previous case: 
	if(!find){
		// We look at the content that comes after to determine if we're in between declarations
		if(textArray.length > cursorRow +1){
			var isEmpty = true;
			var i = cursorRow+1;
			while(isEmpty){
				if(textArray[i] != ""){
					isEmpty = false;
					if(textArray[i].includes("-id:") || textArray[i].includes("probes:") || textArray[i].includes("injectors:")){
						if(textArray[cursorRow] == ""){
							pasteRange = new ace.Range(cursorRow, 0, cursorRow, Number.MAX_VALUE); //default value is at the next row of the cursor
						}else{
							textToInsert = "\n" + textToInsert;
						}
						find = true;
					}
				}
				i++;
				isEmpty &= textArray.length > i;
			}
		}
		
		if(!find){
			// if it's an empty line or it contains only spaces, we have to insert here: 
			if(textArray[cursorRow] == ""){
				pasteRange = new ace.Range(cursorRow, 0, cursorRow, Number.MAX_VALUE);
				find = true;
					
			// if it's the EOF: 
			} else if (textArray.length == cursorRow +1){
				textToInsert = "\n" + textToInsert;
				find = true;
				
			// otherwise we'll backtrack the previous rows to find a start of declaration (and not adding inside one): 
			} else{
				var i = cursorRow;
				textToInsert = "\n" + textToInsert;
				var exceeded = !(cursorRow > 0 && cursorRow <= textArray.length);
				while(!exceeded){
					
					if(textArray[i].includes(componentType + ":")){
						// case where we add the injector at the next line
						new ace.Range(i, Number.MAX_VALUE, i, Number.MAX_VALUE);
						exceeded = true;
					} else if(textArray[i].includes("-id:")){
						pasteRange = new ace.Range(i-1, Number.MAX_VALUE, i-1, Number.MAX_VALUE);
						exceeded = true;
					}else {
						i--;
						exceeded = i < 0;
					}	
				}
			}
		}
	}
		
	editor.session.replace(pasteRange, textToInsert);
}

/**
 * This function generate an unique identifier for a component
 * @param {String} editorText : the text contained in the ace editor
 * @param {String} strComponent : the component which need a new id
 * @return {number} The first unused id
 */
function checkId(editorText, strComponent){
	var i = 0;
	var idFound = false;
	var nameId = strComponent + i;
	
	var eT_spaceless = editorText.replace(/[^\S\n]/g,'');
	
	while (idFound == false) {
		if (eT_spaceless.includes("id:"+nameId)) {
			i++;
			nameId = strComponent + i
		}
		else {
			idFound = true;
		}
	}
	return nameId;
}

/**
 * Manage lock of assistant's options for test plan properties view
 */
function testPlanMenuHandlerPROPERTIES() {
	removeTestPlanContextMenu();
	enableAllTestPlanAssistantsOptions();
}

/**
 * Manage lock of assistant's options for test plan YAML view
 */
function testPlanMenuHandlerYAML() {
	removeTestPlanContextMenu();
	enableAllTestPlanAssistantsOptions();

	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var editorTextWithoutSpaces = editorText.replace(/[^\S\n]/g,'');
	var textArray = editorTextWithoutSpaces.split("\n");
	var cursorRow = editor.getCursorPosition().row;
	
	for(var i = cursorRow ; i > -1 ; i--){
		// if the current line is empty we'll see what's next to determine 
		// if we are at the EOF (end of file) or between 2 servers
		// if it's true we'll let only the add clif server available :
		if(textArray[i] == ""){
			var isEmpty = true;
			var isNextServer = false;
			for(var j = i ; j < editor.session.getLength(); j++){
				isNextServer |= textArray[j].includes("-name:") && j != i;
				isEmpty &= textArray[j] == "";
				if(isNextServer || !isEmpty){
					break;
				}
			}
			if(isEmpty || isNextServer){
				selectedSubmenu = document.getElementById("thisProbePropertiesMenu");
				selectedSubmenu.classList.add("noHover");
				selectedSubmenu = document.getElementById("thisInjectorPropertiesMenu");
				selectedSubmenu.classList.add("noHover");
				selectedSubmenu = document.getElementById("probePropertiesChild");
				selectedSubmenu.classList.add("noHover");
				selectedSubmenu = document.getElementById("probePropertiesChild");
				selectedSubmenu.hidden = true;
				break;
			}
			
		}else if(textArray[i].includes("injectors:")){
			selectedSubmenu = document.getElementById("thisProbePropertiesMenu");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("probePropertiesChild");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("probePropertiesChild");
			selectedSubmenu.hidden = true;
			selectedSubmenu = document.getElementById("thisServerPropertiesMenu");
			selectedSubmenu.classList.add("noHover");
			break;
		}else if(textArray[i].includes("probes:")){
			selectedSubmenu = document.getElementById("thisInjectorPropertiesMenu");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("thisServerPropertiesMenu");
			selectedSubmenu.classList.add("noHover");
			break;
		} else if(textArray[i].includes("server:")){
			selectedSubmenu = document.getElementById("thisProbePropertiesMenu");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("thisInjectorPropertiesMenu");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("probePropertiesChild");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("probePropertiesChild");
			selectedSubmenu.hidden = true;
			break;
		}else if(i == 0 || textArray[i].includes("-name:")){
			selectedSubmenu = document.getElementById("thisProbePropertiesMenu");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("thisInjectorPropertiesMenu");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("probePropertiesChild");
			selectedSubmenu.classList.add("noHover");
			selectedSubmenu = document.getElementById("probePropertiesChild");
			selectedSubmenu.hidden = true;
			selectedSubmenu = document.getElementById("thisServerPropertiesMenu");
			selectedSubmenu.classList.add("noHover");
		}
	}	

}

/**
 * Like the function name say, It enable the test plan assistant's options
 */
function enableAllTestPlanAssistantsOptions(){
	enableContextualSubmenu("thisProbePropertiesMenu");
	enableContextualSubmenu("thisInjectorPropertiesMenu");
	enableContextualSubmenu("probePropertiesChild");
	enableContextualSubmenu("thisServerPropertiesMenu");
	
	var selectedSubmenu = document.getElementById("probePropertiesChild");
	selectedSubmenu.hidden = false;
	
	// Make clif server not available for properties view : 
	selectedSubmenu = document.getElementById("thisServerPropertiesMenu");
	selectedSubmenu.style.display= (localCache.get("activeView") == "properties" ? "none" : "block");
}
