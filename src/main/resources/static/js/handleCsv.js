/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

/**
 * This function splits a given text according to a given separator and the line breaks
 * @param {String} csvText A text retrieved from the editor or from the java post response
 * @param {String} separator The string that will be used to split each line
 */
function stringToTable(csvText, separator) {
	if (separator == null || separator === undefined || separator == "") {
		separator = "\t";
	}
	var splitedCsv = csvText.split("\n");
	var j = 0;
	var splitedCsvLength = splitedCsv.length;
	while (j < splitedCsvLength) {
		if (splitedCsv[j] == "") {
			splitedCsv.splice(j, 1);
			splitedCsvLength--;
		}
		else {
			j++;
		}

	}
	for (var i = 0; i < splitedCsv.length; i++) {
		splitedCsv[i] = splitedCsv[i].split(separator);
	}
	return splitedCsv;
}

/**
 * This function determines how many elements the longest row contains and adds empty columns to the rows that have less elements than the longest row
 * @param {String[]} splitedCsv An array that contain the table rows
 * @return {String[]} An array containing the parameter splitedCsv where each rows have the same length
 */
function uniformRowCsv(splitedCsv) {
	var maxRowElement = 0;
	for (var i = 0; i < splitedCsv.length; i++) {
		if (splitedCsv[i].length > maxRowElement) {
			maxRowElement = splitedCsv[i].length;
		}
	}
	for (var i = 0; i < splitedCsv.length; i++) {
		while (splitedCsv[i].length < maxRowElement) {
			splitedCsv[i].push("");
		}
	}
	return splitedCsv;
}

/**
 * This function determines the length for each column of the longest element
 * @param {String[]} splitedCsv An array that contain the table rows
 * @return {number[]} An array containing the length of the longest element for each column
 */
function maxLengthCol(splitedCsv) {
	var maxLength = [];
	var rowNumber = splitedCsv.length;
	var colNumber = splitedCsv[0].length;
	for (var i = 0; i < colNumber; i++) {
		var temp = 0;
		for (var j = 0; j < rowNumber; j++) {
			if (splitedCsv[j][i].length > temp) {
				temp = splitedCsv[j][i].length;
			}
		}
		maxLength.push(temp);
	}
	return maxLength;
}

/**
 * This function adds the datas to the table
 * @param {String[][]} csvMatrix The splited text
 * @param {boolean} firstRowAsLabels If this parameter is true, a table is added before the data. It contains
 * the first row of csvMatrix
 * @param {number[]} maxLength Contains the size of the maximum length element for each column
 */
function addCellsCsvTable(csvMatrix, firstRowAsLabels, maxLength) {

	//used to determine when datas will be added
	var i = 0;

	if (firstRowAsLabels == true) {
		//add the table labels
		var siblingElement = document.getElementById("modalBodyCsvData");
		//create the modal body
		var modalBodyCsvLabel = document.createElement("div");
		modalBodyCsvLabel.id = "modalBodyCsvLabel";
		modalBodyCsvLabel.classList.add("modal-body");
		
		//add the synchronisation between the scroll areas
		modalBodyCsvLabel.addEventListener("scroll", synchroniseScrollBot);
		document.getElementById("modalBodyCsvData").addEventListener("scroll", synchroniseScrollTop);
		
		//create the table
		var tableCsvLabel = document.createElement("table");
		tableCsvLabel.id = "csvTableLabel";		
		
		//add the row and column to the table
		var tr = document.createElement("tr");
		for (var h = 0; h < csvMatrix[0].length; h++) {
			var th = document.createElement("th");
			var textAsNode = document.createTextNode(csvMatrix[0][h]);
			th.appendChild(textAsNode);
			th.style.minWidth = maxLength[h] * 9 + 5 + "px";
			tr.appendChild(th);
		}
		tableCsvLabel.appendChild(tr);
		modalBodyCsvLabel.append(tableCsvLabel);
		document.getElementById("modalContentCsv").insertBefore(modalBodyCsvLabel, siblingElement);
	}
	else{
		document.getElementById("modalBodyCsvData").removeEventListener("scroll", synchroniseScrollTop);
	}
	if (firstRowAsLabels == true) i = 1;
	//add the table datas
	var parentElement = document.getElementById("modalBodyCsvData");
	for (i; i < csvMatrix.length; i++) {
		var table = document.createElement("table");
		var tr = document.createElement("tr");
		for (var j = 0; j < csvMatrix[i].length; j++) {
			var th = document.createElement("th");
			var textAsNode = document.createTextNode(csvMatrix[i][j]);
			th.style.minWidth = maxLength[j] * 9 + 5 + "px";
			th.appendChild(textAsNode);
			tr.appendChild(th);
		}
		table.appendChild(tr)
		parentElement.appendChild(table);
	}
}

/**
 * This function displays the csv table when the user pressed the button collect data after a test
 */
function displayCsvCollect(csvText) {
	clearCsvTable();
	var splitedCsv = stringToTable(csvText, "\t");
	//we remove the first empty line
//	splitedCsv.shift();
	splitedCsv = uniformRowCsv(splitedCsv);
	maxLengthColumn = maxLengthCol(splitedCsv);
	addCellsCsvTable(splitedCsv, true, maxLengthColumn);
	var modalCsv = document.getElementById("modalCsv");
	adaptModalCsvSize();
	modalCsv.removeAttribute("hidden");
	$('#modalCsv').modal('show');
}

/**
 * This function displays the csv table when a .csv file is opened on the ace editor and if the user clicks on the CSV button
 */
function displayTableCsvButton() {
	clearCsvTable();

	//retrieve the editor's content'
	var editor = ace.edit(getActiveEditor());
	var csvText = editor.getValue();

	//fix the title
	var filename = document.getElementById("displayFileName").innerHTML;
	document.getElementById("modalCsvTitle").innerHTML = filename;

	//we determine the separator and add splitedCells content to the table
	var comaChecked = document.getElementById("radioComma").checked;
	var semicolonChecked = document.getElementById("radioSemicolon").checked;
	var spaceChecked = document.getElementById("radioSpace").checked;
	var tabChecked = document.getElementById("radioTab").checked;
	var otherChecked = document.getElementById("radioCustom").checked;
	var firstRowAsLabel = document.getElementById("checkboxLabels").checked;
	if (comaChecked == true) {
		var splitedCsv = stringToTable(csvText, ",");
	}
	else if (semicolonChecked == true) {
		var splitedCsv = stringToTable(csvText, ";");
	}
	else if (spaceChecked == true) {
		var splitedCsv = stringToTable(csvText, " ");
	}
	else if (tabChecked == true) {
		var splitedCsv = stringToTable(csvText, "\t");
	}
	else if (otherChecked == true) {
		var splitedCsv = stringToTable(csvText, document.getElementById("csvInput").value);
	}
	splitedCsv = uniformRowCsv(splitedCsv);
	var maxLengthColumn = maxLengthCol(splitedCsv);
	addCellsCsvTable(splitedCsv, firstRowAsLabel, maxLengthColumn);
	adaptModalCsvSize();
	var modalCsv = document.getElementById("modalCsv");
	modalCsv.removeAttribute("hidden");
	$('#modalCsv').modal('show');
}

/**
 * Delete the tables created before
 */
function clearCsvTable() {
	var parentNodeCsvContent = document.getElementById("modalBodyCsvData");
	while (parentNodeCsvContent.firstChild) {
		parentNodeCsvContent.removeChild(parentNodeCsvContent.firstChild);
	}
	if (document.getElementById("modalBodyCsvLabel") != null) {
		document.getElementById("modalBodyCsvLabel").remove();
	}

}

/**
 * When the scroll cursor from the bot div is moved, the top div also move  
 */
var synchroniseScrollTop = function syncScrollTop() {
	var scrollDivTop = document.getElementById("modalBodyCsvLabel");
	var scrollDivBot = document.getElementById("modalBodyCsvData");
	scrollDivTop.scrollLeft = scrollDivBot.scrollLeft;

}

/**
 * When the scroll cursor from the top div is moved, the bot div also move  
 */
var synchroniseScrollBot = function syncScrollBot() {
	var scrollDivTop = document.getElementById("modalBodyCsvLabel");
	var scrollDivBot = document.getElementById("modalBodyCsvData");
	scrollDivBot.scrollLeft = scrollDivTop.scrollLeft;
	
}

/**
 * Adapt the modal view to the window size
 */
function adaptModalCsvSize() {
	var windowSize = window.innerHeight;
	var availableHeight = windowSize - 200;
	document.getElementById("modalBodyCsvData").style.maxHeight = availableHeight + "px";
}