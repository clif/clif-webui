
var currentTheme = getCookie("theme");

function swapTheme(element) {
	if (currentTheme == "light") {
		swapStyleSheet("css/bootstrap-dark.min.css")
		currentTheme = "dark";
		element.src = "/images/SelectLightTheme.png";
		document.cookie = "theme=dark; expires=Thu, 01 Jan 2038 00:00:00 UTC; path=/;";
		//change filetree color 
		var filetree = document.getElementById("ulFileTreeContainer");
		filetree.style.color = "black";
	} else if (currentTheme == "dark") {
		swapStyleSheet("css/bootstrap-light.min.css");
		currentTheme = "light";
		element.src = "/images/SelectDarkTheme.png";
		document.cookie = "theme=light; expires=Thu, 01 Jan 2038 00:00:00 UTC; path=/;";
		//change filetree color 
		var filetree = document.getElementById("ulFileTreeContainer");
		filetree.style.color = "white";
	}
}


function getTheme() {
	return currentTheme;
}

function swapStyleSheet(sheet) {
	document.getElementById("pagestyle").setAttribute("href", sheet);
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}