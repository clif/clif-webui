/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */


const localCache = {
	/**
	 * timeout for cache in millis
	 * @type {number}
	 */
	timeout: 30000,
	/** 
	 * @type {{_: number, data: {}}}
	 **/
	data: {},
	remove: function(url) {
		delete localCache.data[url];
	},
	exist: function(url) {
		return localCache.data.hasOwnProperty(url) && localCache.data[url] !== null;
	},
	get: function(url) {
		return localCache.data[url];
	},
	set: function(url, cachedData) {
		localCache.remove(url);
		localCache.data[url] = cachedData;
	}
};

/**
 *This function is called when the web page is refreshed. It loads the plugin content inside a 
 *a global variable named localCache, with the name plugins
 */
function getAllResources() {
	var url = "/buildCache";

	$.ajax({
		url: url,
		type: 'POST',
		dataType: "text",
		data: "test",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			var obj = JSON.parse(response);
			localCache.set("pluginInfo", obj);
		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(jqXhr.responseText)
		}
	});
}


/**
 * This function is called when the web page is refreshed. It loads the probe content inside
 * a global variable named localCache, with the name probes
 */
function getProbeInfo() {
	var url = "/buildProbeCache";
	$.ajax({
		url: url,
		type: 'POST',
		dataType: "text",
		data: "test",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(response) {
			var obj = JSON.parse(response);
			localCache.set("probeInfo", obj);
			completePropertiesContextMenu(localCache.get("probeInfo"));
		},
		error: function(jqXhr, textStatus, errorThrown) {
			showMessage(jqXhr.responseText)
		}
	});
}