/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

if (document.addEventListener) {
	//Autocompletion handler
	document.addEventListener('keydown', function(event) {
		if (isDescendant(document.getElementById("scenarioXML"), event.target)) {
			if (event.key == "F" && event.ctrlKey && event.shiftKey) {
				formatAceEditor();
			}
		}
	});
}

/**
 *This function retrieves the text from the editor and format it to match a xml format 
 */
function formatAceEditor() {
	var editor = ace.edit(getActiveEditor());
	var editorText = editor.getValue();
	var formattedXml = formatXmlV2(editorText);
	editor.setValue(formattedXml);
}
