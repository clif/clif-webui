/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2021 Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
/**
 * adds a listener to the keyboard. if the clicked key is "$", enable the automatic autocompletion and update the autocompletion list.
 */
if (document.addEventListener) {
	//Autocompletion handler
	document.addEventListener('keydown', function(event) {
		if (isDescendant(document.getElementById("scenarioXML"), event.target)) {
			var editor = ace.edit(getActiveEditor());
			var enableAutocomplete = enableAutocompletion();
			editor.setOptions({
				enableBasicAutocompletion: [{
					getCompletions: (editor, session, pos, prefix, callback) => {
						callback(null, initAutocompletionArray());
					},
				}],
			});
			if (event.key == "$" && enableAutocomplete == true) {
				editor.setOptions({
					enableBasicAutocompletion: [{
						getCompletions: (editor, session, pos, prefix, callback) => {
							callback(null, initAutocompletionArray());
						},
					}],
					enableLiveAutocompletion: true,
				});
			}
			else {
				editor.setOptions({
					enableBasicAutocompletion: [{
						getCompletions: (editor, session, pos, prefix, callback) => {
							callback(null, initAutocompletionArray());
						},
					}],
					enableLiveAutocompletion: false,
				});
			}
		}
		else {
			var editor = ace.edit(getActiveEditor());
			editor.setOptions({
				enableLiveAutocompletion: false,
				enableBasicAutocompletion: false,
			});
		}
	});
}

/**
 *This function is called to initialize the autocompletion array
 @returns {Array} arrayAutocompletion - the autocompletion array
 */
function initAutocompletionArray() {
	var arrayAutocompletion = [];
	var pluginArray = scanPlugins();
	pluginArray.sort();

	for (var i = 0; i < pluginArray.length; i++) {
		var arrayPluginId = scanPluginId(pluginArray[i]);
		for (j = 0; j < arrayPluginId.length; j++) {
			var meta = getLocalizedText("autocompletion.plugin").replace("%1", arrayPluginId[j]);
			var value = "${" + arrayPluginId[j] + ":}";


			var object = { value: value, score: 10, meta: meta };
			arrayAutocompletion.push(object);
		}
		arrayAutocompletion.push({ value: "$$", score: 50, meta: getLocalizedText("autocompletion.dollard") });
		arrayAutocompletion.push({ value: "$#", score: 40, meta: getLocalizedText("autocompletion.virtualUser") });
		arrayAutocompletion.push({ value: "${}", score: 30, meta: getLocalizedText("autocompletion.javaProperty") });


	}
	arrayAutocompletion.sort();
	changeAutocompletionWidth(arrayAutocompletion);
	return arrayAutocompletion;
}

/**
 *Changes the width of the autocompletion list on the editor. The longest text is multiplied by the width of the font.
 @param {String[]} arrayAutocompletion The text written inside the autocompletion list 
 */
function changeAutocompletionWidth(arrayAutocompletion) {

	var longestElement = 0;
	for (var i = 0; i < arrayAutocompletion.length; i++) {
		var length = arrayAutocompletion[i]["value"].length + 2 + arrayAutocompletion[i]["meta"].length;
		if (length > longestElement) {
			longestElement = length;
		}
	}
	var caracWidth = 8.3;
	var totalWidth = caracWidth * longestElement;
	if (totalWidth < 350) {
		$(".ace_autocomplete").width(350 + "px");
	}
	else if (totalWidth > 500) {
		$(".ace_autocomplete").width(500 + "px");
	}
	else {
		$(".ace_autocomplete").width(totalWidth + "px");
	}

}

/**
This function determines if the user is somewhere where the autocompletion is allowed.
The autocompletion is allowed inside <params> tags located inside a <behaviour> tag
@returns {boolean} indicate if the live autocompletion must be enabled or not
 */
function enableAutocompletion() {
	var editor = ace.edit(getActiveEditor());
	var completeText = editor.getValue();
	var cursorPosition = editor.selection.getCursor();
	var cursorAbsolutePosition = 0;
	var enableAutocomplete = false;

	//The cursor position is transformed into an absolute position
	var arrayCompleteText = completeText.split("\n");
	for (var i = 0; i < cursorPosition["row"]; i++) {
		arrayCompleteText[i] += "\n";
		cursorAbsolutePosition += arrayCompleteText[i].length;
	}
	cursorAbsolutePosition += cursorPosition["column"];

	//We locate the <behavior and </behavior tags
	var arrayBeginBehavior = [];
	var arrayEndBehavior = [];
	var indexBeginBehavior = completeText.indexOf("<behavior ");
	var indexEndBehavior = completeText.indexOf("</behavior>");

	while (indexBeginBehavior != -1) {
		arrayBeginBehavior.push(indexBeginBehavior);
		indexBeginBehavior = completeText.indexOf("<behavior ", indexBeginBehavior + 1);
	}
	while (indexEndBehavior != -1) {
		arrayEndBehavior.push(indexEndBehavior);
		indexEndBehavior = completeText.indexOf("</behavior>", indexEndBehavior + 1);
	}

	//We locate the <params and </params tags
	var arrayBeginParams = [];
	var arrayEndParams = [];
	var indexBeginParams = completeText.indexOf("<params>");
	var indexEndParams = completeText.indexOf("</params>");

	while (indexBeginParams != -1) {
		arrayBeginParams.push(indexBeginParams);
		indexBeginParams = completeText.indexOf("<params>", indexBeginParams + 1);
	}
	while (indexEndParams != -1) {
		arrayEndParams.push(indexEndParams);
		indexEndParams = completeText.indexOf("</params>", indexEndParams + 1);
	}

	//We check if the cursor absolute position is between two params tag 
	if (arrayEndParams.length != arrayBeginParams.length || arrayEndBehavior.length != arrayBeginBehavior.length) {
		enableAutocomplete = false;
	}
	else {
		for (var i = 0; i < arrayEndBehavior.length && enableAutocomplete == false; i++) {
			if (cursorAbsolutePosition < arrayEndBehavior[i] && cursorAbsolutePosition > arrayBeginBehavior[i]) {
				for (var j = 0; j < arrayBeginParams.length && enableAutocomplete == false; j++) {
					if (cursorAbsolutePosition < arrayEndParams[j] && cursorAbsolutePosition > arrayBeginParams[j]) {
						enableAutocomplete = true;
					}
				}
			}
		}
	}
	if (enableAutocomplete == true) {
		var textExtract = completeText.substring(arrayBeginParams[j - 1], arrayEndParams[j - 1]);
		enableAutocomplete = isInValueTag(cursorAbsolutePosition, textExtract, arrayBeginParams[j - 1]);
	}
	return enableAutocomplete;
}

/**
This is called when the user is between two params tags inside two behaviors tags.
It checks if the user is writting right after 'value=' 
If it is true, the autocompletion is triggered
@param {Array} cursorAbsolutePositon - the row and column where the cursor is located in Ace editor
@param {String} textExtract - the text contained between two <params> tags
@param {numeral} paramsTagPosition - the position of the params tag
@returns {boolean} indicate if the live autcompletion can be enabled 
 */
function isInValueTag(cursorAbsolutePosition, textExtract, paramsTagPosition) {
	var arrayValue = [];
	var arrayEndChevron = [];
	var indexBeginValue = textExtract.indexOf("value=");
	var indexEndChevron = textExtract.indexOf(">");
	var enableAutocomplete = false;

	while (indexBeginValue != -1) {
		arrayValue.push(indexBeginValue);
		indexBeginValue = textExtract.indexOf("value=", indexBeginValue + 1);
	}

	while (indexEndChevron != -1) {
		arrayEndChevron.push(indexEndChevron);
		indexEndChevron = textExtract.indexOf(">", indexEndChevron + 1);
	}

	for (var i = 0; i < arrayValue.length; i++) {
		if (cursorAbsolutePosition > arrayValue[i] + paramsTagPosition && cursorAbsolutePosition < paramsTagPosition + arrayEndChevron[i * 2 + 1]) {
			enableAutocomplete = true;
		}
	}
	return enableAutocomplete;
}