package org.ow2.clif.console.lib.webui.systemconfig;

/**
 * Set up system properties related to CLIF Proactive. 
 * @author Antoine Thevenet
 *
 */
public class SetProactive {
	
	public void setProactiveNet(ConfigProactiveNet configProactiveNet) {
		if (configProactiveNet.getDisableIPv6() != null) {
			System.setProperty("proactive.net.disableIPv6", configProactiveNet.getDisableIPv6());
		}
		if (configProactiveNet.getNetmask() != null) {
			System.setProperty("proactive.net.netmask", configProactiveNet.getNetmask());
		}
		if (configProactiveNet.getNolocal() != null) {
			System.setProperty("proactive.net.nolocal", configProactiveNet.getNolocal());
		}
		if (configProactiveNet.getNoprivate() != null) {
			System.setProperty("proactive.net.noprivate", configProactiveNet.getNoprivate());
		}
	}
	
	public void setProactivePamr(ConfigProactivePamr configProactivePamr) {
		if (configProactivePamr.getConnect_timeout() != null) {
			System.setProperty("proactive.pamr.connect_timeout", configProactivePamr.getConnect_timeout());
		}
		if (configProactivePamr.getSocketfactory() != null) {
			System.setProperty("proactive.pamr.socketfactory", configProactivePamr.getSocketfactory());
		}
	}

	public void setProactivePamrssh(ConfigProactivePamrssh configProactivePamrssh) {
		if (configProactivePamrssh.getAddress() != null) {
			System.setProperty("proactive.pamrssh.address", configProactivePamrssh.getAddress());
		}
		if (configProactivePamrssh.getKey_directory() != null) {
			System.setProperty("proactive.pamrssh.key_directory", configProactivePamrssh.getKey_directory());
		}
		if (configProactivePamrssh.getPort() != null) {
			System.setProperty("proactive.pamrssh.port", configProactivePamrssh.getPort());
		}
		if (configProactivePamrssh.getUsername() != null) {
			System.setProperty("proactive.pamrssh.username", configProactivePamrssh.getUsername());
		}
	}

	public void setProactivePnp(ConfigProactivePnp configProactivePnp) {
		if (configProactivePnp.getDefault_heartbeat() != null) {
			System.setProperty("proactive.pnp.default_heartbeat", configProactivePnp.getDefault_heartbeat());
		}
		if (configProactivePnp.getIdle_timeout() != null) {
			System.setProperty("proactive.pnp.idle_timeout", configProactivePnp.getIdle_timeout());
		}
		if (configProactivePnp.getPort() != null) {
			System.setProperty("proactive.pnp.port", configProactivePnp.getPort());
		}
	}

	public void setProactivePnps(ConfigProactivePnps configProactivePnps) {
		if (configProactivePnps.getAuthenticate()!= null) {
			System.setProperty("proactive.pnps.authentificate", configProactivePnps.getAuthenticate());
		}
		if (configProactivePnps.getDefault_heartbeat() != null) {
			System.setProperty("proactive.pnps.default_heartbeat", configProactivePnps.getDefault_heartbeat());
		}
		if (configProactivePnps.getIdle_timeout() != null) {
			System.setProperty("proactive.pnps.idle_timeout", configProactivePnps.getIdle_timeout());
		}
		if (configProactivePnps.getKeystore() != null) {
			System.setProperty("proactive.pnps.keystore", configProactivePnps.getKeystore());
		}
	}
	public void setProactivePnpsExtended(ConfigProactivePnpsExtended configProactivePnps) {
		if (configProactivePnps.getPassword()!= null) {
			System.setProperty("proactive.pnps.keystore.password", configProactivePnps.getPassword());
		}
	}
	
	public void setProactive(ConfigProactive configProactive) {
		if (configProactive.getUseIPaddress() != null) {
			System.setProperty("proactive.useIPaddress", configProactive.getUseIPaddress());
		}
		if (configProactive.getHostname() != null) {
			System.setProperty("proactive.hostname", configProactive.getHostname());
		}
	}
	
	public void setProactivePamrRouter(ConfigProactivePamrRouter configProactivePamrRouter) {
		if (configProactivePamrRouter.getAddress() != null) {
			System.setProperty("proactive.pamr.router.address", configProactivePamrRouter.getAddress());
		}
		if (configProactivePamrRouter.getPort() != null) {
			System.setProperty("proactive.pamr.router.port", configProactivePamrRouter.getPort());
		}
	}
	public void setProactivePamrAgent(ConfigProactivePamrAgent configProactivePamrAgent) {
		if (configProactivePamrAgent.getId() != null) {
			System.setProperty("proactive.pamr.agent.id", configProactivePamrAgent.getId());
		}
		if (configProactivePamrAgent.getMagic_cookie() != null) {
			System.setProperty("proactive.pamr.agent.magic_cookie", configProactivePamrAgent.getMagic_cookie());
		}
	}
	public void setProactiveCommunication(ConfigProactiveCommunication configProactiveCommunication) {
		if (configProactiveCommunication.getProtocol() != null) {
			System.setProperty("proactive.communication.protocol", configProactiveCommunication.getProtocol());
		}
		if (configProactiveCommunication.getAdditional_protocols() != null) {
			System.setProperty("proactive.communication.additional_protocols", configProactiveCommunication.getAdditional_protocols());
		}
	}
	public void setProactiveCommunicationExtended(ConfigProactiveCommunicationBenchmark configProactive1, ConfigProactiveCommunicationProtocols configProactive2) {
		if (configProactive1.getParameter() != null) {
			System.setProperty("proactive.communication.benchmark.parameter", configProactive1.getParameter());
		}
		if (configProactive2.getOrder() != null) {
			System.setProperty("proactive.communication.protocols.order", configProactive2.getOrder());
		}
	}

}
