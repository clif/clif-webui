/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.storage;

import org.ow2.clif.console.lib.webui.Util;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {
	static public final String PROJECT_ROOT_DEFAULT = "%PROJECTROOT%";
	
    @Value("${spring.security.enabled}")
	private String securityEnabled;

	@Value( "${clif.workspace}" )
	private String clifWorkspace;

	/**
	 * If clifWorkspace value contains PROJECT_ROOT_DEFAULT substring,
	 * then it must be replaced with the user home directory. Otherwise,
	 * the ClifWorkspace value is used/returned as is.
	 * @return The path to CLIF workspace (root folder containing all CLIF projects)
	 */
	public String getLocation()
	{
		if (clifWorkspace.contains(PROJECT_ROOT_DEFAULT))
		{
			clifWorkspace = clifWorkspace.replace(PROJECT_ROOT_DEFAULT, System.getProperty("user.home"));
		}
		return clifWorkspace;
	}

	public boolean securityEnabled() {
		return Util.isTrue(securityEnabled);
	}
}
