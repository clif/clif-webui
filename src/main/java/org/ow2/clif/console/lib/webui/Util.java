/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 * Copyright (C) 2022, 2024 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
* @author Tim Martin 
* @author Bruno Dillenseger
 */
public abstract class Util
{
    public static boolean isJUnitTest() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        List<StackTraceElement> list = Arrays.asList(stackTrace);
        for (StackTraceElement element : list) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }
        }
        return false;
    }

    public static String getLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    public static boolean canEditFile(String permission) {
        return permission.contains("admin") || permission.contains("editor");
    }

    /**
     * Gets the name of the project denoted by the given file path.
     * Actually the path is supposed to be relative to the workspace,
     * so the result is just the first path element.
     * @param filename the project's folder name for any relative path
     * @return just the project folder name, without any subsequent path element
     */
    public static String getProjectName(String filename)
    {
        return Paths.get(filename).getName(0).toString();
    }


    /**
     * Gets the base name of the given file path, without parent path and
     * file name extension (i.e. discarding the last char sequence beginning with .)
     * @param path
     * @return the last segment of the given path, without any file name extension
     */
    public static String getFileBasename(String path)
    {
        String basename = new File(path).getName();
        if (basename.contains("."))
        {
            basename = basename.substring(0, basename.lastIndexOf("."));
        }
        return basename;
    }

    /**
     * Truncates the given path from its first element
     * (which is supposed to be the project directory)
     * @param path a relative path including the project directory as first path element
     * @return a relative path discarding the first element of the given path
     */
    public static Path getRelativePath(Path path)
    {
        return path.getName(0).relativize(path);
    }

    /**
	 * Protection against malicious paths that could contain
	 * .. path elements or possibly an absolute path. An absolute path
	 * is turned relative by discarding the root element. Then the path
	 * is normalized (discard . and resolve .. path elements). Finally,
	 * all remaining occurrences of .. path elements are merely discarded.
	 * @param entry a path
	 * @return a sanitized version of the given path
	 */
	public static Path sanitize(Path entry)
	{
		entry = entry.normalize();
		if (entry.isAbsolute())
		{
            entry = entry.getRoot().relativize(entry);
		}
		return sanitizeNormalized(entry);
	}

    public static String sanitize(String path)
    {
        return sanitize(Paths.get(path)).toString();
    }

	private static Path sanitizeNormalized(Path entry)
	{
		Iterator<Path> paths = entry.iterator();
		Path result = Paths.get("");
		while (paths.hasNext())
		{
			Path pathElement = paths.next();
			if (! pathElement.toString().equals(".."))
			{
				result = (result.getNameCount() == 0) ? pathElement : result.resolve(pathElement);
			}
		}
		return result;
	}

    public static String[] getArgsFile(String file) {
        String[] tmp = { file.substring(0, file.indexOf("\n")), file.substring(file.indexOf("\n") + 1) };
        return tmp;
    }

    public static String[] getArgsPermissions(String attributes) {
        String[] tmp = { attributes.substring(0, attributes.indexOf("/")),
                attributes.substring(attributes.indexOf("/") + 1, attributes.indexOf("/", attributes.indexOf("/") + 1)),
                attributes.substring(attributes.indexOf("/", attributes.indexOf("/") + 1) + 1) };
        return tmp;
    }

    public static String returnMessage(String properties, String[] args) {
        ResourceBundle messages = ResourceBundle.getBundle("lang/res", LocaleContextHolder.getLocale());
        String message = messages.getString(properties);
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                message = message.replace("%" + (i + 1), args[i]);
            }
        }
        return message;
    }

    public static void createPaclifOpts(String target) throws IOException {
        InputStream is = new ClassPathResource("paclif.opts").getInputStream();
        byte[] buffer = new byte[is.available()];
        is.read(buffer);	 
        File targetFile = new File(target);
        OutputStream os = new FileOutputStream(targetFile);
        os.write(buffer);
        os.close();
        is.close();        
	}

    /**
     * Analyze the given word to check if it may be interpreted as
     * a boolean true value
     * @param word some string to analyze
     * @return true if the given word is any of string 'true', 'yes', 'on', 'ok'
     * (case incensitive) or '1'; false otherwise.
     */
    public static boolean isTrue(String word)
    {
        return
            word.equalsIgnoreCase("true")
            || word.equalsIgnoreCase("yes")
            || word.equalsIgnoreCase("on")
            || word.equalsIgnoreCase("ok")
            || word.equals("1");
    }


    /**
     * Runs a clifcmd command line with the given arguments.
     * The clifcmd command line is built accordingly to current OS, which
     * means that, for Windows, "cmd /c" is inserted at the beginning
     * so that the PATH environment variable is used to resolve clifcmd's
     * location.
     * Note: the PATH environment variable is the active one when starting
     * the CLIF web UI server, and its value must include the path to the bin
     * directory of the ProActive CLIF runtime.
     * @param directory the directory to be used as default/current directory
     * by the clifcmd process
     * @param args the arguments to the clifcmd command.
     * @return the process object associated to the running clifcmd command.
     * @throws IOException The process could not be run for some reason,
     * most probably because the clifcmd command was not found due to
     * incomplete or incorrect value of the PATH environment variable.
     */
    public static Process startClifCmdProcess(File directory, String... args)
    throws IOException
    {
        ProcessBuilder pb;
        String[] cmd;
        int i = 0;
        if (System.getProperty("os.name").toLowerCase().contains("win"))
        {
            cmd = new String[3 + args.length];
            cmd[i++] = "cmd";
            cmd[i++] = "/c";
        }
        else
        {
            cmd = new String[1 + args.length];
        }
        cmd[i++] = "clifcmd";
        for (String arg : args)
        {
            cmd[i++] = arg;
        }
        pb = new ProcessBuilder(cmd)
            .redirectErrorStream(true)
            .directory(directory);
        return pb.start();
    }


    public static void killDeployProcess(Process p)
    throws IOException
    {
        try
        {
            if (System.getProperty("os.name").toLowerCase().contains("win"))
            {
                ProcessBuilder pb = new ProcessBuilder(
                    "powershell",
                    "-command",
                    "Remove-CimInstance -Query 'Select * from Win32_Process where commandline LIKE \"%DeployCmd%\"'");
                Process proc = pb.start();
                proc.waitFor();
            }
            else if (p != null)
            {
                p.destroy();
                p.waitFor();
            }
        }
        catch (InterruptedException ex)
        {
            // nevermind if waitFor() was interrupted
        }
    }
}
