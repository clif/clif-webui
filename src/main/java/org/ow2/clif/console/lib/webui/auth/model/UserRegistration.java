/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.auth.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class UserRegistration {

	@NotBlank(message = "Username should be blank")
	@NotEmpty(message = "Username should be blank")
	public final String username;
	@NotBlank(message = "Password should be blank")
	@NotEmpty(message = "Password should be blank")
	public final String password;	    
	@Email(message = "Email should be valid")
	@NotBlank(message = "Email should be blank")
	@NotEmpty(message = "Email should be blank")
	public final String email;
	
	public UserRegistration(String username, String password, String email){
		this.username=username;
		this.password=password;
		this.email=email;
	}

}
