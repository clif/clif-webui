/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin, Antoine Thevenet
 * Copyright (C) 2022, 2024 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;
import javax.xml.parsers.ParserConfigurationException;
import org.ow2.clif.console.lib.webui.Util;
import org.ow2.clif.console.lib.webui.auth.dao.DataAccessImpl;
import org.ow2.clif.console.lib.webui.storage.Exceptions.StorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.xml.sax.SAXException;

import static org.ow2.clif.console.lib.webui.Util.sanitize;

/**
* @author Tim Martin
* @author Antoine Thevenet
* @author Bruno Dillenseger
*/

@Service
public class StorageHandler {
    
    private final DataAccessImpl dataAccessImpl;
    private final boolean securityEnabled;

    private final FileSystemStorage storageService;
    Logger logger = (Logger) LoggerFactory.getLogger(StorageHandler.class);


	@Autowired
	public StorageHandler(StorageProperties properties, DataAccessImpl dataAccessImpl) {
        this.dataAccessImpl = dataAccessImpl;
        this.securityEnabled = properties.securityEnabled();
        this.storageService = new FileSystemStorage(properties.getLocation());
	}

    
    public boolean canEditFile(String projectname)
    {
        projectname = sanitize(projectname);
		if (!this.securityEnabled)
        {
            return true;
        }
		String username = Util.getLoggedUser();
		if (Paths.get(projectname).getNameCount() > 1)
        {
			return Util.canEditFile(dataAccessImpl.getPermission(username, projectname));
		}
        else
        {
			return dataAccessImpl.getPermission(username, projectname).contains("admin");
		}
    }    	


    public String saveFile(String fileName, String fileContent)
    throws FileNotFoundException
    {
        fileName = sanitize(fileName);
        if(!this.canEditFile(Util.getProjectName(fileName)))
        {
            logger.info("User " + Util.getLoggedUser() + "Unauthorized to save " + Util.getProjectName(fileName));
            return Util.returnMessage("error.unauthorized", null);
        }
		String args[] = { new File(fileName).getName() };
		logger.info("Saving file " + fileName);
        storageService.saveFile(fileName, fileContent);
        return Util.returnMessage("action.save", args);
    }


    public String importFile(String fileName, String fileContent)
    {
        fileName = sanitize(fileName);
		if (!canEditFile(Util.getProjectName(fileName)))
        {
			logger.info("User " + Util.getLoggedUser() + "Unauthorized to upload " + Util.getProjectName(fileName));
			return Util.returnMessage("error.unauthorized", null);
		}
		storageService.store(fileName,fileContent);
        logger.info("Uploading file " + fileName);
		String args[] = { new File(fileName).getName() };
     	return Util.returnMessage("action.fileimport", args);
	}


    public ArrayList<String> importProject(byte[] data)
    throws IOException
    {
        ArrayList<String> newProjects = storageService.importUnzipProject(data);
        for(String p : newProjects)
        {
            p = p.replaceAll("/","");
            if (!Util.isJUnitTest() && securityEnabled)
            { // Creating project in DB
                dataAccessImpl.createProject(Util.getLoggedUser(), p);            
            }
        }
        return newProjects;
	}


	public String createFile(String file)
    throws StorageException
    {
        Path filePath = sanitize(Paths.get(file));
        if (
            filePath.getNameCount() <= 1
            || !canEditFile(Util.getProjectName(filePath.toString())))
        {
            logger.info(
                "User "
                + Util.getLoggedUser()
                + " is not authorized to create file "
                + filePath.toString());
            return Util.returnMessage("error.unauthorized", null);
        }
        storageService.createFile(file);
        return Util.returnMessage(
            "action.create",
            new String[] { filePath.subpath(1, filePath.getNameCount()).toString() });
    }


    public String createDirectory(String path)
    {
        Path directoryPath = sanitize(Paths.get(path));
        String[] args = new String[] { directoryPath.getName(0).toString() };
        if (directoryPath.getNameCount() > 1)
        { // not creating a project
            if(!canEditFile(Util.getProjectName(directoryPath.toString())))
            {
                logger.info("User " + Util.getLoggedUser() + " is not authorized to create files in " + Util.getProjectName(directoryPath.toString()));
                return Util.returnMessage("error.unauthorized", null);
            }
            args = new String[] { directoryPath.subpath(1, directoryPath.getNameCount()).toString() };
        }
        storageService.createDirectory(directoryPath);
        if (directoryPath.getNameCount() == 1 && !Util.isJUnitTest() && securityEnabled)
        { // Creating project in DB
            dataAccessImpl.createProject(Util.getLoggedUser(), directoryPath.toString());
        }
        return Util.returnMessage("action.create", args);
    }


    /**
	 * Renames a file or directory (whatever a project directory itself, or a project sub-directory).
     * This method first "sanitizes" both given paths, discarding possible root path element
     * and . occurrences, as well as resolving as much as possible .. occurrences, or discarding
     * them when not possible. So, giving a "malicious" path as parameter will result in
     * a safe, transformed path.
	 * Note: this method enforces that the target parent directory is the same as
     * the initial path's parent directory. In other words, this operation cannot be used to
     * actually move a directory or file.
	 * @param path directory or file to rename (relative to the workspace)
	 * @param newName new name, any parent path information, if included in this name, is ignored
	 */
	public String rename(String path, String newName)
    throws StorageException
    {
        Path fromPath = sanitize(Paths.get(path));
        if(!canEditFile(Util.getProjectName(fromPath.toString())))
        {
            logger.info("Rename file: User " + Util.getLoggedUser() + " is not authorized to rename file " + Util.getProjectName(fromPath.toString()));
            return Util.returnMessage("error.unauthorized", null);
        }
        Path toPath = sanitize(Paths.get(newName));
        if (fromPath.getNameCount() == 0) // empty path is invalid
        {
            logger.info("Rename file: source path is invalid: " + path);
            throw new StorageException(
                Util.returnMessage(
                    "action.file.invalid-name",
                    new String[] { path }));
        }
        else if (fromPath.getNameCount() == 1) // rename project directory
        {
            toPath = toPath.getFileName();
        }
        else // rename project file or sub-directory
        {
            toPath = fromPath.getParent().resolve(toPath.getFileName());
        }
        storageService.rename(fromPath, toPath);
        if (fromPath.getNameCount() == 1 && !Util.isJUnitTest() && securityEnabled)
        {  // Renaming project in DB
            dataAccessImpl.renameProject(toPath.getName(0).toString(), fromPath.getName(0).toString());
        }
        return Util.returnMessage(
            "action.rename",
            new String[] { fromPath.getFileName().toString(), toPath.getFileName().toString()});
    }
    

    /**
	 * Moves a directory or a file to the given directory.
	 * Source and destination path names shall be relative to the workspace.
	 * This method first "sanitizes" both given paths, discarding possible root path element
     * and . occurrences, as well as resolving as much as possible .. occurrences, or discarding
     * them when not possible. So, giving a "malicious" path as parameter will result in
     * a safe, transformed path.
	 * Note: moving a project directory is not supported, so the target directory shall
	 * hold at least 2 path elements.
	 * @param from directory or file to move (relative to the workspace)
	 * @param destDir target directory (relative to the workspace)
     * @throws StorageException moving failed
	 */
    public String move(String from, String destDir)
    throws StorageException
    {
        Path fromPath = sanitize(Paths.get(from));
        if (!canEditFile(Util.getProjectName(fromPath.toString())))
        {
            logger.info("Move file: User " + Util.getLoggedUser() + " is not authorized to move file " + Util.getProjectName(fromPath.toString()));
            return Util.returnMessage("error.unauthorized", null);
        }
        Path toPath = Util.sanitize(Paths.get(destDir));
        if (fromPath.getNameCount() <= 1) // source path is empty or project: invalid
        {
            logger.info("Move file: source path is invalid: " + from);
            throw new StorageException(
                Util.returnMessage(
                    "action.file.invalid-name",
                    new String[] { from }));
        }
        else if (toPath.getNameCount() == 0) // destination path is empty: invalid
        {
            logger.info("Move file: destination path is invalid: " + destDir);
            throw new StorageException(
                Util.returnMessage(
                    "action.file.invalid-name",
                    new String[] { destDir }));
        }
        storageService.move(fromPath, toPath);
        return Util.returnMessage(
            "action.move",
            new String[] { fromPath.toString(), toPath.toString()});
	}


    /**
	 * Deletes a directory or a file. The given path must be relative to the workspace.
	 * This method first "sanitizes" the given path, discarding possible root path element
     * and . occurrences, as well as resolving as much as possible .. occurrences, or
     * discarding them when not possible. So, giving a "malicious" path as parameter
     * will result in a safe, transformed path.
     * Note: when the given path is a directory, it is deleted whatever empty or not
	 * @param fileOrDir path of directory or file to delete (relative to the workspace)
     * @throws StorageException deletion failed
    */
	public String delete(String fileOrDir)
    {
        Path path = sanitize(Paths.get(fileOrDir));
		if (!canEditFile(Util.getProjectName(path.toString())))
        {
            logger.info(
                "Delete file: user "
                + Util.getLoggedUser()
                + " is not authorized to move file "
                + Util.getProjectName(path.toString()));
			return Util.returnMessage("error.unauthorized",null);
		}
		storageService.delete(path);
        if (path.getNameCount() == 1 && !Util.isJUnitTest() && securityEnabled)
        { // Deleting project in DB
            dataAccessImpl.deleteProject(path.toString());				
        }	
        logger.info("Deleted " + path);
        return Util.returnMessage("action.delete", new String[] { path.toString() });
	}


	public String loadContent(String filename)
    throws IOException
    {
        filename = sanitize(filename);
        logger.info("Loading file " + filename);
    	return storageService.loadContent(filename);
	}


	public String retrieveContent(String filename) {
        filename = sanitize(filename);
        logger.info("Retrieve file "+filename);
    	return storageService.retrieveContent(filename);
	}

	public boolean isFile(String relativePath) {
		return storageService.isFile(sanitize(relativePath));
	}

	public Model listUploadedFiles(Model model)
    throws IOException
    {
		String username = Util.getLoggedUser();
		if (username.isEmpty())
        {
			return null;
		}
		logger.info("Connected as: " + username);
		model.addAttribute("username", username);
        logger.info("Getting uploaded files...");
        model.addAttribute("files", storageService.loadAll().collect(Collectors.toList()));		
        if (securityEnabled)
        {
            model.addAttribute("projects", dataAccessImpl.getAccessibleProjects(username));
        }
        else
        {
            model.addAttribute("projects", storageService.loadProjects());
        }
		model.addAttribute("directories", storageService.loadDirectories().collect(Collectors.toList()));	
        logger.info("Getting user permissions...");
		model.addAttribute("permissions", dataAccessImpl.getAccessUser(Util.getLoggedUser()));		
        model.addAttribute("securityEnabled", securityEnabled);
        return model;
	}


	public Object[] getAccessiblesFiles()
    {
        String username = Util.getLoggedUser();
		if (username.isEmpty() && securityEnabled)
        {
			return null;
		}
        return storageService.loadAll().toArray();
	}


	public Object[] getAccessiblesDirs()
    {
        String username = Util.getLoggedUser();
		if (username.isEmpty() && securityEnabled)
        {
			return null;
		}
        return storageService.loadDirectories().toArray();
	}


    public InputStream getZipProject(String projectName) throws IOException {
        return storageService.exportZipProject(projectName);
    }


    /**
	 * Creates a file with some initial content: either CLIF test plan
     * or ISAC scenario "skeleton".
     * The given path must be relative to the workspace.
	 * This method first "sanitizes" the given path, discarding possible root path
     * element and . occurrences, as well as resolving as much as possible .. occurrences,
     * or discarding them when not possible. So, giving a "malicious" path as parameter
     * will result in a safe, transformed path.
	 * @param file path of file to create (relative to the workspace). If ending with
     * ".ctp", a default CLIF test plan is created; if ending with ".xis", an
     * ISAC scenario "skeleton" is created. Otherwise, the file is just created with no content.
     * @throws StorageException deletion failed
    */
    public String createFileWithContent(String file)
    throws IOException
    {
        file = sanitize(file);
        logger.info("Creating file " + file + " with initial content");
        createFile(file);
        if (file.endsWith(".ctp"))
        {
            storageService.initCtpFile(new File(file));
        }
        else if (file.endsWith(".xis"))
        {
            storageService.initXisFile(new File(file));
        }
        return storageService.loadContent(file);
    }


    /**
	 * Creates a copy of a file.
     * The given paths must be relative to the workspace.
	 * This method first "sanitizes" the given paths, discarding possible root path
     * element and . occurrences, as well as resolving as much as possible .. occurrences,
     * or discarding them when not possible. So, giving a "malicious" path as parameter
     * will result in a safe, transformed path.
     * @param source path of file to duplicate (relative to the workspace)
	 * @param destination path of file to create (relative to the workspace), as a copy of the
     * "source" file
     * @throws IOException file duplication failed
    */
    public String duplicateFile(String source, String destination)
    throws IOException
    {
        Path fromPath = Util.sanitize(Paths.get(source));
        Path toPath = Util.sanitize(Paths.get(destination));
        if (fromPath.equals(toPath))
        {
            return Util.returnMessage("action.canceled", null);
        }
        else if (
            toPath.getNameCount() < 2
            || !canEditFile(Util.getProjectName(toPath.toString())))
        {
            logger.info(
                "User "
                + Util.getLoggedUser()
                + " is not authorized to duplicate a file to "
                + Util.getProjectName(toPath.toString()));
            return Util.returnMessage("error.unauthorized", null);
        }
        storageService.duplicateFile(fromPath, toPath);
        return storageService.loadContent(toPath.toString());
    }


    /**
     * This function is called when the web page is opened in order to build the cache
     * @param resourceToAccess
     * @return
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
	public String buildCache() throws IOException, ParserConfigurationException, SAXException{
		return storageService.initCache();
	}
	
	/**
     * This function is called when the web page is opened in order to build the cache
     * @param resourceToAccess
     * @return
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
	public String buildProbeCache() throws IOException, ParserConfigurationException, SAXException{
		return storageService.initCacheProbe();
	}

	/*public void addZippedFolder(String path) {
        ImportZipFolder.put(util.getUserLogged(),path);
        logger.info("creating folder space key : "+util.getUserLogged()+" value : "+path);
	}
    */
}
