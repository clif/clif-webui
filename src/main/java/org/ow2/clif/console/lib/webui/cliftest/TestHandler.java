/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin
 * Copyright (C) 2022 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.cliftest;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.supervisor.api.ClifException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.util.ThrowableHelper;
import org.ow2.clif.console.lib.webui.storage.StorageProperties;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactive;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactiveCommunication;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactiveCommunicationBenchmark;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactiveCommunicationProtocols;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactiveNet;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactivePamr;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactivePamrAgent;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactivePamrRouter;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactivePamrssh;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactivePnp;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactivePnps;
import org.ow2.clif.console.lib.webui.systemconfig.ConfigProactivePnpsExtended;
import org.ow2.clif.console.lib.webui.systemconfig.SetProactive;
import org.ow2.clif.console.lib.webui.Util;
import org.ow2.clif.console.lib.webui.elasticsearch.MappingElasticSearch;
import org.ow2.clif.supervisor.api.BladeState;

/**
 * @author Bruno Dillenseger
 * @author Tim Martin
 */
@Service
public class TestHandler {

	private final Path rootLocation;

	static volatile boolean deployOK = true;

	private Map<String, Process> runningProcess;

	private ClifRegistry registry;

	private final String SuccessInit = "Test plan %1 initialized successfully.";

	private String[] injectorsIds;
	private String[] serversNames;
	// full path of deployed test plan file, including project directory
	private String testPlanFileName; 
	private String resultFolderName;
	private ClifAppFacade clifApp;

	public String[] getInjectorsIds()
	{
		return injectorsIds;
	}

	protected void setTestPlan(String fileName)
	{
		this.testPlanFileName = fileName;

		try
		{
			InputStream is = new FileInputStream(new File(rootLocation.toFile(), fileName));
			Map<String, ClifDeployDefinition> testPlanProject = TestPlanReader.readFromProp(is);
			Set<String> injectors = new HashSet<String>();
			Set<String> servers = new HashSet<String>();
			for (String id : testPlanProject.keySet())
			{
				if (!testPlanProject.get(id).isProbe())
				{
					injectors.add(id);
				}
				servers.add(testPlanProject.get(id).getServerName());
			}
			this.injectorsIds = injectors.toArray(new String[injectors.size()]);
			this.serversNames = servers.toArray(new String[servers.size()]);
		}
		catch (Exception e)
		{
			throw new ClifHandlerException("error.testplan.deployed", e);
		}
	}

	public String getTestPlanFileName() {
		return testPlanFileName;
	}

	public String getResultFolderName() {
		return resultFolderName;
	}

	public void setResultFolderName(String resultFolderName) {
		this.resultFolderName = resultFolderName;
	}

	public String[] getServersNames() {
		return serversNames;
	}

	public ClifAppFacade getClifApp() {
		return clifApp;
	}

	public void setClifApp(ClifAppFacade clifApp) {
		this.clifApp = clifApp;
	}
	
	//We load system properties
	@Autowired
	private ConfigProactive configProactive;
	@Autowired
	private ConfigProactiveCommunication configProactiveCommunication;
	@Autowired
	private ConfigProactiveCommunicationBenchmark configProactiveCommunicationBenchmark;
	@Autowired
	private ConfigProactiveCommunicationProtocols configProactiveCommunicationProtocols;
	@Autowired
	private ConfigProactiveNet configProactiveNet;
	@Autowired
	private ConfigProactivePamr configProactivePamr;
	@Autowired
	private ConfigProactivePamrAgent configProactivePamrAgent;
	@Autowired
	private ConfigProactivePamrRouter configProactivePamrRouter;
	@Autowired
	private ConfigProactivePamrssh configProactivePamrssh;
	@Autowired
	private ConfigProactivePnp configProactivePnp;
	@Autowired
	private ConfigProactivePnps configProactivePnps;
	@Autowired
	private ConfigProactivePnpsExtended configProactivePnpsExtended;
	
	/**
	 * Before launching CLIF registry, we launch each system property linked to CLIF proactive
	 * @throws Exception
	 */
	@PostConstruct
	public void setSystemProperties() throws Exception {
		SetProactive setProactive = new SetProactive();
		setProactive.setProactive(configProactive);
		setProactive.setProactiveCommunication(configProactiveCommunication);
		setProactive.setProactiveCommunicationExtended(configProactiveCommunicationBenchmark,
				configProactiveCommunicationProtocols);
		setProactive.setProactiveNet(configProactiveNet);
		setProactive.setProactivePamr(configProactivePamr);
		setProactive.setProactivePamrAgent(configProactivePamrAgent);
		setProactive.setProactivePamrRouter(configProactivePamrRouter);
		setProactive.setProactivePamrssh(configProactivePamrssh);
		setProactive.setProactivePnp(configProactivePnp);
		setProactive.setProactivePnps(configProactivePnps);
		setProactive.setProactivePnpsExtended(configProactivePnpsExtended);
		System.setProperty("fractal.provider", "org.objectweb.proactive.core.component.Fractive");
		try {
			logger.info("Creating a CLIF Registry...");
			this.registry = ClifRegistry.getInstance(true);
			logger.info("Created registry " + this.registry);
		} catch (Exception ex) {
			logger.info("Failed. Trying to connect to an existing CLIF Registry...");
			this.registry = ClifRegistry.getInstance(false);
			logger.info("Connected to " + this.registry);
		}

	}

	@Autowired
	public TestHandler(StorageProperties properties) throws Exception {
		this.rootLocation = Paths.get(properties.getLocation());
		this.runningProcess = new HashMap<>();
	}

	Logger logger = (Logger) LoggerFactory.getLogger(TestHandler.class);

	private ClifAppFacade getClifAppFacade(String file)
	{
		logger.debug("Searching test with name: " + file);
		String testName = Util.getFileBasename(file);
		return new ClifAppFacade(this.registry.lookupClifApp(testName), testName);
	}

	public String readOutput(Process p) throws IOException
	{
		BufferedReader stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stderr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		boolean success = false;
		String line = "";
		while (!success && (line = stdout.readLine()) != null)
		{
			logger.info(line);
			if (line.contains("is deployed"))
			{
				success = true;
				stderr.close();
			}
			// note: "line" holds only the "is deployed" line from stdout (the most interesting)
		}
		stdout.close();
		if (! success)
		{
			line = stderr.readLine(); // retain only 1st line from stderr (ignore stack trace)
			logger.warn(line);
			stderr.close();
			int exitValue = 0;
			try
			{
				exitValue = p.waitFor();
			}
			catch (InterruptedException ex)
			{
				throw new IOException(line + " (interrupted)");
			}
			throw new IOException(line + " (exit code: " + exitValue + ")");
		}
		return line;
	}

	/**
	 * This function checks the server that are registered in the registry. It
	 * compares them to the servers mentioned in the ctp file
	 * 
	 * @param file
	 * @return
	 */
	public String checkServers(String file)
	{
		setTestPlan(Util.sanitize(Paths.get(file)).toString());
		// retrieve the servers existing in the registry and mentioned in the ctp file
		String[] registryServers = registry.getServers();
		String[] ctpServers = getServersNames();
		// Complete the JSON response
		String responseRegistry = "\"registryServers\":[";
		String responseMissing = "\"missingServers\":[";
		String registryInfo = "\"registry\":" + "\"" + registry.toString() + "\"";
		// complete the JSON about registry servers
		for (int i = 0; i < registryServers.length; i++) {
			responseRegistry += "\"" + registryServers[i] + "\"" + ",";
		}
		if (registryServers.length >= 1) {
			responseRegistry = responseRegistry.substring(0, responseRegistry.length() - 1);
		}
		responseRegistry += "]";

		// complete the JSON about the servers mentioned in the ctp file
		for (int i = 0; i < ctpServers.length; i++) {
			boolean serverExist = false;
			for (int j = 0; j < registryServers.length; j++) {
				if (ctpServers[i].equals(registryServers[j])) {
					serverExist = true;
				}
			}
			if (serverExist == false) {
				responseMissing += "\"" + ctpServers[i] + "\"" + ",";
			}
		}
		if (responseMissing.charAt(responseMissing.length() - 1) == ',') {
			responseMissing = responseMissing.substring(0, responseMissing.length() - 1);
		}
		responseMissing += "]";
		String response = "{" + responseRegistry + "," + responseMissing + "," + registryInfo + "}";
		return response;
	}

	/**
	 * This function launches the clif command clifcmd deploy. It also set up some
	 * variables that will be used by the other commands
	 * 
	 * @param file the .ctp file name
	 * @return
	 * @throws IOException
	 * @throws ClifException
	 */
	public String deployTest(String file)
	throws IOException, ClifException
	{
		file = Util.sanitize(Paths.get(file)).toString();
		String testplanName = Util.getFileBasename(file);
		String testplanFilename = Util.getRelativePath(Paths.get(file)).toString();
		logger.info("Deploying " + testplanName + " from " + testplanFilename);
		try
		{
			Util.killDeployProcess(null);
		}
		catch (Exception ex)
		{
			// nevermind - just tried to clean Windows from existing Deployment process
		}
		Process process = Util.startClifCmdProcess(
			rootLocation.resolve(Paths.get(file).getName(0)).toFile(),
			"deploy",
			testplanName,
			testplanFilename);
		runningProcess.put(file, process);

		// set up some variables to avoid using post methods
		setTestPlan(file);
		return readOutput(process);
	}

	public String initTest(String testRunId) {
		String testPlanName = Util.getFileBasename(getTestPlanFileName());
		setResultFolderName(testRunId);
		logger.info("Initializing test plan " + testPlanName + " with test run id " + testRunId);
		try {
			ClifAppFacade clifApp = getClifAppFacade(testPlanName);
			if (clifApp == null) {
				throw new ClifHandlerException("error.testplan.deployed");
			}
			setClifApp(clifApp);
			logger.info("Test plan found");
			clifApp.init(testRunId);
			int res = clifApp.waitForState(null, BladeState.INITIALIZED);
			if (res == 0) {
				logger.info("Initialized");
				return SuccessInit.replace("%1", testPlanName);
			}
		} catch (Exception ex) {
			logger.info("Initialization failed:\n" + ThrowableHelper.getStackTrace(ex));
			if (ex instanceof ClifException && ex.getCause() == null) {
				throw new ClifHandlerException("error.init.failed", ex);
			}
		}
		throw new ClifHandlerException("error.init.failed");
	}

	public String startTest() throws IOException
	{
		String testPlanName = Util.getFileBasename(getTestPlanFileName());
		try {
			ClifAppFacade clifApp = getClifApp();
			if (clifApp == null) {
				throw new ClifHandlerException("error.testplan.deployed");
			}
			logger.info("Starting " + testPlanName + " test plan...");
			int res = clifApp.start(null);
			if (res == 0)
			{
				logger.debug("Blades states before waiting: " + clifApp.getGlobalState(null).toString());
				res = clifApp.waitForState(null, BladeState.RUNNING);
				logger.debug("Blades states after waiting: " + clifApp.getGlobalState(null).toString());
				if (res == 0) {
					logger.info("Test started");
					return "Test started";
				}
			}
			throw new ClifHandlerException("error.init.blades");
		} catch (Exception ex) {
			throw new ClifHandlerException("error.execution.starting", ex);
		}
	}

	public String suspendTest() throws IOException
	{
		String testPlanName = Util.getFileBasename(getTestPlanFileName());
		logger.info("Suspending test plan " + testPlanName);
		try
		{
			ClifAppFacade clifApp = getClifApp();
			if (clifApp == null)
			{
				throw new ClifHandlerException("error.testplan.deployed");
			}
			logger.debug("Blades states before suspend: " + clifApp.getGlobalState(null).toString());
			int res = clifApp.suspend(null);
			if (res == 0)
			{
				res = clifApp.waitForState(null, BladeState.SUSPENDED);
				logger.debug("Blades states after waiting : " + clifApp.getGlobalState(null).toString());
				if (res == 0)
				{
					logger.info("Test " + testPlanName + " is suspended");
					return "Test is suspended";
				}
			}
			throw new ClifHandlerException("error.blade.unsuspendable");
		}
		catch (Exception ex)
		{
			throw new ClifHandlerException("error.suspend.blades", ex);
		}
	}

	public String stopTest()
	{
		String testPlanName = getTestPlanFileName();
		logger.info("Stopping test plan " + Util.getFileBasename(testPlanName));
		try
		{
			ClifAppFacade clifApp = getClifApp();
			if (clifApp == null)
			{
				clifApp = getClifAppFacade(Util.getFileBasename(testPlanName));
			}
			if (clifApp == null)
			{
				throw new ClifHandlerException("error.testplan.deployed");
			}
			if (clifApp.getGlobalState(null) != BladeState.COMPLETED) {
				logger.debug("Blades states before stopping test: " + clifApp.getGlobalState(null).toString());
				int res = clifApp.stop(null);
				logger.debug("Blades states after stopping test: " + clifApp.getGlobalState(null).toString());

				if (res == 0)
				{
					res = clifApp.waitForState(null, BladeState.STOPPED);
					if (res == 0) {
						Process p = this.runningProcess.get(testPlanName);
						if (p != null)
						{
							Util.killDeployProcess(p);
						}
						this.runningProcess.remove(testPlanName);
						setClifApp(null);
						logger.info("Test stopped");
						return "Test stopped";
					}
				}
				throw new ClifHandlerException("error.blade.unstoppable");
			}
			else
			{
				Process p = this.runningProcess.get(testPlanName);
				Util.killDeployProcess(p);
				this.runningProcess.remove(testPlanName);
				setClifApp(null);
				logger.info("Test process stopped");
				return "Test stopped";
			}
		}
		catch (Exception ex)
		{
			throw new ClifHandlerException("error.execution.stopping", ex);
		}
	}

	public String resumeTest()
	{
		String testPlanName = Util.getFileBasename(getTestPlanFileName());
		try
		{
			ClifAppFacade clifApp = getClifApp();
			if (clifApp == null)
			{
				throw new ClifHandlerException("error.testplan.deployed");
			}
			logger.info("Resuming " + testPlanName + " test plan...");
			logger.info("Blades states before resuming : " + clifApp.getGlobalState(null).toString());
			int res = clifApp.resume(null);
			if (res == 0)
			{
				res = clifApp.waitForState(null, BladeState.RUNNING);
				logger.info("Blades states after waiting : " + clifApp.getGlobalState(null).toString());
				if (res == 0)
				{
					logger.info("Test Resumed");
					return "Test Resumed";
				}
			}
			throw new ClifHandlerException("error.blade.unresumable");
		}
		catch (Exception ex)
		{
			throw new ClifHandlerException("error.resume.blades", ex);
		}
	}

	public String[][] getMetrics()
	{
		String[] injIds = getInjectorsIds();
		String[][] stats = new String[injIds.length][11];
		ClifAppFacade clifApp = getClifApp();
		if (clifApp == null)
		{
			throw new ClifHandlerException("error.testplan.deployed");
		}
		BladeState state = clifApp.getGlobalState(null);
		if (state.equals(BladeState.COMPLETED))
		{
			logger.info("Test completed");
			return null;
		}
		for (int i = 0; i < injIds.length; i++)
		{
			stats[i][0] = "Injector " + injIds[i];
			long[] injStat = clifApp.getStats(injIds[i]);
			for (int j = 0; j < injStat.length; j++)
			{
				stats[i][j + 1] = String.valueOf(injStat[j]);
			}
		}
		return stats;
	}

	/**
	 * This function retrieves the various servers. It also generates the quickstats
	 * file
	 * 
	 * @param generateQuickstat
	 * @return
	 * @throws IOException
	 */
	public String collectTest(String generateQuickstat) throws IOException
	{
		String testPlanName = Util.getFileBasename(getTestPlanFileName());
		String projectName = Util.getProjectName(getTestPlanFileName());
		try
		{
			String quickstatContent = "";
			ClifAppFacade clifApp = getClifAppFacade(testPlanName);
			logger.info("Waiting for test termination...");
			clifApp.join(null);
			logger.info("Collecting data from test plan " + testPlanName + "...");
			int res = clifApp.collect(null, null);
			if (res < 0)
			{
				throw new ClifHandlerException("error.blade.uncollectable");
			}
			if (generateQuickstat.equals("generateQuickstat"))
			{
				// Generate the quickstats file by running clifcmd quickstats
				logger.info("Generating quickstats report");
				Process process = Util.startClifCmdProcess(
					this.rootLocation.resolve(projectName).toFile(),
					"quickstats",
					"report");

				// We retrieve the quickstat command standard output
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				StringBuilder stringBuilder = new StringBuilder();
				int c = bufferedReader.read();

				while (c != -1) {
					stringBuilder.append(String.valueOf((char) c));
					c = bufferedReader.read();
				}
				quickstatContent = stringBuilder.toString();
				if (quickstatContent.indexOf("Quick statistics report done") != -1) {
					quickstatContent = quickstatContent.substring(0,
							quickstatContent.indexOf("Quick statistics report done"));
				}
				//delete useless space returned by quickstats
				String[] splitQuickstat = quickstatContent.split("\t");
				for (int i = 0; i < splitQuickstat.length; i++)
				{
					splitQuickstat[i].trim();
				}
				quickstatContent = "";
				for (String cell : splitQuickstat)
				{
					quickstatContent += cell + "\t";
				}
				quickstatContent = quickstatContent.substring(0, quickstatContent.length() - 1);

				// We choose the file name
				String folderName = retrieveLastFolderName(this.rootLocation.resolve(projectName).resolve("report"));

				// We create a file and fill the quickstat content to it
				File file = this.rootLocation
					.resolve(projectName)
					.resolve("report")
					.resolve(folderName + ".csv")
					.toFile();
				file.createNewFile();
				FileWriter myWriter = new FileWriter(file);
				myWriter.write(quickstatContent);
				myWriter.close();

				// We add the filename to the returned value
				quickstatContent = folderName + ".csv" + "\n" + quickstatContent;
			}
			logger.info("Collection done");
			return quickstatContent;
		}
		catch (Exception ex)
		{
			throw new ClifHandlerException("error.execution.collecting", ex);
		}
	}

	/**
	 * This function is used to retrieve from which test run folder
	 * the "quick stats" report will be generated. The folder is
	 * selected according to its name, which (1) shall begin with
	 * current test run id, and (2) be the greater in terms of string
	 * comparison. This relies on the common CLIF usage consisting
	 * in naming test run folders by concatenating a test run id with
	 * the date and time of test deployment, given the date-and-time
	 * format string order is consistent with chronology.
	 * 
	 * @param reportFolder The global report folder that we want to read
	 * @return the name of the last/latest test run folder
	 * @throws IOException
	 */
	public String retrieveLastFolderName(Path reportFolder)
	throws IOException
	{
		String result = "";
		DirectoryStream<Path> stream = Files.newDirectoryStream(reportFolder);

		for (Path path : stream)
		{
			if (Files.isDirectory(path))
			{
				String directoryName = path.getFileName().toString();
				if (
					directoryName.contains(getResultFolderName())
					&& directoryName.compareTo(result) > 0)
				{
					result = directoryName;
				}
			}
		}
		if (result.isEmpty())
		{
			throw new ClifHandlerException("test.quickstat.error.notest");
		}
		return result;
	}

	/**
	 * This function creates the ElasticSearch index and the date from the filePath.
	 * It also retrieves information about the file size.
	 * 
	 * @param partialFilePath : The partial file path of the file that contains the
	 *                        wanted data. Example :
	 *                        projet/report/testName_date/BladeId/event
	 * @return A list of the required parameters to build the bulk
	 * @throws ParseException
	 */
	public ArrayList<Object> prepareTestData(String partialFilePath) throws ParseException {
		String filePath = this.rootLocation + "/" + partialFilePath;

		// create a Date pattern
		String pattern = "yyyy-MM-dd_HH-mm-ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		// retrieve the date from folderName
		String[] pathItemArray = partialFilePath.split("/");
		String folderTimeStamp = "";
		String dateTime = "";
		// retrieve the folder that contains the timestamp
		for (int i = 0; i < pathItemArray.length; i++) {
			if (pathItemArray[i].length() >= 19 && pathItemArray[i].contains("_") && pathItemArray[i].contains("-")) {
				dateTime = pathItemArray[i].substring(pathItemArray[i].length() - 19, pathItemArray[i].length());
				folderTimeStamp = pathItemArray[i];
			}
		}
		// format date to match the pattern
		dateTime = dateTime.replaceAll("h", "-");
		dateTime = dateTime.replaceAll("m", "-");
		// convert time since 1970 in ms
		Date date = simpleDateFormat.parse(dateTime);
		long originTime = date.getTime();

		// index format : clif-event-bladeId-date
		String bladeId = pathItemArray[pathItemArray.length - 1];
		bladeId = bladeId.replace(' ', '_');
		String event = pathItemArray[pathItemArray.length - 2];
		String index = "clif-" + event + "-" + bladeId + "-" + folderTimeStamp;
		index = index.toLowerCase();
		index = index.replace(' ', '_');

		// Obtain file size
		File file = new File(filePath);
		String bytes = Long.toString(file.length());

		ArrayList<Object> bulkParameters = new ArrayList<Object>();
		bulkParameters.add(index);
		bulkParameters.add(filePath);
		bulkParameters.add(originTime);
		bulkParameters.add(bladeId);
		bulkParameters.add(bytes);
		return bulkParameters;

	}

	/**
	 * Retrieves the class name associated to the event by reading the file
	 * filePath.classname
	 * 
	 * @param filePath : the absolute url of the event file
	 * @return the class name associated to the event file
	 * @throws IOException
	 */
	public String getClassName(String filePath) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath + ".classname")));
		String className = "";
		while (reader.ready()) {
			className += reader.readLine();
		}
		reader.close();
		return className;
	}

	/**
	 * This function uses Java reflection to get the labels associated to an event.
	 * The method getEventFieldLabels is called to retrieve the labels.
	 * 
	 * @param event     : the file name. Example : action
	 * @param className :the class associated to this event. Example :
	 *                  org.ow2.clif.storage.api.ActionEvent
	 * @return the labels associated to the event
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public String[] obtainLabels(String event, String className)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// use the reflexion to invoke the static method getEventFieldLabels
		Class<?> cls = Class.forName(className);
		Method method = cls.getMethod("getEventFieldLabels", String.class);
		String[] labels = (String[]) method.invoke(null, event);
		return labels;
	}

	/**
	 * This function check if an index already exists in ElasticSearch.
	 * 
	 * @param index : the index name that we want to test
	 * @return If the index exists, returns true. Else, returns false.
	 * @throws IOException
	 */
	public boolean checkIndexExist(String index) throws IOException {
		RestHighLevelClient client = null;
		ClientConfiguration clientConfiguration = ClientConfiguration.builder().connectedTo("localhost:9200").build();
		client = RestClients.create(clientConfiguration).rest();

		GetIndexRequest requestIndex = new GetIndexRequest(index);
		boolean exists = client.indices().exists(requestIndex, RequestOptions.DEFAULT);
		return exists;
	}

	/**
	 * Creates a bulk with the data read from url and the labels from labels.
	 * 
	 * @param index      : The index name
	 * @param origin     : The origin time. Used to calculate the date parameter
	 * @param url        : The location of the file to be read
	 * @param labels     : The labels that match the current event
	 * @param classname: The event class name
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */

	public void handleBulk(String index, long origin, String url, String[] labels, String className)
			throws IOException, ClassNotFoundException {
		RestHighLevelClient client = null;
		ClientConfiguration clientConfiguration = ClientConfiguration.builder().connectedTo("localhost:9200").build();
		client = RestClients.create(clientConfiguration).rest();
		BulkRequest blk = new BulkRequest();
		MappingElasticSearch mappingElasticSearch = new MappingElasticSearch();

		// We read the file
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(url)));
		int id = 0;

		// we get the label data types
		String[] labelType = mappingElasticSearch.getLabelType(className, labels.length);

		// data
		while (reader.ready()) {
			String line = reader.readLine();
			XContentBuilder builder = XContentFactory.jsonBuilder();
			builder.startObject();

			if (line.charAt(0) != '#') {
				String[] values = line.split(",");
				// handle date
				long temp = origin + Long.valueOf(values[0]);
				values[0] = String.valueOf(temp);
				builder.timeField(labels[0], temp);

				// handle other fields
				builder = mappingElasticSearch.buildField(builder, labels, values, labelType);

				// complete the bulk
				builder.endObject();
				IndexRequest request = new IndexRequest(index).id(String.valueOf(id)).source(builder);
				blk.add(request);
				if (id % 1000 == 0 && id > 1) {
					client.bulk(blk, RequestOptions.DEFAULT);
					blk = new BulkRequest();
				}
				id++;
			}
		}

		reader.close();
		// send data
		client.bulk(blk, RequestOptions.DEFAULT);

		// mapping
		mappingElasticSearch.mappingEvent(index, labels, client, labelType);
//		logger.info("Sent " + url + " to ElasticSearch");
	}

}
