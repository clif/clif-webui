/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin, Antoine Thevenet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.webui.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.ow2.clif.console.lib.webui.Util;
import org.ow2.clif.console.lib.webui.storage.Exceptions.*;
import org.ow2.clif.console.lib.webui.storage.StorageHandler;

/**
 *
 * @author Tim Martin
 * @author Antoine Thevenet
 */

@Controller
public class StorageController {

	private final StorageHandler storageHandler;
	Logger logger = (Logger) LoggerFactory.getLogger(StorageController.class);

	@Autowired
	public StorageController(StorageHandler storageHandler) {
		this.storageHandler = storageHandler;
	}

	@RequestMapping("/")
	public String mapEditor() {
		return "redirect:/login";
	}

	@GetMapping("/clifwebui")
	public String listUploadedFiles(Model model) throws IOException {
		model = storageHandler.listUploadedFiles(model);
		if (model == null) {
			return "login";
		}
		return "clifwebui";
	}

	@RequestMapping(value = "/reloadFiles", method = RequestMethod.GET)
	@ResponseBody
	public Object[] reloadFiles() throws IOException {
		return storageHandler.getAccessiblesFiles();
	}

	@RequestMapping(value = "/reloadDirs", method = RequestMethod.GET)
	@ResponseBody
	public Object[] reloadDirs() throws IOException {
		return storageHandler.getAccessiblesDirs();
	}

	@RequestMapping(value = "/getFileContent", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> displayFile(@RequestBody String filename) {
		try {
			String response = storageHandler.loadContent(filename);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getFilteredFilesContent", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> displayFilteredFiles(
			@RequestParam("projects") Set<String> projects,
			@RequestParam("filesnames") Set<String> filesnames,
			@RequestParam("excludedfilesnames") Set<String> excludedfilesnames, 
			@RequestParam("keyword") String keyword, 
			@RequestParam("sensitiveCase") boolean sensitiveCase) {
		try {
			
			if(!sensitiveCase) { keyword = keyword.toUpperCase(); }
			
			// Retrieve projects paths: 
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(reloadFiles()).replace("[", "").replace("]", "").replace("\"", "");
			Set<String> filesPath = new HashSet<String>(Arrays.asList(json.split(",")));
			Set<LinkedList<String>> splitedFilesPath = new HashSet<>();
			filesPath.forEach((filePath)->{splitedFilesPath.add(new LinkedList<String>(Arrays.asList(filePath.trim().split("/"))));});
			
			// Retrieve file content an filter: 
			String tmpProjectName, tmpPath, tmpFileContent;
			boolean fileAccepted, excludedfileAccepted;
			
			JSONArray responseObject = new JSONArray();
			for(LinkedList<String> filePath : splitedFilesPath) {// project scope defined by user
				tmpProjectName = filePath.peekFirst();
				
				// project filter: 
				if(projects.contains(tmpProjectName)) {
					tmpPath = "/" + String.join("/", filePath);
					
					// check if it's a file & contains the filename filter: 
					fileAccepted = false; 
					excludedfileAccepted=true;
					
					for(String filename : filesnames) { fileAccepted |= tmpPath.contains(filename); }
					for(String excludedfilename : excludedfilesnames) { excludedfileAccepted &= !tmpPath.contains(excludedfilename); }
					if(filesnames.isEmpty()) { fileAccepted = true; }
					
					if(storageHandler.isFile(tmpPath) && fileAccepted && excludedfileAccepted){
						
						if(keyword.length() == 0) {
							JSONObject response = new JSONObject();
							response.put("filePath", tmpPath);
							response.put("projectName", tmpProjectName);
							response.put("matchesNumber", 0);
							responseObject.put(response);
						}else {
							// retrieve the content: 
							tmpFileContent = storageHandler.retrieveContent(tmpPath);
							if(!sensitiveCase) {
								tmpFileContent = tmpFileContent.toUpperCase();
							}
							if(tmpFileContent.contains(keyword)){
								JSONObject response = new JSONObject();
								response.put("filePath", tmpPath);
								response.put("projectName", tmpProjectName);
								response.put("matchesNumber", StringUtils.countMatches(tmpFileContent, keyword));
								responseObject.put(response);
							}
						}
					}
				}
				
			}
			
			return new ResponseEntity<>(responseObject.toString(), HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/saveFile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> saveFile(@RequestBody String file) {
		try {
			String[] fileContent = Util.getArgsFile(file);
			String response = storageHandler.saveFile(fileContent[0], fileContent[1]);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/importFile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleFileUpload(@RequestBody String file) {
		try {
			String[] fileContent = Util.getArgsFile(file);
			String response = storageHandler.importFile(fileContent[0], fileContent[1]);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/importProject", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ArrayList<String>> handleProjectImport(@RequestBody byte[] data) {
		try {
			ArrayList<String> response = storageHandler.importProject(data);
			logger.info("Response: " + response);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info(e.toString());
			ArrayList<String> response = new ArrayList<>();
			response.add(e.getLocalizedMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/exportProject/{projectName}", method = RequestMethod.GET)
	public void handleExportImport(@PathVariable("projectName") String projectName, HttpServletResponse response) {
		try {
			InputStream is = storageHandler.getZipProject(projectName);
			response.setContentType("application/zip");
			IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			logger.info("Error writing file to output stream for ", projectName, ex);
			throw new RuntimeException("IOError writing file to output stream");
		}
	}

	@RequestMapping(value = "/createFile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleFileCreate(@RequestBody String file, RedirectAttributes redirectAttributes)
			throws IOException {
		try {
			String response = storageHandler.createFile(file);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/createFileWithContent", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleFileCreateWithContent(@RequestBody String fileWithContent,
			RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = storageHandler.createFileWithContent(fileWithContent);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/duplicateFile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleDuplicateFile(@RequestBody String file, RedirectAttributes redirectAttributes)
			throws IOException {
		try {
			String destination = file.substring(0, file.indexOf("\n"));
			String source = file.substring(file.indexOf("\n") + 1);
			String response = storageHandler.duplicateFile(source, destination);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/createDirectory", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleDirectoryCreate(@RequestBody String path, RedirectAttributes redirectAttributes)
			throws IOException {
		try {
			String response = storageHandler.createDirectory(path);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/rename", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleRename(@RequestBody String nameThenNewName,
			RedirectAttributes redirectAttributes) throws IOException {
		try {
			String path = nameThenNewName.substring(0, nameThenNewName.indexOf("\n"));
			String newName = nameThenNewName.substring(nameThenNewName.indexOf("\n") + 1);
			String response = storageHandler.rename(path, newName);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/movefile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleMove(@RequestBody String nameThenNewName, RedirectAttributes redirectAttributes)
	throws IOException
	{
		try	
		{
			String file = nameThenNewName.substring(0, nameThenNewName.indexOf("\n"));
			String destPath = nameThenNewName.substring(nameThenNewName.indexOf("\n") + 1);
			String response = storageHandler.move(file, destPath);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		catch (Exception ex)
		{
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleDelete(@RequestBody String file) {
		try {
			String response = storageHandler.delete(file);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

	@RequestMapping(value = "/getLocalizedTexts", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, String> sendLocalizedTexts() {
		HashMap<String, String> locales = new HashMap<String, String>();
		ResourceBundle messages = ResourceBundle.getBundle("lang/res", LocaleContextHolder.getLocale());
		Enumeration<String> keys = messages.getKeys();
		while (keys.hasMoreElements()) {
			String param = keys.nextElement();
			locales.put(param, messages.getString(param));
		}
		return locales;
	}

	@RequestMapping(value = "/buildCache", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> buildCache(@RequestBody String resourceToAccess,
			RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = storageHandler.buildCache();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/buildProbeCache", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> handleCacheProbe(@RequestBody String resourceToAccess,
			RedirectAttributes redirectAttributes) throws IOException {
		try {
			String response = storageHandler.buildProbeCache();
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
