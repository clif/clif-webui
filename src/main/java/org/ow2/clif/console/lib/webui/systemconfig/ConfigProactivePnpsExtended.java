package org.ow2.clif.console.lib.webui.systemconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * Reads the application.properties file specified when lanching CLIF Webui.
 * Retrieves the properties containing "proactive.pnp.keystore"
 * @author Antoine Thevenet
 *
 */
@Configuration
@ConfigurationProperties(prefix = "proactive.pnps.keystore")
public class ConfigProactivePnpsExtended {
	private String password = null;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
