/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2020 Tim Martin, Antoine Thevenet
 * Copyright (C) 2022, 2024 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui.storage;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileSystemUtils;
import org.ow2.clif.console.lib.webui.storage.Exceptions.*;
import org.ow2.clif.console.lib.webui.Util;
import org.xml.sax.SAXException;

/**
 * @author Tim Martin
 * @author Antoine Thevenet
 * @author Bruno Dillenseger
 */

public class FileSystemStorage {

	private final Path rootLocation;

	Logger logger = (Logger) LoggerFactory.getLogger(FileSystemStorage.class);

	public FileSystemStorage(String rootLocation) {
		this.rootLocation = Paths.get(rootLocation);
		this.init();
	}

	public FileSystemStorage() {
		this.rootLocation = Paths.get(System.getProperty("user.dir") + "/CLIFspace/");
	}

	public void store(String file, String content) {
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + file);
			}
			if (resolve(file).toFile().exists()) {
				throw new StorageException(resolve(file).toString() + " already exists");
			}
			File f = resolve(file).toFile();
			f.createNewFile();
			FileWriter myWriter = new FileWriter(f);
			myWriter.write(content);
			myWriter.close();

		} catch (IOException e) {
			throw new StorageException("Failed to store file " + file, e);
		}
	}


	/**
	 * Creates a file according to the given path, which shall be relative
	 * to the workspace. More generally, it is supposed to be sanitized
	 * (not absolute, no . nor .. path element).
	 * Any non-existing directory in the given filename's path is created.
	 * @param fileName the relative path for the new file to be created
	 * @throws StorageException the file could not be created, either because
	 * it already exists, or for some other reasons: IO issue with the target
	 * filesystem, permissions...
	 */
	public void createFile(String fileName)
	throws StorageException
	{
		logger.debug("Creating new file " + fileName);
		Path fullPath = resolve(fileName);
		try
		{
			Files.createDirectories(fullPath.getParent());
			if (!fullPath.toFile().createNewFile())
			{
				throw new StorageException(fullPath.toString() + " already exists");
			}
		}
		catch (IOException ex)
		{
			throw new StorageException("Failed to create file " + fullPath.toString(), ex);
		}
	}


	/**
	 * Initialize a CLIF test plan file content.
	 * @param ctpFile the relative path (starting with the project path element).
	 * to the target .ctp file. The path is supposed to be sanitized
	 * (not absolute, no . nor .. path element)
	 * @throws IOException if an I/O error occurred, most probably when writing to
	 * the target file (unlikely but also possibly while reading the file content
	 * template)
	 */
	void initCtpFile(File ctpFile)
	throws IOException
	{
		initFileContentFromResource(ctpFile, "templates/file/skeleton/ctpSkeleton.txt");
	}


	/**
	 * Initialize an ISAC scenerio file content.
	 * @param xisFile the relative path (starting with the project path element).
	 * to the target .xis file. The path is supposed to be sanitized
	 * (not absolute, no . nor .. path element)
	 * @throws IOException if an I/O error occurred, most probably when writing to
	 * the target file (unlikely but also possibly while reading the file content
	 * template)
	 */
	void initXisFile(File xisFile)
	throws IOException
	{
		initFileContentFromResource(xisFile, "templates/file/skeleton/xisSkeleton.xml");
	}

	/**
	 * Initialize a CLIF test plan file content.
	 * @param ctpFile the relative path (starting with the project path element).
	 * to the target .ctp file. The path is supposed to be sanitized
	 * (not absolute, no . nor .. path element)
	 * @throws IOException if an I/O error occurred, most probably when writing to
	 * the target file (unlikely but also possibly while reading the file content
	 * template)
	 */
	protected void initFileContentFromResource(File path, String resource)
	throws IOException
	{
		logger.info("Setting content for file " + path);
		InputStream is = new ClassPathResource(resource).getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String content = reader.lines().collect(Collectors.joining("\n"));
		reader.close();
		FileWriter myWriter = new FileWriter(resolve(path.toString()).toFile());
		myWriter.write(content);
		myWriter.close();
	}


	/**
	 * Duplicates a file. Both source and destination paths which shall be relative
	 * to the workspace. More generally, they are supposed to be sanitized (not absolute,
	 * . discarded, .. resolved or discarded).
	 * @param fromPath the relative path of the file to be duplicated
	 * @param toPath the relative path of the target copy file to create
	 */
	public void duplicateFile(Path fromPath, Path toPath)
	throws IOException
	{
		createFile(toPath.toString());
		logger.info("Copying " + fromPath + " to " + toPath);
		try (
			Reader in = new FileReader(resolve(fromPath).toFile());
			Writer out = new FileWriter(resolve(toPath).toFile()))
		{
			char[] buffer = new char[4096];
			int n;
			while ((n = in.read(buffer)) != -1)
			{
				out.write(buffer, 0, n);
			}
		}
	}


	/**
	 * Creates a directory according to the given path, which shall be relative
	 * to the workspace. More generally, it is supposed to be sanitized (not absolute,
	 * . discarded, .. resolved or discarded)
	 * @param path the relative path for the new directory to be created
	 */
	public void createDirectory(Path path)
	{
		logger.debug("Creating directory " + path + "...");
		Path fullPath = resolve(path);
		if (fullPath.getParent() == null || !fullPath.getParent().toFile().exists())
		{
			logger.info("Could not create directory " + fullPath + " (parent does not exist)");
			throw new StorageException(path + ": parent directory doesn't exist");
		}
		if (fullPath.toFile().exists())
		{
			logger.info("Could not create directory " + fullPath + " (already exists)");
			throw new StorageException(path + " already exists");
		}
		try
		{
			Files.createDirectories(fullPath);
			logger.info("Created directory " + fullPath);
			if (path.getNameCount() == 1)
			{ // created dir is a project
				Util.createPaclifOpts(fullPath.resolve("paclif.opts").toString());
			}
		}
		catch (IOException ex)
		{
			String message = "Could not create directory " + fullPath + "(" + ex.getLocalizedMessage() + ")";
			logger.info(message);
			throw new StorageException(message, ex);
		}
	}


	/**
	 * Renames a directory (whatever a project directory itself, or a project sub-directory)
	 * or a file. Source and destination path names are assumed to be relative to the workspace.
	 * More generally, they are supposed to be sanitized (not absolute, . discarded,
	 * .. resolved or discarded).
	 * Note: for a plain renaming, the source and destination paths shall have the same
	 * parent directory, but this is not checked nor enforced. If the destination directory
	 * is different from the source directory, this method performs a move.
	 * @param fromPath directory or file to rename (relative to the workspace)
	 * @param toPath new name, including path (relative to the workspace)
	 */
	public void rename(Path fromPath, Path toPath)
	{
		logger.info("Renaming " + fromPath + " to " + toPath);
		try
		{
			Files.move(resolve(fromPath), resolve(toPath));
		}
		catch (IOException ex)
		{
			logger.info("Renaming failed: " + ex.getLocalizedMessage());
			throw new StorageException(
				Util.returnMessage(
					"action.rename.failed",
					new String[] {
						fromPath.toString(),
						toPath.toString()
					}));
		}
	}


	/**
	 * Moves a directory or a file to the given directory.
	 * Source and destination path names are assumed to be relative to the workspace.
	 * More generally, they are supposed to be sanitized (not absolute, . discarded,
	 * .. resolved or discarded).
	 * @param fromPath directory or file to move (relative to the workspace)
	 * @param toPath target directory (relative to the workspace)
	 */
	public void move(Path fromPath, Path toPath)
	{
		logger.info("Moving " + fromPath + " to " + toPath);
		try
		{
			Files.move(resolve(fromPath), resolve(toPath).resolve(fromPath.getFileName()));
		}
		catch (IOException ex)
		{
			logger.info("Moving failed: " + ex.getLocalizedMessage());
			throw new StorageException(
				Util.returnMessage(
					"action.move.failed",
					new String[] {
						fromPath.toString(),
						toPath.toString()
					}));
		}
	}


	/**
	 * Deletes a file or directory. The given path shall be relative
	 * to the workspace. More generally, it is supposed to be sanitized
	 * (not absolute, no . nor .. path element)
	 * @param path the relative path of the file or directory to be deleted
	 * @throws StorageException the file could not be deleted, either because it
	 * does not exist, or for some other reasons: IO issue with the target filesystem,
	 * permissions...
	 */
	public void delete(Path path)
	throws StorageException
	{
		logger.info("Deleting " + path);
		Path fullPath = resolve(path);
		try
		{
			if (Files.isDirectory(fullPath))
			{
				FileSystemUtils.deleteRecursively(fullPath);
			}
			else
			{
				Files.delete(fullPath);
			}
		}
		catch (IOException ex)
		{
			throw new StorageException(
				Util.returnMessage(
					"action.delete.failed",
					new String[] { path.toString() }),
				ex);
		}
	}


	public Stream<String> loadAll()
	{
		try
		{
			return Files.walk(this.rootLocation)
				.filter(path -> !path.equals(this.rootLocation))
				.map(this.rootLocation::relativize)
				.map(path -> path.toString().replaceAll("\\\\", "/"));
		}
		catch (IOException e)
		{
			throw new StorageException("Failed to read stored files", e);
		}
	}


	public List<String> loadProjects() throws IOException {
		logger.debug("List projects: ");
		List<String> listPath =
			(Files.walk(this.rootLocation, 1)
				.filter(path -> !path.equals(this.rootLocation))
				.filter(path -> path.toFile().isDirectory())
				.sorted()
				.map(this.rootLocation::relativize)
				.map(path -> path.toString())
			).collect(Collectors.toList());
		logger.debug(Arrays.toString(listPath.toArray()));
		return listPath;
	}

/*
	public Path load(String filename) {
		return rootLocation.resolve(filename);
	}

	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);
			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}
*/
	public String saveFile(String filename, String fileContent) throws FileNotFoundException {
		PrintWriter prw = new PrintWriter(resolve(filename).toFile());
		prw.println(fileContent);
		prw.close();
		return "File saved";
	}


	public String loadContent(String filename)
	throws IOException
	{
		StringBuilder str = new StringBuilder();
		Scanner sc = new Scanner(resolve(filename));
		while (sc.hasNextLine())
		{
			str.append(sc.nextLine()).append("%0A");
		}
		sc.close();
		return str.toString();
	}


	public String retrieveContent(String filename) {
		File file = resolve(filename).toFile();
		StringBuilder content = new StringBuilder();
		
		try (BufferedReader in = new BufferedReader(
			new InputStreamReader(new FileInputStream(file), "UTF8"))) {
			String str;
			while ((str = in.readLine()) != null) {
				content.append(str);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return content.toString();
	}


	public void init() {
		try {
			Files.createDirectories(rootLocation);
		} catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}

	public Stream<String> loadDirectories()
	{
		try
		{
			return Files.walk(this.rootLocation)
				.filter(path -> !path.equals(this.rootLocation))
				.filter(Files::isDirectory)
				.map(this.rootLocation::relativize)
				.map(path -> path.toString().replaceAll("\\\\", "/"));
		}
		catch (IOException ex)
		{
			throw new StorageException("Failed to fetch directories", ex);
		}
	}

	public ArrayList<String> importUnzipProject(byte[] data) throws IOException
	{
		Set<String> projects = new HashSet<String>();
		String projectName;
		logger.info("unzipping project...");
		InputStream is = new ByteArrayInputStream(data);
		ZipInputStream zis = new ZipInputStream(is);
		ZipEntry zipEntry = zis.getNextEntry();
		byte[] buffer = new byte[1024];
		while (zipEntry != null)
		{
			String entryName = zipEntry.getName();
			logger.debug("Reading zip entry: " + entryName);
			entryName = Util.sanitize(Paths.get(entryName)).toString();
			projectName = Paths.get(entryName).getName(0).toString();

			if (Paths.get(entryName).getNameCount() == 1 && !zipEntry.isDirectory())
			{
				logger.info("Ignoring top-level zip file entry " + entryName);
			}
			else if (resolve(projectName).toFile().exists() && !projects.contains(projectName))
			{
				throw new StorageException(Util.returnMessage("error.import.archive.already-exists", new String[] { projectName }));
			}
			else
			{
				projects.add(projectName);
				File targetFile = resolve(entryName).toFile();
				if (zipEntry.isDirectory())
				{
					logger.info("Creating directory	" + targetFile.getCanonicalPath());
					Files.createDirectories(targetFile.toPath());
				}
				else // plain file in at least one (project) directory
				{
					if (!targetFile.getParentFile().exists())
					{
						logger.info("Creating path " + targetFile.getParentFile().getCanonicalPath().toString());
						Files.createDirectories(targetFile.getParentFile().toPath());
					}
					logger.info("Creating " + targetFile.toString());
					targetFile.createNewFile();
					try
					{
						try (
							FileOutputStream fos = new FileOutputStream(targetFile);
							BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length))
						{
							int len;
							while ((len = zis.read(buffer)) > 0)
							{
								bos.write(buffer, 0, len);
							}
						}
					}
					catch (Exception ex)
					{
						logger.info("Exception caught: " + ex.getLocalizedMessage());
						throw ex;
					}
				}
			}
			zipEntry = zis.getNextEntry();
		}
		for (String prj : projects)
		{ // create paclif.opts in every new project when necessary
			File paclifOptsFile = new File(resolve(prj).toFile(), "paclif.opts");
			if (!paclifOptsFile.exists())
			{
				logger.info("Creating paclif.opts at " + paclifOptsFile);
				Util.createPaclifOpts(paclifOptsFile.toString());
			}
		}
		logger.info("successfully created files");
		return new ArrayList<String>(projects);
	}

	public InputStream exportZipProject(String project) throws IOException {
		File projectDir = resolve(project).toFile();
		logger.info("Creating zip " + project + ".zip");

		if (!projectDir.exists())
			throw new StorageException(project + " does not exist");
		if (!projectDir.isDirectory() || project.contains("/"))
			throw new StorageException(project + " is not a project");
		File zipFile = resolve(project + ".zip").toFile();
		FileOutputStream fos = new FileOutputStream(zipFile);
		ZipOutputStream zos = new ZipOutputStream(fos);
		try {
			zipFile(projectDir, projectDir.getName(), zos);
			zos.close();
			fos.close();
		} catch (Exception e) {
			throw new StorageException("Failed to zip " + project);
		}
		InputStream is = new FileInputStream(zipFile);
		zipFile.delete();
		return is;

	}

	private void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
		if (fileToZip.isHidden()) {
			return;
		}
		if (fileToZip.isDirectory()) {
			if (fileName.endsWith("/")) {
				zipOut.putNextEntry(new ZipEntry(fileName));
				zipOut.closeEntry();
			} else {
				zipOut.putNextEntry(new ZipEntry(fileName + "/"));
				zipOut.closeEntry();
			}
			File[] children = fileToZip.listFiles();
			for (File childFile : children) {
				zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
			}
			return;
		}
		FileInputStream fis = new FileInputStream(fileToZip);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zipOut.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zipOut.write(bytes, 0, length);
		}
		fis.close();
	}

	////////////////////////////// cache Init///////////////////////////////
	/**
	 * This function is used to retrieve the content of each plugin file
	 * 
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public String initCache() throws IOException, ParserConfigurationException, SAXException
	{
		// OS dependent
		String osName = System.getProperty("os.name").toLowerCase();
		ProcessBuilder processBuilder = null;
		if (osName.contains("win"))
		{
			processBuilder = new ProcessBuilder("cmd", "/c", "retrievePlugins.bat");
		}
		else
		{
			processBuilder = new ProcessBuilder("retrievePlugins.sh");
		}
		Process process = processBuilder.start();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuilder stringBuilder = new StringBuilder();
		int c = bufferedReader.read();
		while (c != -1)
		{
			stringBuilder.append(String.valueOf((char) c));
			c = bufferedReader.read();
		}
		return stringBuilder.toString();
	}

	/**
	 * This function is used to retrieve the content of each plugin file
	 * 
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public String initCacheProbe() throws IOException, ParserConfigurationException, SAXException
	{
		// OS dependant
		String osName = System.getProperty("os.name").toLowerCase();
		
		ProcessBuilder processBuilder = null;
		if (osName.contains("win"))
		{
			processBuilder = new ProcessBuilder("cmd", "/c", "retrieveProbes.bat");
		} 
		else
		{
			processBuilder = new ProcessBuilder("retrieveProbes.sh");
		}
		processBuilder.redirectErrorStream(true);
		Process process = processBuilder.start();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuilder stringBuilder = new StringBuilder();
		int c = bufferedReader.read();
		while (c != -1)
		{
			stringBuilder.append(String.valueOf((char) c));
			c = bufferedReader.read();
		}
		return stringBuilder.toString();
	}

//////////////////////////////////Read properties/////////////////////////////////
	/**
	 * This function reads a .properties file to extract the plugin name, the plugin
	 * xml file name and the gui xml file name
	 * 
	 * @param folderURL          : The folder that contains every plugin
	 * @param pluginFolderURL    : The folder that contains the .properties file
	 * @param propertiesFileName : the .properties file name
	 * @return
	 * @throws IOException
	 */
	public ArrayList<String> readProperties(String folderURL, String pluginFolderURL, String propertiesFileName)
			throws IOException {

		InputStream propertiesFile = new ClassPathResource(
				(folderURL + pluginFolderURL).replaceAll("classes/", "") + "/" + propertiesFileName).getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(propertiesFile));
		String lineRead = new String();
		ArrayList<String> properties = new ArrayList<String>(Arrays.asList("", "", ""));

		while ((lineRead = reader.readLine()) != null) {
			if (lineRead.contains("plugin.name")) {
				properties.set(0, lineRead.substring(lineRead.indexOf("=") + 1));
			}
			if (lineRead.contains("plugin.guiFile")) {
				properties.set(1, lineRead.substring(lineRead.indexOf("=") + 1));
			}
			if (lineRead.contains("plugin.xmlFile")) {
				properties.set(2, lineRead.substring(lineRead.indexOf("=") + 1));
			}
		}
		return properties;
	}

///////////////////////////////////XML to JSON////////////////////////////////////
	/**
	 * This function reads a xml file and converts it to a String with a JSON format
	 * 
	 * @param resourcePath : The .xml file name and URL
	 * @return
	 * @throws IOException
	 */
	public String xmlToJson(String resourcePath) throws IOException {
		InputStream resource = new ClassPathResource(resourcePath.replaceAll("classes/", "")).getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
		String xml = reader.lines().collect(Collectors.joining("\n"));

		JSONObject xmlJSONObj = XML.toJSONObject(xml);
		String jsonPrettyPrintString = xmlJSONObj.toString(4);

		return jsonPrettyPrintString;
	}

	public boolean isFile(String relativePath) {
		return Files.isRegularFile(resolve(relativePath));
	}

	private Path resolve(String filename)
	{
		return rootLocation.resolve(filename);
	}

	private Path resolve(Path path)
	{
		return rootLocation.resolve(path);
	}
}
