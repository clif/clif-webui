/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2024 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.webui;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.ow2.clif.console.lib.webui.Util.sanitize;

import java.nio.file.Paths;

public class UtilTest {
    @Test
    public void sanitize_should_keep_normalized_path_as_is()
    {
        assertEquals("a/b/c", sanitize(Paths.get("a/b/c")).toString());
    }

    @Test
    public void sanitize_should_relativize_absolute_path()
    {
        assertEquals("a/b/c", sanitize(Paths.get("/a/b/c")).toString());
    }

    @Test
    public void sanitize_should_normalize_complex_path()
    {
        assertEquals("a/b/c2", sanitize(Paths.get("./a/../a/./b/../b/c1/../../b/c2")).toString());
    }

    @Test
    public void sanitize_should_clean_suspicious_path()
    {
        assertEquals(Paths.get(""), sanitize(Paths.get("../upper/a/b/c/../../../..")));
    }
}
